CREATE TABLE "users"(
    "id" INTEGER NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "password" VARCHAR(255) NOT NULL,
    "image" VARCHAR(255) NULL,
    "address" TEXT NOT NULL,
    "contact_number" INTEGER NOT NULL,
    "role" VARCHAR(255) NOT NULL,
    "status" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
    "updated_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL
);
ALTER TABLE
    "users" ADD PRIMARY KEY("id");
ALTER TABLE
    "users" ADD CONSTRAINT "users_email_unique" UNIQUE("email");
CREATE TABLE "stores"(
    "id" INTEGER NOT NULL,
    "user_id" INTEGER NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "image" VARCHAR(255) NULL,
    "description" TEXT NOT NULL,
    "subscription_count" INTEGER NULL,
    "status" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
    "updated_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL
);
ALTER TABLE
    "stores" ADD PRIMARY KEY("id");
ALTER TABLE
    "stores" ADD CONSTRAINT "stores_name_unique" UNIQUE("name");
CREATE TABLE "interactions"(
    "id" INTEGER NOT NULL,
    "user_id" INTEGER NOT NULL,
    "product_id" INTEGER NULL,
    "store_id" INTEGER NULL,
    "is_viewed" BOOLEAN NULL,
    "is_saved" BOOLEAN NULL,
    "is_subscribed" BOOLEAN NULL,
    "created_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
    "updated_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL
);
ALTER TABLE
    "interactions" ADD PRIMARY KEY("id");
CREATE TABLE "products"(
    "id" INTEGER NOT NULL,
    "shore_id" INTEGER NOT NULL,
    "title" VARCHAR(255) NOT NULL,
    "description" TEXT NULL,
    "variation" JSON NOT NULL,
    "price" DOUBLE PRECISION NOT NULL,
    "qty" DECIMAL(8, 2) NOT NULL,
    "main_img" VARCHAR(255) NOT NULL,
    "images" TEXT NULL,
    "detail" TEXT NULL,
    "type" VARCHAR(255) NOT NULL,
    "category" VARCHAR(255) NOT NULL,
    "status" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
    "updated_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL
);
ALTER TABLE
    "products" ADD PRIMARY KEY("id");
CREATE TABLE "shopping_lists"(
    "id" INTEGER NOT NULL,
    "user_id" INTEGER NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "status" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
    "updated_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL
);
ALTER TABLE
    "shopping_lists" ADD PRIMARY KEY("id");
CREATE TABLE "reviews"(
    "id" INTEGER NOT NULL,
    "user_id" INTEGER NOT NULL,
    "product_id" INTEGER NOT NULL,
    "rating" INTEGER NOT NULL,
    "content" TEXT NOT NULL,
    "status" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
    "updated_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL
);
ALTER TABLE
    "reviews" ADD PRIMARY KEY("id");
CREATE TABLE "shopping_list_products"(
    "id" INTEGER NOT NULL,
    "list_id" INTEGER NOT NULL,
    "product_id" INTEGER NOT NULL
);
ALTER TABLE
    "shopping_list_products" ADD PRIMARY KEY("id");
CREATE TABLE "review_replies"(
    "id" INTEGER NOT NULL,
    "user_id" INTEGER NOT NULL,
    "review_id" INTEGER NOT NULL,
    "content" TEXT NOT NULL,
    "status" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
    "updated_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL
);
ALTER TABLE
    "review_replies" ADD PRIMARY KEY("id");
CREATE TABLE "shopping_cart_products"(
    "id" INTEGER NOT NULL,
    "user_id" INTEGER NOT NULL,
    "product_id" INTEGER NOT NULL
);
ALTER TABLE
    "shopping_cart_products" ADD PRIMARY KEY("id");
CREATE TABLE "orders"(
    "id" INTEGER NOT NULL,
    "user_id" INTEGER NOT NULL,
    "store_id" INTEGER NOT NULL,
    "receiver" VARCHAR(255) NOT NULL,
    "address" TEXT NOT NULL,
    "contact_number" INTEGER NOT NULL,
    "status" VARCHAR(255) NOT NULL,
    "paid_at" TIMESTAMP(0) WITHOUT TIME ZONE NULL,
    "shipped_at" TIMESTAMP(0) WITHOUT TIME ZONE NULL,
    "created_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
    "updated_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL
);
ALTER TABLE
    "orders" ADD PRIMARY KEY("id");
CREATE TABLE "order_products"(
    "id" INTEGER NOT NULL,
    "order_id" INTEGER NOT NULL,
    "product_id" INTEGER NOT NULL,
    "img" VARCHAR(255) NOT NULL,
    "price" DOUBLE PRECISION NOT NULL,
    "qty" INTEGER NOT NULL
);
ALTER TABLE
    "order_products" ADD PRIMARY KEY("id");
ALTER TABLE
    "stores" ADD CONSTRAINT "stores_user_id_foreign" FOREIGN KEY("user_id") REFERENCES "users"("id");
ALTER TABLE
    "interactions" ADD CONSTRAINT "interactions_user_id_foreign" FOREIGN KEY("user_id") REFERENCES "users"("id");
ALTER TABLE
    "shopping_lists" ADD CONSTRAINT "shopping_lists_user_id_foreign" FOREIGN KEY("user_id") REFERENCES "users"("id");
ALTER TABLE
    "reviews" ADD CONSTRAINT "reviews_user_id_foreign" FOREIGN KEY("user_id") REFERENCES "users"("id");
ALTER TABLE
    "products" ADD CONSTRAINT "products_shore_id_foreign" FOREIGN KEY("shore_id") REFERENCES "stores"("id");
ALTER TABLE
    "interactions" ADD CONSTRAINT "interactions_store_id_foreign" FOREIGN KEY("store_id") REFERENCES "stores"("id");
ALTER TABLE
    "interactions" ADD CONSTRAINT "interactions_product_id_foreign" FOREIGN KEY("product_id") REFERENCES "products"("id");
ALTER TABLE
    "reviews" ADD CONSTRAINT "reviews_product_id_foreign" FOREIGN KEY("product_id") REFERENCES "products"("id");
ALTER TABLE
    "shopping_list_products" ADD CONSTRAINT "shopping_list_products_list_id_foreign" FOREIGN KEY("list_id") REFERENCES "shopping_lists"("id");
ALTER TABLE
    "shopping_list_products" ADD CONSTRAINT "shopping_list_products_product_id_foreign" FOREIGN KEY("product_id") REFERENCES "products"("id");
ALTER TABLE
    "review_replies" ADD CONSTRAINT "review_replies_user_id_foreign" FOREIGN KEY("user_id") REFERENCES "users"("id");
ALTER TABLE
    "review_replies" ADD CONSTRAINT "review_replies_review_id_foreign" FOREIGN KEY("review_id") REFERENCES "reviews"("id");
ALTER TABLE
    "shopping_cart_products" ADD CONSTRAINT "shopping_cart_products_user_id_foreign" FOREIGN KEY("user_id") REFERENCES "users"("id");
ALTER TABLE
    "shopping_cart_products" ADD CONSTRAINT "shopping_cart_products_product_id_foreign" FOREIGN KEY("product_id") REFERENCES "products"("id");
ALTER TABLE
    "orders" ADD CONSTRAINT "orders_user_id_foreign" FOREIGN KEY("user_id") REFERENCES "users"("id");
ALTER TABLE
    "orders" ADD CONSTRAINT "orders_store_id_foreign" FOREIGN KEY("store_id") REFERENCES "stores"("id");
ALTER TABLE
    "order_products" ADD CONSTRAINT "order_products_order_id_foreign" FOREIGN KEY("order_id") REFERENCES "orders"("id");
ALTER TABLE
    "order_products" ADD CONSTRAINT "order_products_product_id_foreign" FOREIGN KEY("product_id") REFERENCES "products"("id");