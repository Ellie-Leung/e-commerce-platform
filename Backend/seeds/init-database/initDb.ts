import pg from 'pg';
import XLSX from "XLSX";
import dotenv from 'dotenv';
import env from '../utils/env';
import path from 'path'

dotenv.config();

const client = new pg.Client({
    database: env.DB_NAME,
    user: env.DB_USERNAME,
    password: env.DB_PASSWORD,
});


export interface users {
    name: string;
    email: string;
    password: string;
    image?: text;
    address: text;
    contact_number: integer;
    role: string;
    status: string;
    created_at: number;
    updated_at: number;
}

export interface stores {
    user_id: integer;
    name: string;
    image?: string;
    description?: text;    
    status: string;
    created_at: number;
    updated_at: number;
}

export interface products {
    store_id: integer;
    title: string;
    description: text;
    variation: json[];
    price: decimal[];
    stock: integer;
    images: string[];
    detail?: text;
    type: string;
    category: json[];
    status: string;
    created_at: number;
    updated_at: number;
}

export interface interactions {
    user_id: integer;
    product_id?: integer;
    store_id?: integer;
    is_viewed?: boolean;
    is_saved?: boolean;
    is_subscribed?: boolean;
    created_at: number;
    updated_at: number;
}

export interface shopping_lists {
    user_id: integer;
    name: string;
    status: string;
    created_at: number;
    updated_at: number;
}


export interface shopping_list_products {
    list_id: integer;
    product_id: integer;

}

export interface reviews {
    user_id: integer;
    product_id: integer;
    rating: integer;
    content?: text;
    status: string;
    created_at: number;
    updated_at: number;
}

export interface review_replies {
    user_id: integer;
    review_id:integer;
    content: text;
    status: string;
    created_at: number;
    updated_at: number;
}

export interface shopping_cart_products {
    user_id: integer;
    product_id: integer;
}

export interface orders {
    user_id: integer;
    store_id: integer;
    amount: decimal;
    receiver: string;
    address: text;
    contact_number: integer;
    status: string;
    paid_at?: timestamps;
    shipped_at?: timestamps;
    created_at: number;
    updated_at: number;
}


export interface order_products {
    order_id: integer;
    product_id: integer;
    image: string;
    variation: json;
    unit_price: decimal;
    qty: integer;
}


async function main() {
    try {
        await client.connect(); // "dial-in" to the postgres server
        let workbook = XLSX.readFile(path.resolve(path.join(__dirname, "./init_db.xlsx")));

        let usersWS = workbook.Sheets["users"];
        let storesWS = workbook.Sheets["stores"];
        let productsWS = workbook.Sheets["products"];
        let interactionsWS = workbook.Sheets["interactions"];
        let shopping_listsWS = workbook.Sheets["shopping_lists"];
        let shopping_list_productsWS = workbook.Sheets["shopping_list_products"];
        let reviewsWS = workbook.Sheets["reviews"];
        let review_repliesWS = workbook.Sheets["review_replies"];
        let shopping_cart_productsWS = workbook.Sheets["shopping_cart_products"];
        let ordersWS = workbook.Sheets["orders"];
        let order_productsWS = workbook.Sheets["order_products"];

        await client.query(/*SQL*/ `TRUNCATE TABLE users RESTART IDENTITY CASCADE`);
        await client.query(/*SQL*/ `TRUNCATE TABLE stores RESTART IDENTITY CASCADE`);
        await client.query(/*SQL*/ `TRUNCATE TABLE products RESTART IDENTITY CASCADE`);
        await client.query(/*SQL*/ `TRUNCATE TABLE interactions RESTART IDENTITY CASCADE`);
        await client.query(/*SQL*/ `TRUNCATE TABLE shopping_lists RESTART IDENTITY CASCADE`);
        await client.query(/*SQL*/ `TRUNCATE TABLE shopping_list_products RESTART IDENTITY CASCADE`);
        await client.query(/*SQL*/ `TRUNCATE TABLE reviews RESTART IDENTITY CASCADE`);
        await client.query(/*SQL*/ `TRUNCATE TABLE review_replies RESTART IDENTITY CASCADE`);
        await client.query(/*SQL*/ `TRUNCATE TABLE shopping_cart_products RESTART IDENTITY CASCADE`);
        await client.query(/*SQL*/ `TRUNCATE TABLE orders RESTART IDENTITY CASCADE`);
        await client.query(/*SQL*/ `TRUNCATE TABLE order_products RESTART IDENTITY CASCADE`);

        let users: users[] = XLSX.utils.sheet_to_json(usersWS);
        let stores: stores[] = XLSX.utils.sheet_to_json(storesWS);
        let products: products[] = XLSX.utils.sheet_to_json(productsWS);
        let interactions: interactions[] = XLSX.utils.sheet_to_json(interactionsWS);
        let shopping_lists: shopping_lists[] = XLSX.utils.sheet_to_json(shopping_listsWS);
        let shopping_list_products: shopping_list_products[] = XLSX.utils.sheet_to_json(shopping_list_productsWS);
        let reviews: reviews[] = XLSX.utils.sheet_to_json(reviewsWS);
        let review_replies: review_replies[] = XLSX.utils.sheet_to_json(review_repliesWS);
        let shopping_cart_products: shopping_cart_products[] = XLSX.utils.sheet_to_json(shopping_cart_productsWS);
        let orders: orders[] = XLSX.utils.sheet_to_json(ordersWS);
        let order_products: order_products[] = XLSX.utils.sheet_to_json(order_productsWS);

        for (let user of users) {
            let image = user.image ? user.image : null;

            await client.query(
                `INSERT INTO users (name, email, password, image, address, contact_number, role, status) 
                    values ($1, $2, $3, $4, $5, $6, $7, $8)`,
                [user.name, user.email, user.password, image, user.address, user.contact_number, user.role, user.status]
            );
        }


        for (let store of stores) {
            let image = store.image ? store.image : null;
            let description = store.description ? store.description : null;

            await client.query(
                `INSERT INTO stores (user_id, name, image, description, status) 
                    values ($1, $2, $3, $4, $5)`,
                [store.user_id, store.name, image, description, store.status]
            );
        }

        for (let product of products) {
            let detail = product.detail ? product.detail : null;
          
            await client.query(
                `INSERT INTO products (store_id, title, description, variation, price, stock, images, detail, type, category, status)
                    values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)`,
                [product.store_id, product.title, product.description, product.variation, product.price, product.stock, product.images, detail, product.type, product.category, product.status]
            );
        }

        for (let interaction of interactions) {
            let product_id = interaction.product_id ? interaction.product_id : null;
            let store_id = interaction.store_id ? interaction.store_id : null;
            let is_viewed = interaction.is_viewed ? interaction.is_viewed : null;
            let is_saved = interaction.is_saved ? interaction.is_saved : null;
            let is_subscribed = interaction.is_subscribed ? interaction.is_subscribed : null;

            await client.query(
                `INSERT INTO interactions (user_id, product_id, store_id, is_viewed, is_saved, is_subscribed) 
                    values ($1, $2, $3, $4, $5, $6)`,
                [interaction.user_id, product_id, store_id, is_viewed, is_saved, is_subscribed]
            );
        }

        for (let shopping_list of shopping_lists) {
            
            await client.query(
                "INSERT INTO shopping_lists (user_id, name, status) values ($1, $2, $3)",
                [shopping_lists.user_id, shopping_lists.name, shopping_lists.status]
            );
        }


        for (let shopping_list_product of shopping_list_products) {

            await client.query(
                "INSERT INTO shopping_list_products (list_id, product_id) values ($1, $2)",
                [shopping_list_product.list_id, shopping_list_product.product_id]
            );
        }


        for (let review of reviews) {
            let content = review.content ? review.content : null;

            await client.query(
                `INSERT INTO reviews (user_id, product_id, rating, content, status) 
                    values ($1, $2, $3, $4, $5)`,
                [review.user_id, review.product_id, review.rating, content, review.status]
            );
        }


        for (let review_reply of review_replies) {

            await client.query(
                `INSERT INTO review_replies (user_id, review_id, content, status) 
                    values ($1, $2, $3, $4)`,
                [review_reply.user_id, review_reply.review_id, review_reply.content, review_reply.status]
            );
        }


        for (let shopping_cart_product of shopping_cart_products) {

            await client.query(
                `INSERT INTO shopping_cart_products (user_id, product_id) 
                    values ($1, $2)`,
                [shopping_cart_product.user_id, shopping_cart_product.product_id]
            );
        }


        for (let order of orders) {
            let paid_at = order.paid_at ? order.paid_at : null;
            let shipped_at = order.shipped_at ? order.shipped_at : null;

            await client.query(
                `INSERT INTO orders (user_id, store_id, amount, receiver, address, contact_number, status, paid_at, shipped_at)
                    values ($1, $2, $3, $4, $5, $6, $7, $8, $9)`,
                [order.user_id, order.store_id, order.amount, order.receiver, order.address, order.contact_number, order.status, paid_at, shipped_at]
            );
        }

        for (let order_product of order_products) {

            await client.query(
                `INSERT INTO order_products (order_id, product_id, image, variation, unit_price, qty) 
                    values ($1, $2, $3, $4, $5, $6)`,
                [order_product.order_id, order_product.product_id, order_product.image, order_product.variation, order_product.unit_price, order_product.qty]
            );
        }

        console.log('done')
    } catch (error) {
        console.log(error);
    } finally {
        await client.end(); // close connection with the database
    }
}
main();