import { Knex } from "knex";
import pg from "pg";
import XLSX from "XLSX";
import env from "../config/env";
import path from "path";

export interface users {
  name: string;
  email: string;
  password: string;
  image_id?: string;
  image_url?: string;
  address?: string;
  contact_number?: number;
  role: string;
  status: string;
  created_at: number;
  updated_at: number;
}

export interface stores {
  user_id: number;
  name: string;
  description?: string;
  status: string;
  created_at: number;
  updated_at: number;
}

export interface products {
  store_id: number;
  name: string;
  description?: string;
  images_url?: string[];
  images_id?: string[];
  detail?: string;
  type: string;
  category: JSON[];
  status: string;
  created_at: number;
  updated_at: number;
}

export interface product_variants {
  product_id: number;
  option: JSON;
  price: number;
  stock: number;
}

export interface interactions {
  user_id: number;
  product_id?: number;
  store_id?: number;
  is_viewed?: boolean;
  is_saved?: boolean;
  is_subscribed?: boolean;
  created_at: number;
  updated_at: number;
}

export interface wishlists {
  user_id: number;
  name: string;
  status: string;
  created_at: number;
  updated_at: number;
}

export interface wishlist_products {
  list_id: number;
  product_id: number;
}

export interface reviews {
  user_id: number;
  product_id: number;
  rating: number;
  content?: string;
  status: string;
  created_at: number;
  updated_at: number;
}

export interface review_replies {
  user_id: number;
  review_id: number;
  content: string;
  status: string;
  created_at: number;
  updated_at: number;
}

export interface shopping_cart {
  user_id: number;
  product_id: number;
  option: JSON;
  quantity: number;
}

export interface orders {
  user_id: number;
  store_id: number;
  amount: number;
  receiver: string;
  address: string;
  contact_number: number;
  status: string;
  paid_at?: any;
  shipped_at?: any;
  created_at: number;
  updated_at: number;
}

export interface order_products {
  order_id: number;
  product_id: number;
  product_name: string;
  image_url?: string;
  variant: JSON;
  unit_price: number;
  qty: number;
}

export async function seed(knex: Knex): Promise<void> {
  const client = new pg.Client({
    database: env.DB_NAME,
    user: env.DB_USERNAME,
    password: env.DB_PASSWORD,
  });

  try {
    await client.connect(); // "dial-in" to the postgres server
    let workbook = XLSX.readFile(
      path.resolve(path.join(__dirname, "./init_db.xlsx"))
    );

    let usersWS = workbook.Sheets["users"];
    let storesWS = workbook.Sheets["stores"];
    let productsWS = workbook.Sheets["products"];
    let product_variantsWS = workbook.Sheets["product_variants"];
    let interactionsWS = workbook.Sheets["interactions"];
    let wishlistsWS = workbook.Sheets["wishlists"];
    let wishlist_productsWS = workbook.Sheets["wishlist_products"];
    let reviewsWS = workbook.Sheets["reviews"];
    let review_repliesWS = workbook.Sheets["review_replies"];
    let shopping_cartWS = workbook.Sheets["shopping_cart"];
    let ordersWS = workbook.Sheets["orders"];
    let order_productsWS = workbook.Sheets["order_products"];

    await client.query(/*SQL*/ `TRUNCATE TABLE users RESTART IDENTITY CASCADE`);
    await client.query(
      /*SQL*/ `TRUNCATE TABLE stores RESTART IDENTITY CASCADE`
    );
    await client.query(
      /*SQL*/ `TRUNCATE TABLE products RESTART IDENTITY CASCADE`
    );
    await client.query(
      /*SQL*/ `TRUNCATE TABLE product_variants RESTART IDENTITY CASCADE`
    );
    await client.query(
      /*SQL*/ `TRUNCATE TABLE interactions RESTART IDENTITY CASCADE`
    );
    await client.query(
      /*SQL*/ `TRUNCATE TABLE wishlists RESTART IDENTITY CASCADE`
    );
    await client.query(
      /*SQL*/ `TRUNCATE TABLE wishlist_products RESTART IDENTITY CASCADE`
    );
    await client.query(
      /*SQL*/ `TRUNCATE TABLE reviews RESTART IDENTITY CASCADE`
    );
    await client.query(
      /*SQL*/ `TRUNCATE TABLE review_replies RESTART IDENTITY CASCADE`
    );
    await client.query(
      /*SQL*/ `TRUNCATE TABLE shopping_cart RESTART IDENTITY CASCADE`
    );
    await client.query(
      /*SQL*/ `TRUNCATE TABLE orders RESTART IDENTITY CASCADE`
    );
    await client.query(
      /*SQL*/ `TRUNCATE TABLE order_products RESTART IDENTITY CASCADE`
    );

    let users: users[] = XLSX.utils.sheet_to_json(usersWS);
    let stores: stores[] = XLSX.utils.sheet_to_json(storesWS);
    let products: products[] = XLSX.utils.sheet_to_json(productsWS);
    let product_variants: product_variants[] =
      XLSX.utils.sheet_to_json(product_variantsWS);
    let interactions: interactions[] = XLSX.utils.sheet_to_json(interactionsWS);
    let wishlists: wishlists[] = XLSX.utils.sheet_to_json(wishlistsWS);
    let wishlist_products: wishlist_products[] =
      XLSX.utils.sheet_to_json(wishlist_productsWS);
    let reviews: reviews[] = XLSX.utils.sheet_to_json(reviewsWS);
    let review_replies: review_replies[] =
      XLSX.utils.sheet_to_json(review_repliesWS);
    let shopping_cart: shopping_cart[] =
      XLSX.utils.sheet_to_json(shopping_cartWS);
    let orders: orders[] = XLSX.utils.sheet_to_json(ordersWS);
    let order_products: order_products[] =
      XLSX.utils.sheet_to_json(order_productsWS);

    for (let user of users) {
      await knex("users").insert({
        name: user.name,
        email: user.email,
        password: user.password,
        image_id: user.image_id,
        image_url: user.image_url,
        address: user.address,
        contact_number: user.contact_number,
        role: user.role,
        status: user.status,
      });
    }

    for (let store of stores) {
      await knex("stores").insert({
        user_id: store.user_id,
        name: store.name,
        description: store.description,
        status: store.status,
      });
    }

    for (let product of products) {
      await knex("products").insert({
        store_id: product.store_id,
        name: product.name,
        description: product.description,
        images_url: product.images_url,
        images_id: product.images_id,
        detail: product.detail,
        type: product.type,
        category: product.category,
        status: product.status,
      });
    }

    for (let product_variant of product_variants) {
      await knex("product_variants").insert({
        product_id: product_variant.product_id,
        option: product_variant.option,
        price: product_variant.price,
        stock: product_variant.stock,
      });
    }

    for (let interaction of interactions) {
      await knex("interactions").insert({
        user_id: interaction.user_id,
        product_id: interaction.product_id,
        store_id: interaction.store_id,
        is_viewed: interaction.is_viewed,
        is_saved: interaction.is_saved,
        is_subscribed: interaction.is_subscribed,
      });
    }

    for (let wishlist of wishlists) {
      await knex("wishlists").insert({
        user_id: wishlist.user_id,
        name: wishlist.name,
        status: wishlist.status,
      });
    }

    for (let wishlist_product of wishlist_products) {
      await knex("wishlist_products").insert({
        list_id: wishlist_product.list_id,
        product_id: wishlist_product.product_id,
      });
    }

    for (let review of reviews) {
      await knex("reviews").insert({
        user_id: review.user_id,
        product_id: review.product_id,
        rating: review.rating,
        content: review.content,
        status: review.status,
      });
    }

    for (let review_reply of review_replies) {
      await knex("review_replies").insert({
        user_id: review_reply.user_id,
        review_id: review_reply.review_id,
        content: review_reply.content,
        status: review_reply.status,
      });
    }

    for (let cart_item of shopping_cart) {
      await knex("shopping_cart").insert({
        user_id: cart_item.user_id,
        product_id: cart_item.product_id,
        option: cart_item.option,
        quantity: cart_item.quantity,
      });
    }

    for (let order of orders) {
      await knex("orders").insert({
        user_id: order.user_id,
        store_id: order.store_id,
        amount: order.amount,
        receiver: order.receiver,
        address: order.address,
        contact_number: order.contact_number,
        status: order.status,
        paid_at: order.paid_at,
        shipped_at: order.shipped_at,
      });
    }

    for (let order_product of order_products) {
      await knex("order_products").insert({
        order_id: order_product.order_id,
        product_id: order_product.product_id,
        product_name: order_product.product_name,
        image_url: order_product.image_url,
        variant: order_product.variant,
        unit_price: order_product.unit_price,
        qty: order_product.qty,
      });
    }

    console.log("done");
  } catch (error) {
    console.log(error);
  } finally {
    await client.end(); // close connection with the database
  }
}
