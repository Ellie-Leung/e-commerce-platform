import express from "express";
import env from "./config/env";
import setAppRoutes from "./routes/routes";
import fileUpload from "express-fileupload";
import http from "http";
import { Server as SocketIO } from "socket.io";
import cors from "cors";
import { setSocketIO } from "./utils/socketIO";

const app = express();

const server = new http.Server(app);
export const io = new SocketIO(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
});

setSocketIO(io);
app.use(cors());
app.use(express.json());
app.use(fileUpload());
app.use(express.urlencoded({ extended: true }));

setAppRoutes(app);

if (!env.SERVER_PORT) {
  throw new Error("SERVER_PORT is not defined");
}

server.listen(env.SERVER_PORT, () => {
  console.log(`Server is working on http://localhost:${env.SERVER_PORT}`);
});
