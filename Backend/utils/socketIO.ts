import socketIO from "socket.io";

let users: any[] = [];
  
const addUser = (userId: number, socketId: string) => {
  !users.some((user) => user.userId === userId) &&
    users.push({ userId, socketId });
};

const removeUser = (socketId: string) => {
  users = users.filter((user) => user.socketId !== socketId);
};

const getUser = (userId: number) => {
  return users.find((user) => user.userId === userId);
};

export function setSocketIO(value: socketIO.Server) {
    let io = value;
    io.on('connection',(socket)=>{
      console.log(`${socket.id} is connected`);

    socket.on("addUser", (userId: number) => {
      addUser(userId, socket.id);
      io.emit("getUsers", users);
    });

    socket.on("sendMessage", ({ senderId, receiverId, text }) => {
      // console.log(`io get msg: ${text} `)
      const user = getUser(receiverId);
console.log('user:', user)
      if(user) {
        io.to(user.socketId).emit("getMessage", {
          senderId,
          text,
        });
        // console.log(`io sent to user: ${text} `)
      }
    });
  
    socket.on("disconnect", () => {
      console.log(`${socket.id} is disconnected`);
      removeUser(socket.id);
      io.emit("getUsers", users);
    });
    });
}
