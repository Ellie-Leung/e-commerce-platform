import mongoose from "mongoose";

const MessageSchema = new mongoose.Schema(
    {
      chatId: {
        type: String,
      },
      senderId: {
        type: Number,
      },
      receiverId: {
        type: Number,
      },
      text: {
        type: String,
      },
    },
    { timestamps: true }
  );

  const Message = mongoose.model("Message", MessageSchema);
  
  export default Message