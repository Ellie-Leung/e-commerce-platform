import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("users", (table) => {
    table.increments("id").primary();
    table.string("name").notNullable();
    table.string("email").unique().notNullable();
    table.string("password").notNullable();
    table.string("image_id");
    table.string("image_url");
    table.text("address");
    table.integer("contact_number").unsigned();
    table.string("role").notNullable();
    table.string("status").notNullable();
    table.timestamps(false, true);
  });

  await knex.schema.createTable("stores", (table) => {
    table.increments("id").primary();
    table.integer("user_id").unsigned().notNullable();
    table.foreign("user_id").references("users.id");
    table.string("name").unique().notNullable();
    table.text("description");
    table.string("status").notNullable();
    table.timestamps(false, true);
  });

  await knex.schema.createTable("products", (table) => {
    table.increments("id").primary();
    table.integer("store_id").unsigned().notNullable();
    table.foreign("store_id").references("stores.id");
    table.string("name").notNullable();
    table.text("description");
    table.specificType("images_url", "text[]");
    table.specificType("images_id", "text[]");
    table.text("detail");
    table.string("type").notNullable();
    table.string("category").notNullable();
    table.string("status").notNullable();
    table.timestamps(false, true);
  });

  await knex.schema.createTable("product_variants", (table) => {
    table.increments("id").primary();
    table.integer("product_id").unsigned().notNullable();
    table.foreign("product_id").references("products.id");
    table.jsonb("option");
    table.decimal("price", 24, 2).notNullable();
    table.integer("stock").unsigned().notNullable();
  });

  await knex.schema.createTable("interactions", (table) => {
    table.increments("id").primary();
    table.integer("user_id").unsigned().notNullable();
    table.foreign("user_id").references("users.id");
    table.integer("product_id").unsigned();
    table.foreign("product_id").references("products.id");
    table.integer("store_id").unsigned();
    table.foreign("store_id").references("stores.id");
    table.boolean("is_viewed");
    table.boolean("is_saved");
    table.boolean("is_subscribed");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("wishlists", (table) => {
    table.increments("id").primary();
    table.integer("user_id").unsigned().notNullable();
    table.foreign("user_id").references("users.id");
    table.string("name").notNullable();
    table.string("status").notNullable();
    table.timestamps(false, true);
  });

  await knex.schema.createTable("wishlist_products", (table) => {
    table.increments("id").primary();
    table.integer("list_id").unsigned().notNullable();
    table.foreign("list_id").references("wishlists.id");
    table.integer("product_id").unsigned().notNullable();
    table.foreign("product_id").references("products.id");
  });

  await knex.schema.createTable("reviews", (table) => {
    table.increments("id").primary();
    table.integer("user_id").unsigned().notNullable();
    table.foreign("user_id").references("users.id");
    table.integer("product_id").unsigned().notNullable();
    table.foreign("product_id").references("products.id");
    table.decimal("rating", 2, 1).notNullable();
    table.text("content");
    table.string("status");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("review_replies", (table) => {
    table.increments("id").primary();
    table.integer("user_id").unsigned();
    table.foreign("user_id").references("users.id");
    table.integer("review_id").unsigned();
    table.foreign("review_id").references("reviews.id");
    table.text("content");
    table.string("status");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("shopping_cart", (table) => {
    table.increments("id").primary();
    table.integer("user_id").unsigned().notNullable();
    table.foreign("user_id").references("users.id");
    table.integer("product_id").unsigned().notNullable();
    table.foreign("product_id").references("products.id");
    table.jsonb("option").notNullable();
    table.integer("quantity").notNullable();
  });

  await knex.schema.createTable("orders", (table) => {
    table.increments("id").primary();
    table.integer("user_id").unsigned().notNullable();
    table.foreign("user_id").references("users.id");
    table.integer("store_id").unsigned().notNullable();
    table.foreign("store_id").references("stores.id");
    table.decimal("amount", 14, 2).notNullable();
    table.string("receiver").notNullable();
    table.text("address").notNullable();
    table.integer("contact_number").unsigned().notNullable();
    table.string("status").notNullable();
    table.dateTime("paid_at").nullable();
    table.timestamp("shipped_at").nullable();
    table.timestamps(false, true);
  });

  await knex.schema.createTable("order_products", (table) => {
    table.increments("id").primary();
    table.integer("order_id").unsigned().notNullable();
    table.foreign("order_id").references("orders.id");
    table.integer("product_id").unsigned().notNullable();
    table.foreign("product_id").references("products.id");
    table.string("product_name").notNullable();
    table.string("image_url");
    table.jsonb("variant").notNullable();
    table.decimal("unit_price", 14, 2).notNullable();
    table.integer("qty").unsigned();
  });
}

export async function down(knex: Knex): Promise<void> {
  // DROP TABLE "users"(
  knex.schema.dropTableIfExists("users");
  knex.schema.dropTableIfExists("stores");
  knex.schema.dropTableIfExists("products");
  knex.schema.dropTableIfExists("product_variants");
  knex.schema.dropTableIfExists("interactions");
  knex.schema.dropTableIfExists("wishlists");
  knex.schema.dropTableIfExists("wishlist_products");
  knex.schema.dropTableIfExists("reviews");
  knex.schema.dropTableIfExists("review_replies");
  knex.schema.dropTableIfExists("shopping_cart");
  knex.schema.dropTableIfExists("orders");
  knex.schema.dropTableIfExists("order_products");
}
