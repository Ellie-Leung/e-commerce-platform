import { Request, Response, NextFunction } from "express";
import { Bearer } from 'permit';
import jwtSimple from 'jwt-simple';
import env from '../config/env';
import { knex } from "../config/db";

const permit = new Bearer({
    query:"access_token"
})

interface RequestWithUserRole extends Request {
  user?: any,
}

export async function isAuthUser(req: RequestWithUserRole, res: Response, next: NextFunction) {
    try {
        const token = permit.check(req)

        if (!token) {
            return res.status(401).json({msg: "Please sign in to proceed."})
        }

        const payload = jwtSimple.decode(token, env.JWT_SECRET);
        const userId = payload.id
        const user = (await knex.table("users").where("id", userId))[0]

        console.log("token",token)
        console.log("payload", payload)
        
        if (!user) {
            return res.status(401).json({msg:"Permission Denied"});
        }

        req.user = {
            id: user.id,
            role: user.role,
            name: user.name
        }

        return next()

    } catch(e: any) {
        return res.status(401).json({msg: e.message});
    }
}


export function isAuthRole(role: string) {
  return async (req: RequestWithUserRole, res: Response, next: NextFunction) => {
    if (!role.includes(req.user.role)) {
      return res.status(403).json({msg:`Role: ${req.user.role} is not allowed to access this resource `})    
    }

    if (role == "store") {
      const storeId = (await knex.table("stores").select('id').where("user_id", req.user.id))[0]

      req.user = {
        ...req.user,
        store_id: storeId.id,
      }
    }
    next();
  };
};