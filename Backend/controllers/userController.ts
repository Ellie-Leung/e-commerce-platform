import { Request, Response } from "express";
import UserService from "../services/userService";
import jwtSimple from "jwt-simple";
import env from "../config/env";
import cloudinary from "cloudinary";

export default class UserController {
    
  constructor(private userService: UserService) {}

  // user register
  registerUser = async (req: Request, res: Response) => {
    try {
      let { name, email, password } = req.body;

      let user = await this.userService.userRegister(name, email, password)

      const payload = {
        id: user.id,
        name: user.name,
        email: user.email,
        role: user.role,
        created_at: user.created_at
      };

      const token = jwtSimple.encode(payload, env.JWT_SECRET);
      res.status(200).json({ token });
      
    } catch(e: any) {
      res.status(401).json({ error: e.message});
    }
  };
  

  // user login
  loginUser = async (req: Request, res: Response) => {
    try {
      const { email, password } = req.body;
  
      if (!email || !password) {
        res.status(400).json({error: "Please Enter Email & Password"});
        return
      }
      
     
      let user = await this.userService.userLogin(email, password);
    
      if (!user) {
        res.status(401).json({error: "Invalid email or password"});
        return
      } 
      
      const payload = {
        id: user.id,
        name: user.name,
        email: user.email,
        image_url: user.image_url,
        role: user.role,
        created_at: user.created_at,
      };

      const token = jwtSimple.encode(payload, env.JWT_SECRET);
      res.status(200).json({ token });
      
      
    } catch(e: any) {
      res.status(401).json({error: e.message});
    }
  };

  // update User password
updatePassword = async (req: any, res: Response) => {
  try {
    let userId = req.user.id
    let oldPassword = req.body.oldPassword;
    let newPassword = req.body.newPassword;
    let confirmPassword = req.body.confirmPassword;
    
    const user = await this.userService.userById(userId);

    const isPasswordMatched = await user.password == oldPassword;

    if (!isPasswordMatched) {
      res.status(400).json({error: "Old password is incorrect"});
      return
    }

    if (newPassword !== confirmPassword) {
      res.status(400).json({error: "Password does not match"});
      return
    }

    await this.userService.passwordUpdate(userId, newPassword);

    res.status(200).json({msg: "Password updated successfully"});

  } catch(e: any) {
    res.status(401).json({error: e.message});
  }
};

// update User Profile
updateProfile = async (req: any, res: Response) => {
  try {
    let userId = req.user.id

    const user = await this.userService.userById(userId);

    const newUserData = {
      name: req.body.name,
      email: req.body.email,
      image_id: user.image_id,
      image_url: user.image_url,
    };

    if (req.body.avatar !== "") {

      const imageId = user.image_id;
      
      if (imageId !== "") {
        await cloudinary.v2.uploader.destroy(imageId);
      }
     
      const myCloud = await cloudinary.v2.uploader.upload(req.body.avatar, {
        folder: "avatars",
        width: 150,
        crop: "scale",
      });

      newUserData.image_id = myCloud.public_id
      newUserData.image_url = myCloud.secure_url

    };

    await this.userService.userProfileUpdate(userId, newUserData);

    res.status(200).json({
      success: true,
      msg: "User Profile Updated"
    });

  } catch(e: any) {
    res.status(401).json({error: e.message});
  }
};
  
// Get single user (admin)
getSingleUser = async (req: Request, res: Response) => {
  try {
    let userId = req.params.id

    const user = await this.userService.userById(Number(userId));

    if (!user) {
      res.status(400).json({error: `User does not exist with Id: ${userId}`});
      return
    }

    res.status(200).json({
      success: true,
      user,
    });

  } catch(e: any) {
    res.status(401).json({error: e.message});
  }
};

// update User Role -- Admin
updateUserRole = async (req: Request, res: Response) => {
  try {
    let userId = req.params.id
    const newUserData = {
      name: req.body.name,
      email: req.body.email,
      role: req.body.role,
    };

    const user = await this.userService.userById(Number(userId));

    if (!user) {
      return res.status(400).json({error: `User does not exist with Id: ${req.params.id}`});
    }

    await this.userService.userRoleUpdate(Number(userId), newUserData);

    res.status(200).json({
      success: true,
      message: "User Status Updated Successfully",
    });

  } catch(e: any) {
    res.status(401).json({error: e.message});
  }
};

// Delete User --Admin
deleteUser = async (req: Request, res: Response) => {
  try {
    let userId = req.params.id
    let newStatus = "inactive"

    const user = await this.userService.userById(Number(userId));

   if (!user) {
      res.status(400).json({error: `User does not exist with Id: ${req.params.id}`});
      return
    }

    // const imageId = user.avatar.public_id;

    // await cloudinary.v2.uploader.destroy(imageId);

    await this.userService.userStatusUpdate(Number(userId), newStatus);

    res.status(200).json({
      success: true,
      message: "User Deleted Successfully",
    });

  } catch(e: any) {
    res.status(401).json({error: e.message});
  }
};

// Get User Detail
getUserDetails = async (req: any, res: Response) => {
  try {
    let userId = req.user.id

  const user = await this.userService.userById(userId);

  res.status(200).json({
    success: true,
    user,
  });
} catch(e: any) {
  res.status(401).json({error: e.message});
}
};

// // Forgot Password
// forgotPassword = async (req: Request, res: Response) => {
//   let email = req.body.email

//   const user = await this.userService.userByEmail(email);

//   if (!user) {
//     return res.status(404).json({error: "User not found"});
//   }

//   // // Get ResetPassword Token
//   // const resetToken = user.getResetPasswordToken();

//   // await user.save({ validateBeforeSave: false });

//   // const resetPasswordUrl = `${req.protocol}://${req.get(
//   //   "host"
//   // )}/password/reset/${resetToken}`;

//   // const message = `Your password reset token is :- \n\n ${resetPasswordUrl} \n\nIf you have not requested this email then, please ignore it.`;

//   try {
//     await sendEmail({
//       email: user.email,
//       subject: `Ecommerce Password Recovery`,
//       message,
//     });

//     res.status(200).json({
//       success: true,
//       message: `Email sent to ${user.email} successfully`,
//     });
//   } catch (error) {
//     // user.resetPasswordToken = undefined;
//     // user.resetPasswordExpire = undefined;

//     // await user.save({ validateBeforeSave: false });

//     return res.status(500).json({error: error.message});
//   }
// };

// // Reset Password
// resetPassword = async (req: Request, res: Response) => {
//   // creating token hash
//   const resetPasswordToken = crypto
//     .createHash("sha256")
//     .update(req.params.token)
//     .digest("hex");

//   const user = await User.findOne({
//     resetPasswordToken,
//     resetPasswordExpire: { $gt: Date.now() },
//   });

//   if (!user) {
//     return res.status(400).json({error: "Reset Password Token is invalid or has been expired"});
//   }

//   if (req.body.password !== req.body.confirmPassword) {
//     return res.status(400).json({error: "Password does not password"});
//   }

//   user.password = req.body.password;
//   user.resetPasswordToken = undefined;
//   user.resetPasswordExpire = undefined;

//   await user.save();

//   sendToken(user, 200, res);
// };

// Get all users(admin)
getAllUsers = async (req: Request, res: Response) => {
  try{
  const users = await this.userService.allUsers();

  res.status(200).json({
    success: true,
    users,
  });

} catch(e: any) {
   res.status(401).json({ error: e.message});
}
};


  //get chat member
  getMember = async (req: Request, res: Response) => {
    try {
        const userId = req.query.id

        const member = await this.userService.memberById(Number(userId));

        res.status(200).json({
          success: true,
          member,
        });
      
    } catch (err: any) {
      res.status(401).json({ error: err.message});
    }
  };

}