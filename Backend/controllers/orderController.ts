import { Request, Response } from "express";
import OrderService from "../services/orderService";
// import ErrorHandler from "../utils/errorHandler";

export default class OrderController {
  
  constructor(private orderService: OrderService) {}

  // // create new order --OK
  // newOrder = async (req: Request, res: Response, next: NextFunction) => {
  //   try {
  //     let order = req.body;

  //     let order = await this.orderService.createNewOrder(order);
  //       res.status(201).json({
  //         success: true,
  //         order,
  //       });
  //   } catch(e: any) {
  //         return next(new ErrorHandler(e.message, 401))
  //   }
  // };
    
  // get order detail --OK
  orderDetail = async (req: Request, res: Response) => {
    try {
      let orderId = req.params.id

      let order = await this.orderService.getOrderDetail(Number(orderId));

      if (!order) {
        res.status(404).json({error: "Order not found with this Id"});
        return
      }
    
      res.status(200).json({
        success: true,
        order,
      });

    } catch(e: any) {
      res.status(401).json({error: e.message});
    }
  };
    
    // get user orders --OK
    myOrders = async (req: any, res: Response) => {
      try {
        let userId = req.user.id

        let orders = await this.orderService.getMyOrders(Number(userId));
        // console.log(orders)
        res.status(200).json({
          success: true,
          orders,
        });
        
      } catch(e: any) {
        res.status(401).json({error: e.message});
      }
    };
    
    // get all orders (admin) --OK
    allOrders = async (req: Request, res: Response) => {
      try {
        let orders = await this.orderService.getAllOrders();
    
        let totalAmount = 0;
    
        orders.forEach((order: any) => {
          totalAmount += order.amount;
        });
    
        res.status(200).json({
          success: true,
          totalAmount,
          orders,
        });
      } catch(e: any) {
        res.status(401).json({error: e.message});
      }
    };
    
    // update order status (admin)
    updateOrder = async (req: Request, res: Response) => {
      try {
        let orderId = req.params.id

        let order = await this.orderService.orderUpdate(Number(orderId));
    
        if (!order) {
          res.status(404).json({error: "Order not found with this Id"});
          return
        }
    
        if (order.status === "Delivered") {
          res.status(400).json({error: "This order is already delivered"});
          return
        }
    
        if (req.body.status === "Shipped") {
          order.orderItems.forEach(async (o: any) => {
            await this.updateStock(o.id, o.quantity);
          });
        }
        order.orderStatus = req.body.status;
    
        if (req.body.status === "Delivered") {
          order.deliveredAt = Date.now();
        }
    
        await order.save({ validateBeforeSave: false });
        res.status(200).json({
          success: true,
        });
      } catch(e: any) {
        res.status(401).json({error: e.message});
      }
      
    };
    
    // update stock
    updateStock = async (id: number, quantity: number) => {

        const product = await this.orderService.getOrderProduct(id);
    
        product.Stock -= quantity;
    
        // await product.save({ validateBeforeSave: false });
      
    } 
    

}