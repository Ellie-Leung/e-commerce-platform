import { Request, Response } from "express";
import ProductService from "../services/productService";
import cloudinary from "cloudinary";

export default class ProductController {
  constructor(private productService: ProductService) {}

  // get all products
  getAllProducts = async (req: Request, res: Response) => {
    try {
      let resultPerPage = 15;
      let query = req.query;
      // console.log(query)
      let products = await this.productService.allPublicProducts(query);
      // console.log(products)
      let productsCount = products.length;

      res.status(200).json({
        success: true,
        products,
        productsCount,
        resultPerPage,
      });
    } catch (e: any) {
      res.status(401).json({ error: e.message });
    }
  };

  // get all product(admin)
  getAdminProducts = async (req: Request, res: Response) => {
    try {
      let products = await this.productService.allProducts();

      res.status(200).json({
        success: true,
        products,
      });
    } catch (e: any) {
      res.status(401).json({ error: e.message });
    }
  };

  // get all public products (store)
  getStoreProducts = async (req: Request, res: Response) => {
    try {
      let resultPerPage = 15;
      let storeId = Number(req.params.id);

      const products = await this.productService.productByStore(storeId);
      let productsCount = products.length;

      res.status(200).json({
        success: true,
        products,
        productsCount,
        resultPerPage,
      });
    } catch (e: any) {
      res.status(401).json({ error: e.message });
    }
  };

  // get all products (store)
  getAllStoreProducts = async (req: Request, res: Response) => {
    try {
      let resultPerPage = 15;
      let storeId = Number(req.params.id);

      const products = await this.productService.productByStore(storeId);
      let productsCount = products.length;

      res.status(200).json({
        success: true,
        products,
        productsCount,
        resultPerPage,
      });
    } catch (e: any) {
      res.status(401).json({ error: e.message });
    }
  };

  // get product details
  getProductDetails = async (req: Request, res: Response) => {
    try {
      let product_id = Number(req.params.id);
      const product = await this.productService.productDetailsById(product_id);

      if (!product) {
        res.status(404).json({
          error: "Product not found",
        });
        return;
      }

      res.status(200).json({
        success: true,
        product,
      });
    } catch (e: any) {
      res.status(401).json({ error: e.message });
    }
  };

  // get cart items
  getCartItems = async (req: any, res: Response) => {
    try {
      let userId = req.user.id;
      const cartItems = await this.productService.cartItemById(userId);

      if (!cartItems) {
        res.status(404).json({
          msg: "No cart item found",
        });
        return;
      }

      res.status(200).json({
        success: true,
        cartItems,
      });
    } catch (e: any) {
      res.status(401).json({ error: e.message });
    }
  };

  // Create Product (store)
  createProduct = async (req: any, res: Response) => {
    try {
      let images_url: string[] = [];
      let images_id: string[] = [];

      if (typeof req.body.images_url === "string") {
        images_url.push(req.body.images_url);
        req.body.images_url = images_url;
      }

      if (typeof req.body.images_id === "string") {
        images_id.push(req.body.images_id);
        req.body.images_id = images_id;
      }

      console.log("images_url:", images_url);

      req.body.user = req.user.id;
      req.body.store = req.user.store_id;
      console.log("req.body:", req.body);
      const product = await this.productService.newProduct(req.body);
      console.log("product:", product);
      res.status(201).json({
        success: true,
        product,
      });
    } catch (e: any) {
      res.status(401).json({ error: e.message });
    }
  };

  // update product (admin)
  updateProduct = async (req: Request, res: Response) => {
    try {
      let product = await this.productService.productById(
        Number(req.params.id)
      );

      if (!product) {
        return res.status(404).json({ error: "Product not found" });
      }

      let images_url: string[] = [];

      if (typeof req.body.images_url === "string") {
        images_url.push(req.body.images_url);
      } else {
        images_url = req.body.images_url;
      }

      if (images_url !== undefined) {
        for (let i = 0; i < product.images.length; i++) {
          await cloudinary.v2.uploader.destroy(product.images[i].public_id);
        }

        const imagesLinks: any[] = [];

        for (let i = 0; i < images_url.length; i++) {
          const result = await cloudinary.v2.uploader.upload(images_url[i], {
            folder: "products",
          });

          imagesLinks.push({
            public_id: result.public_id,
            url: result.secure_url,
          });
        }

        req.body.images = imagesLinks;
      }

      product = await this.productService.updateProductInfo(
        Number(req.params.id),
        req.body,
        {
          new: true,
          runValidators: true,
          useFindAndModify: false,
        }
      );

      res.status(200).json({
        success: true,
        product,
      });
    } catch (e: any) {
      return res.status(401).json({ error: e.message });
    }
  };

  // delete product
  updateStoreProduct = async (req: Request, res: Response) => {
    try {
      const product = await this.productService.productById(
        Number(req.params.id)
      );

      if (!product) {
        return res.status(404).json({ error: "Product not found" });
      }

      for (let i = 0; i < product.images.length; i++) {
        await cloudinary.v2.uploader.destroy(product.images[i].public_id);
      }

      await this.productService.updateProduct(Number(req.params.id));

      res.status(200).json({
        success: true,
        message: "Product Delete Successfully",
      });
    } catch (e: any) {
      return res.status(401).json({ error: e.message });
    }
  };

  // update cart item
  updateCartItem = async (req: any, res: Response) => {
    try {
      // console.log("update cart data:", req.body)
      let { itemId, newQty } = req.body;

      let cartItem = await this.productService.updateCart(itemId, newQty);

      res.status(200).json({
        success: true,
        cartItem,
      });
    } catch (e: any) {
      res.status(401).json({ error: e.message });
    }
  };

  // add cart item
  addCartItem = async (req: any, res: Response) => {
    try {
      // console.log("add to cart data:", req.body)
      let { productId, option, quantity } = req.body;
      let userId = req.user.id;
      // console.log("add to cart userId:", userId)
      let cartId = await this.productService.addToCart(
        userId,
        productId,
        option,
        quantity
      );
      //  console.log("returned cartId:", cartId)
      res.status(200).json({
        success: true,
        cartId,
      });
    } catch (e: any) {
      res.status(401).json({ error: e.message });
    }
  };

  // remove cart item
  removeCartItem = async (req: Request, res: Response) => {
    try {
      let itemId = req.params.id;
      // console.log(itemId)

      await this.productService.removeFromCart(Number(itemId));

      res.status(200).json({
        success: true,
        msg: "successfully removed from cart list",
      });
    } catch (e: any) {
      return res.status(401).json({ error: e.message });
    }
  };

  // get reviews by product id
  getProductReviews = async (req: Request, res: Response) => {
    try {
      let productId = Number(req.query.id);
      const reviews = await this.productService.reviewsByProductId(productId);

      if (!reviews) {
        res.status(404).json({
          error: "Product not found",
        });
        return;
      }

      res.status(200).json({
        success: true,
        reviews,
      });
    } catch (e: any) {
      res.status(401).json({ error: e.message });
    }
  };

  // Create New Review or Update the review
  createProductReview = async (req: any, res: Response) => {
    try {
      const { rating, comment, productId } = req.body;

      const review = {
        user: req.user.id,
        name: req.user.name,
        rating: Number(rating),
        comment,
      };

      const product = await this.productService.productById(productId);

      const isReviewed = product.reviews.find(
        (review: any) => review.user.toString() === req.user.id.toString()
      );

      if (isReviewed) {
        product.reviews.forEach((review: any) => {
          if (review.user.toString() === req.user._id.toString())
            (review.rating = rating), (review.comment = comment);
        });
      } else {
        product.reviews.push(review);
        product.numOfReviews = product.reviews.length;
      }

      let avg = 0;

      product.reviews.forEach((review: any) => {
        avg += review.rating;
      });

      product.ratings = avg / product.reviews.length;

      await product.save({ validateBeforeSave: false });

      res.status(200).json({
        success: true,
      });
    } catch (e: any) {
      return res.status(401).json({ error: e.message });
    }
  };

  // delete review
  updateReview = async (req: Request, res: Response) => {
    try {
      const product = await this.productService.productById(
        Number(req.query.productId)
      );

      if (!product) {
        return res.status(401).json({ error: "Product not found" });
      }

      const reviews = product.reviews.filter(
        (review: any) => review.id !== req.query.id
      );

      let avg = 0;

      reviews.forEach((review: any) => {
        avg += review.rating;
      });

      let ratings = 0;

      if (reviews.length === 0) {
        ratings = 0;
      } else {
        ratings = avg / reviews.length;
      }

      const numOfReviews = reviews.length;

      await this.productService.updateProductInfo(
        Number(req.query.productId),
        {
          reviews,
          ratings,
          numOfReviews,
        },
        {
          new: true,
          runValidators: true,
          useFindAndModify: false,
        }
      );

      res.status(200).json({
        success: true,
      });
    } catch (e: any) {
      return res.status(401).json({ error: e.message });
    }
  };
}
