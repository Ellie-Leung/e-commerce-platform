import { Request, Response } from "express";
import env from '../config/env';
const stripe = require("stripe")(env.STRIPE_SECRET_KEY);

export default class PaymentController {

  constructor() {}

  processPayment = async (req: Request, res: Response) => {
    try {
      const myPayment = await stripe.paymentIntents.create({
        amount: req.body.amount,
        currency: "hkd",
        metadata: {
            company: "Jamkel",
        },
      });
      
      res
        .status(200)
        .json({ success: true, client_secret: myPayment.client_secret });
    } catch(e: any) {
      return res.status(401).json({error: e.message})
    }
  };
    
  sendStripeApiKey = async (req: Request, res: Response) => {
    try {
      res.status(200).json({ stripeApiKey: env.STRIPE_API_KEY });
    } catch(e: any) {
      return res.status(401).json(e.message)
    }
  };


}