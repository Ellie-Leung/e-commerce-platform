import { Request, Response } from "express";
import ChatService from "../services/chatService";

export default class ChatController {
  constructor(private chatService: ChatService) {}

  //new chat
  newChat = async (req: Request, res: Response) => {
    try {
      const senderId = req.body.senderId;
      const receiverId = req.body.receiverId;

      const savedChat = await this.chatService.saveNewChat(senderId, receiverId);

      res.status(200).json(savedChat);
    } catch (err) {
      res.status(500).json(err);
    }
  };

  //get all chats of a user
  getAllChats = async (req: Request, res: Response) => {
    try {
      const userId = Number(req.params.id);

      const chats = await this.chatService.chatsByUser(userId);

      res.status(200).json(chats);
    } catch (err) {
      res.status(500).json(err);
    }
  };

  // get chat
  getChat = async (req: Request, res: Response) => {
    try {
      const firstUserId = req.params.firstUserId;
      const secondUserId = req.params.secondUserId;

      const chat = await this.chatService.chatByUsers(Number(firstUserId), Number(secondUserId));

      res.status(200).json(chat);
    } catch (err) {
      res.status(500).json(err);
    }
  };

  //add new msg
  newMessage = async (req: Request, res: Response) => {
    try {
      // console.log(req.body)
      const savedMessage = await this.chatService.saveMessage(req.body);
      res.status(200).json(savedMessage);
    } catch (err) {
      res.status(500).json(err);
    }
  };

  //get msg
  getMessages = async (req: Request, res: Response) => {
    try {
      let chatId = req.params.id;

      const messages = await this.chatService.messagesById(chatId);

      res.status(200).json(messages);
    } catch (err) {
      res.status(500).json(err);
    }
  };
}
