import Knex from 'knex'
import mongoose from "mongoose"
import env from './env';

const knexConfigs = require("../knexfile");
const configMode = env.NODE_ENV;
const knexConfig = knexConfigs[configMode];
export const knex = Knex(knexConfig);


mongoose.connect(env.MONGO_URL).then(() => {
    console.log("Connected to MongoDB");
}).catch((err: Error) => {
    console.log('MongoDB connection error.' + err)
    process.exit();
})