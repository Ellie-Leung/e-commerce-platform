import Cloudinary from "cloudinary" 
import env from './env';

const cloudinary = Cloudinary.v2;

cloudinary.config({
  cloud_name: env.CLOUDINARY_NAME,
  api_key: env.CLOUDINARY_API_KEY,
  api_secret: env.CLOUDINARY_API_SECRET,
})