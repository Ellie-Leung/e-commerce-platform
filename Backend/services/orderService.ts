import type { Knex } from "knex";

export default class OrderService {
    constructor(private knex: Knex) { }

    // async createNewOrder(): Promise<any> {
    //     const order = await Order.create({
    //         shippingInfo,
    //         orderItems,
    //         paymentInfo,
    //         itemsPrice,
    //         taxPrice,
    //         shippingPrice,
    //         totalPrice,
    //         paidAt: Date.now(),
    //         user: req.user._id,
    //       });
    // }

    async getOrderDetail(orderId: number): Promise<any> {
      return (await this.knex('orders')
      .leftJoin('order_products', 'orders.id', 'order_products.order_id')
      .where('orders.id', orderId)
      )[0]
    }


    async getMyOrders(userId: number): Promise<any> {
      return (await this.knex('orders')
      .select("orders.*", "order_products.*")
      .leftJoin('order_products', 'orders.id', 'order_products.order_id')
      .where('orders.user_id', userId)
      
      )
  }


    async getAllOrders(): Promise<any> {
      return (await this.knex('orders')
      .select("orders.*", "order_products.*")
      .leftJoin('order_products', 'orders.id', 'order_products.order_id') 
      // .groupBy('orders.id', 'order_products.id')
      )
    }

    async orderUpdate(orderId: number): Promise<any> {
      return (await this.knex('orders')
      .select("orders.*", "order_products.*")
      .leftJoin('order_products', 'orders.id', 'order_products.order_id') 
      .where('orders.id', orderId)
      )
    }

    async getOrderProduct(id: number): Promise<any> {
      return (await this.knex('product_variants')
      .where('product_variants.id', id)
      )
    }
    


    

}