import type { Knex } from "knex";

export default class ProductService {
  constructor(private knex: Knex) {}

  async allProducts(): Promise<any> {
    return await this.knex("products")
      .select("products.*", "product_variants.*")
      .leftJoin("product_variants", "products.id", "product_variants.product_id")
      .groupBy("products.id", "product_variants.id");
  }

  async allPublicProducts(query: any): Promise<any> {
    let result = this.knex
      .select(
        this.knex.raw("ROUND(AVG(reviews.rating),1) AS avg_rating"),
        "products.*",
        "stores.name as store_name",
        "stores.id as store_id",
        "product_variants.price"
      )
      .from("products")
      .count("reviews as numOfReviews")
      .leftJoin("reviews", "products.id", "reviews.product_id")
      .leftJoin("stores", "products.store_id", "stores.id")
      .leftJoin("product_variants", "products.id", "product_variants.product_id")
      .groupBy("products.id", "stores.name", "stores.id", "product_variants.price")
      .where({ "products.status": "public" });

    if (query.search) {
      let keywords = query.search;
      const keywordArray = keywords.split("+").filter((word: any) => word != "");
      keywordArray.forEach((word: string) => result.where("products.name", "ilike", `%${word}%`));
    }

    if (query.category) {
      result.where("products.category", "ilike", `%${query.category}%`);
    }

    if (query.priceflr) {
      result.where("product_variants.price", ">=", query.priceflr);
    }

    if (query.priceceil) {
      result.where("product_variants.price", "<", query.priceceil);
    }

    // if (query.ratingflr) {
    //   result.where("avg_rating", ">=", query.ratingflr);
    // }

    // if(query.ratingceil) {
    //     result.where('avg_rating', '<', query.ratingceil)
    // }

    const currentPage = Number(query.page) || 1;
    let resultPerPage = 15;
    const offset = resultPerPage * (currentPage - 1);
    result.limit(resultPerPage).offset(offset);

    let a = await result;

    if (query.ratingflr) {
      a = a.filter((value) => {
        return value.avg_rating >= query.ratingflr;
      });
    }

    if (query.ratingceil) {
      a = a.filter((value) => {
        return value.avg_rating <= query.ratingceil;
      });
    }

    return a;
  }

  async productByStore(storeId: number): Promise<any> {
    return await this.knex("products")
      .select("products.*", "product_variants.option", "product_variants.price", "product_variants.stock", "stores.name as store_name", "stores.id as store_id")
      .avg("reviews.rating as avg_rating")
      .count("reviews as numOfReviews")
      .leftJoin("reviews", "products.id", "reviews.product_id")
      .leftJoin("product_variants", "products.id", "product_variants.product_id")
      .leftJoin("stores", "products.store_id", "stores.id")
      .groupBy("products.id", "product_variants.id")
      .where({ "products.store_id": storeId })
      .where({ "products.status": "public" })
      .groupBy("products.id", "stores.name", "stores.id", "product_variants.id", "reviews.id");
  }

  async productDetailsById(product_id: number): Promise<any> {
    return (
      await this.knex("products")
        .select("products.*", "stores.name as store_name", "product_variants.option", "product_variants.price", "product_variants.stock")
        .avg("reviews.rating as avg_rating")
        .count("reviews as numOfReviews")
        .leftJoin("stores", "products.store_id", "stores.id")
        .leftJoin("reviews", "products.id", "reviews.product_id")
        .leftJoin("product_variants", "products.id", "product_variants.product_id")
        .where("products.id", product_id)
        .groupBy("products.id", "stores.name", "product_variants.id", "reviews.id")
    )[0];
  }

  async reviewsByProductId(product_id: number): Promise<any> {
    return await this.knex("reviews")
      .select("reviews.*", "users.name as user_name", "users.image_url as user_img")
      .leftJoin("users", "reviews.user_id", "users.id")
      .where("reviews.product_id", product_id);
  }

  async cartItemById(userId: number): Promise<any> {
    return await this.knex("shopping_cart")
      .select(
        "shopping_cart.*",
        "stores.name as store_name",
        "products.name as name",
        "products.images_url as images",
        "product_variants.price as price",
        "product_variants.stock as stock"
      )
      .where("shopping_cart.user_id", userId)
      .leftJoin("products", "shopping_cart.product_id", "products.id")
      .leftJoin("product_variants", "products.id", "product_variants.product_id")
      .leftJoin("stores", "products.store_id", "stores.id");
  }

  async newProduct(data: any): Promise<any> {
    const productId = (
      await this.knex("products")
        .insert({
          store_id: data.store,
          name: data.name,
          description: data.description,
          images_url: data.images_url,
          images_id: data.images_id,
          type: "product",
          category: data.category,
          status: "public",
        })
        .returning("id as productId")
    )[0].productId;

    const value = {
      product_id: productId,
      option: JSON.stringify({ DEFAULT: "DEFAULT" }),
      price: data.price,
      stock: data.stock,
    };
    await this.knex("product_variants").insert(value);
  }

  async productById(productId: number): Promise<any> {
    return await this.knex();
  }

  async updateProductInfo(id: number, data: any, status: any): Promise<any> {
    return await this.knex("products");
  }

  async updateProduct(id: number): Promise<any> {
    return await this.knex("products");
  }

  async updateCart(itemId: number, newQty: number): Promise<any> {
    return await this.knex("shopping_cart").where({ id: itemId }).update({ quantity: newQty });
  }

  async addToCart(userId: number, productId: number, option: any, newQty: number): Promise<any> {
    return await this.knex("shopping_cart")
      .insert({
        user_id: userId,
        product_id: productId,
        option,
        quantity: newQty,
      })
      .returning("id as cartId");
  }

  async removeFromCart(itemId: number): Promise<any> {
    return await this.knex("shopping_cart").where({ id: itemId }).del();
  }
}
