import Chat from"../models/Chat"
import Message from "../models/Message";

export default class ChatService {

    constructor() {}

async saveNewChat(senderId: number, receiverId: number): Promise<any> {
    const newChat = new Chat({
        members: [senderId, receiverId],
      });

    return await newChat.save()
}

async chatsByUser(userId: number): Promise<any> {
   
    return await   Chat.find({
        members: { $in: [userId] },
      });
}

async chatByUsers(firstUserId: number, secondUserId: number): Promise<any> {
    return await Chat.findOne({
        members: { $all: [firstUserId, secondUserId] },
      });
}
  

async saveMessage(msg: string): Promise<any> {
    // console.log(msg)
    const newMessage = new Message(msg);
    return await newMessage.save();
}
  
  
async messagesById(chatId: string): Promise<any> {
  
    return await Message.find({
        chatId: chatId,
      });
}





}