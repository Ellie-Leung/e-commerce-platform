import type { Knex } from "knex";

export default class UserService {
    constructor(private knex: Knex) { }
   
    async userRegister(name: string, email: string, password: string): Promise<any> {

      return (await this.knex('users')
      .insert({name: name, email: email, password: password, role: 'user', status: 'active'})
      )
    };

    async userLogin(email: string, password: string): Promise<any> {
        return (await this.knex
              .table("users")
              .where("email", email)
              .where("password", password)
          )[0];

    }

    async allUsers(): Promise<any> {
      return (await this.knex("users"));
    }


    async userById(id: number): Promise<any> {
      return (await this.knex
            .table("users")
            .where("id", id)
        )[0];
    }

    async memberById(userId: number): Promise<any> {
      return (await this.knex
          .select("id", "name", "email", "image_url")
          .table("users")
          .where("id", userId)
      )[0];
  }

    async userByEmail(email: string): Promise<any> {
      return (await this.knex
            .table("users")
            .where("email", email)
        )[0];
    }

    async passwordUpdate(id: number, password: string): Promise<any> {
      return (await this.knex
            .table("users")
            .where("id", id)
            .update("password", password)
        )
    }


    async userRoleUpdate(id: number, newData: any): Promise<any> {
      return (await this.knex
            .table("users")
            .where("id", id)
            .update({name: newData.name, email:newData.email, role:newData.role})
        )
    }

    async userStatusUpdate(id: number, status: string): Promise<any> {
      return (await this.knex
            .table("users")
            .where("id", id)
            .update("status", status)
        )
    }

    async userProfileUpdate(id: number, newData: any): Promise<any> {
      return (await this.knex
            .table("users")
            .where("id", id)
            .update({name: newData.name, email:newData.email, image_id:newData.image_id, image_url:newData.image_url})
        )
    }
    


    

}