import express from "express";
import type PaymentController from '../controllers/paymentController'
import { isAuthUser } from '../middlewares/auth'

export function createPaymentRoutes(paymentController: PaymentController) {

    let paymentRoutes = express.Router();

    paymentRoutes.post("/payment", isAuthUser, paymentController.processPayment);

    paymentRoutes.get("/stripeapikey", isAuthUser, paymentController.sendStripeApiKey);

    return paymentRoutes
}