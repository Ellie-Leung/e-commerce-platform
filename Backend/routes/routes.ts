import type { Express } from 'express'
import { knex } from "../config/db";

import { createUserRoutes } from "./userRoutes";
import UserController from "../controllers/userController";
import UserService from "../services/userService";
import { createProductRoutes } from "./productRoutes"
import ProductService from "../services/productService";
import ProductController from "../controllers/productController";
import { createOrderRoutes } from "./orderRoutes"
import OrderService from "../services/orderService";
import OrderController from "../controllers/orderController";
import { createPaymentRoutes } from "./paymentRoutes";
import PaymentController from "../controllers/paymentController";
import { createChatRoutes } from "./chatRoutes";
import ChatController from "../controllers/chatController";
import ChatService from "../services/chatService";


export default function setAppRoutes(app: Express) {

    let userService = new UserService(knex);
    let userController = new UserController(userService);
    let productService = new ProductService(knex);
    let productController = new ProductController(productService);
    let orderService = new OrderService(knex);
    let orderController = new OrderController(orderService);
    let paymentController = new PaymentController();
    let chatService = new ChatService();
    let chatController = new ChatController(chatService);

    app.use(createUserRoutes(userController))
    app.use(createProductRoutes(productController))
    app.use(createOrderRoutes(orderController))
    app.use(createPaymentRoutes(paymentController))
    app.use(createChatRoutes(chatController))

}