import express from "express";
import type ChatController from "../controllers/chatController";

export function createChatRoutes(chatController: ChatController) {
  let chatRoutes = express.Router();

  chatRoutes.post("/chat", chatController.newChat);
  chatRoutes.get("/chats/:id", chatController.getAllChats);
  chatRoutes.get("/chat/:id", chatController.getChat);
  chatRoutes.post("/message", chatController.newMessage);
  chatRoutes.get("/messages/:id", chatController.getMessages);

  return chatRoutes;
}
