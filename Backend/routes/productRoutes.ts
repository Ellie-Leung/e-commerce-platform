import express from "express";
import type ProductController from '../controllers/productController'
import { isAuthUser, isAuthRole } from '../middlewares/auth'

export function createProductRoutes(productController: ProductController) {

    let productRoutes = express.Router();

    productRoutes.get("/products", productController.getAllProducts);
    productRoutes.get("/product/:id", productController.getProductDetails);

    productRoutes.get("/cart", isAuthUser, productController.getCartItems);
    productRoutes.post("/cart", isAuthUser, productController.addCartItem);
    productRoutes.put("/cart", isAuthUser, productController.updateCartItem);
    productRoutes.delete("/cart/:id", isAuthUser, productController.removeCartItem);

    productRoutes.get("/reviews", productController.getProductReviews);
    productRoutes.post("/review", isAuthUser, productController.createProductReview);
    productRoutes.put("/review", isAuthUser, productController.updateReview);

    productRoutes.get("/store/:id", productController.getStoreProducts);
    productRoutes.get("/store/products", isAuthUser, productController.getAllStoreProducts);
    productRoutes.post("/product", isAuthUser, isAuthRole("store"), productController.createProduct);
    productRoutes.put("/product", isAuthUser, productController.updateStoreProduct);

    productRoutes.get("/admin/products", isAuthUser, isAuthRole("admin"), productController.getAdminProducts);
    productRoutes.put("/admin/product/:id", isAuthUser, isAuthRole("admin"), productController.updateProduct);

    return productRoutes
}