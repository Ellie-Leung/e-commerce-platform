import express from "express";
import type OrderController from '../controllers/orderController'
import { isAuthUser, isAuthRole } from '../middlewares/auth'


export function createOrderRoutes(orderController: OrderController) {

    let orderRoutes = express.Router();

    // userRoutes.post('/user', orderController.createNewUser);
    
    // orderRoutes.post("/order/new", isAuthUser, orderController.newOrder);
    orderRoutes.get("/order/:id", isAuthUser, orderController.orderDetail);
    orderRoutes.get("/orders", isAuthUser, orderController.myOrders);

    orderRoutes.get("/admin/orders", isAuthUser, isAuthRole("admin"), orderController.allOrders);
    orderRoutes.put("/admin/order/:id", isAuthUser, isAuthRole("admin"), orderController.updateOrder);
    // orderRoutes.delete("/admin/order/:id", isAuthUser, isAuthRole("admin"), orderController.deleteOrder);

    return orderRoutes
}