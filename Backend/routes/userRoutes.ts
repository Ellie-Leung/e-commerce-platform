import express from "express";
import UserController from '../controllers/userController'
import { isAuthUser, isAuthRole } from '../middlewares/auth'


export function createUserRoutes(userController: UserController) {

    let userRoutes = express.Router();

    userRoutes.post("/register", userController.registerUser);
    userRoutes.post("/login", userController.loginUser);
    // userRoutes.get("/logout", userController.logout);

    userRoutes.get("/chat/member", userController.getMember);
   
    userRoutes.get("/user", isAuthUser, userController.getUserDetails);
    userRoutes.put("/user/update", isAuthUser, userController.updateProfile);
    // userRoutes.post("/password/forgot", userController.forgotPassword);
    // userRoutes.put("/password/reset/:token", userController.resetPassword);
    userRoutes.put("/password/update", isAuthUser, userController.updatePassword);

    userRoutes.get("/admin/users", isAuthUser, isAuthRole("admin"), userController.getAllUsers);
    userRoutes.get("/admin/user/:id", isAuthUser, isAuthRole("admin"), userController.getSingleUser);
    userRoutes.put("/admin/user/:id", isAuthUser, isAuthRole("admin"), userController.updateUserRole);
    userRoutes.delete("/admin/user/:id", isAuthUser, isAuthRole("admin"), userController.deleteUser);

    return userRoutes
}