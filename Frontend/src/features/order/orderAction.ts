import * as constants from "./orderConstant";
import axios from "axios";
  

  export const createOrder = (order: {}) => async (dispatch: any) => {
    try {
      dispatch({ type: constants.CREATE_ORDER_REQUEST });
  
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      const { data } = await axios.post(`${process.env.REACT_APP_HOSTING}/order/new`, order, config);
  
      dispatch({ type: constants.CREATE_ORDER_SUCCESS, payload: data });
    } catch (error: any) {
      dispatch({
        type: constants.CREATE_ORDER_FAIL,
        payload: error.response.data.message,
      });
    }
  };
  
 // Get all orders by user -- OK
  export const myOrders = () => async (dispatch: any) => {
    try {
      dispatch({ type: constants.MY_ORDERS_REQUEST });

      let token = JSON.parse(localStorage.getItem("token") || '{}')

      const { data } = await axios.get(`${process.env.REACT_APP_HOSTING}/orders?access_token=${token}`);
console.log(data)
      dispatch({ type: constants.MY_ORDERS_SUCCESS, payload: data.orders });
    } catch (error: any) {
      dispatch({
        type: constants.MY_ORDERS_FAIL,
        payload: error.response.data.message,
      });
    }
  };
  
  // Get All Orders (admin)
  export const getAllOrders = () => async (dispatch: any) => {
    try {
      dispatch({ type: constants.ALL_ORDERS_REQUEST });

      let token = JSON.parse(localStorage.getItem("token") || '{}')
  
      const { data } = await axios.get(`${process.env.REACT_APP_HOSTING}/admin/orders?access_token=${token}`);
  console.log(data)
      dispatch({ type: constants.ALL_ORDERS_SUCCESS, payload: data.orders });
    } catch (error: any) {
      dispatch({
        type: constants.ALL_ORDERS_FAIL,
        payload: error.response.data.message,
      });
    }
  };
  

  export const updateOrder = (id: number, order: any) => async (dispatch: any) => {
    try {
      dispatch({ type: constants.UPDATE_ORDER_REQUEST });
  
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      const { data } = await axios.put(
        `${process.env.REACT_APP_HOSTING}/admin/order/${id}`,
        order,
        config
      );
  
      dispatch({ type: constants.UPDATE_ORDER_SUCCESS, payload: data.success });
    } catch (error: any) {
      dispatch({
        type: constants.UPDATE_ORDER_FAIL,
        payload: error.response.data.message,
      });
    }
  };
  

  export const deleteOrder = (id: number) => async (dispatch: any) => {
    try {
      dispatch({ type: constants.DELETE_ORDER_REQUEST });
  
      const { data } = await axios.delete(`${process.env.REACT_APP_HOSTING}/admin/order/${id}`);
  
      dispatch({ type: constants.DELETE_ORDER_SUCCESS, payload: data.success });
    } catch (error: any) {
      dispatch({
        type: constants.DELETE_ORDER_FAIL,
        payload: error.response.data.message,
      });
    }
  };
  
  // Get Order Details
  export const getOrderDetails = (id: number) => async (dispatch: any) => {
    try {
      dispatch({ type: constants.ORDER_DETAILS_REQUEST });

      let token = JSON.parse(localStorage.getItem("token") || '{}')
  
      const { data } = await axios.get(`${process.env.REACT_APP_HOSTING}/order/${id}?access_token=${token}`);
  
      dispatch({ type: constants.ORDER_DETAILS_SUCCESS, payload: data.order });
    } catch (error: any) {
      dispatch({
        type: constants.ORDER_DETAILS_FAIL,
        payload: error.response.data.message,
      });
    }
  };

  //  // Payment
  //  export const payOrder = (paymentData: any) => async (dispatch: any) => {
  //   const config = {
  //     headers: { "Content-Type": "application/json",},
  //   };

  //   const { data } = await axios.post(
  //     `${process.env.REACT_APP_HOSTING}/payment`,
  //     paymentData,
  //     config
  //   );

  //   const client_secret = data.client_secret;

  //   if (!stripe || !elements) return;

  //   const result = await stripe.confirmCardPayment(client_secret, {
  //     payment_method: {
  //       card: elements.getElement(CardNumberElement)!,
  //       billing_details: {
  //         name: user.name,
  //         email: user.email,
  //         address: {
  //           line1: shippingInfo.address,
  //           city: shippingInfo.city,
  //           state: shippingInfo.state,
  //           postal_code: shippingInfo.pinCode,
  //           country: shippingInfo.country,
  //         },
  //       },
  //     },
  //   });

  //  }
  
  // Clearing Errors
  export const clearErrors = () => async (dispatch: any) => {
    dispatch({ type: constants.CLEAR_ERRORS });
  };