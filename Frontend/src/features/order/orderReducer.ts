import * as constants from "./orderConstant";
  
  export const newOrderReducer = (state = {}, action:any) => {
    switch (action.type) {
      case constants.CREATE_ORDER_REQUEST:
        return {
          ...state,
          loading: true,
        };
  
      case constants.CREATE_ORDER_SUCCESS:
        return {
          loading: false,
          order: action.payload,
        };
  
      case constants.CREATE_ORDER_FAIL:
        return {
          loading: false,
          error: action.payload,
        };
      case constants.CLEAR_ERRORS:
        return {
          ...state,
          error: null,
        };
  
      default:
        return state;
    }
  };
  
  export const myOrdersReducer = (state = { orders: [] }, action:any) => {
    switch (action.type) {
      case constants.MY_ORDERS_REQUEST:
        return {
          loading: true,
        };
  
      case constants.MY_ORDERS_SUCCESS:
        return {
          loading: false,
          orders: action.payload,
        };
  
      case constants.MY_ORDERS_FAIL:
        return {
          loading: false,
          error: action.payload,
        };
      case constants.CLEAR_ERRORS:
        return {
          ...state,
          error: null,
        };
  
      default:
        return state;
    }
  };
  
  export const allOrdersReducer = (state = { orders: [] }, action:any) => {
    switch (action.type) {
      case constants.ALL_ORDERS_REQUEST:
        return {
          loading: true,
        };
  
      case constants.ALL_ORDERS_SUCCESS:
        return {
          loading: false,
          orders: action.payload,
        };
  
      case constants.ALL_ORDERS_FAIL:
        return {
          loading: false,
          error: action.payload,
        };
      case constants.CLEAR_ERRORS:
        return {
          ...state,
          error: null,
        };
  
      default:
        return state;
    }
  };
  
  export const orderReducer = (state = {}, action:any) => {
    switch (action.type) {
      case constants.UPDATE_ORDER_REQUEST:
      case constants.DELETE_ORDER_REQUEST:
        return {
          ...state,
          loading: true,
        };
  
      case constants.UPDATE_ORDER_SUCCESS:
        return {
          ...state,
          loading: false,
          isUpdated: action.payload,
        };
  
      case constants.DELETE_ORDER_SUCCESS:
        return {
          ...state,
          loading: false,
          isDeleted: action.payload,
        };
  
      case constants.UPDATE_ORDER_FAIL:
      case constants.DELETE_ORDER_FAIL:
        return {
          ...state,
          loading: false,
          error: action.payload,
        };
      case constants.UPDATE_ORDER_RESET:
        return {
          ...state,
          isUpdated: false,
        };
  
      case constants.DELETE_ORDER_RESET:
        return {
          ...state,
          isDeleted: false,
        };
      case constants.CLEAR_ERRORS:
        return {
          ...state,
          error: null,
        };
  
      default:
        return state;
    }
  };
  
  export const orderDetailsReducer = (state = { order: {} }, action:any) => {
    switch (action.type) {
      case constants.ORDER_DETAILS_REQUEST:
        return {
          loading: true,
        };
  
      case constants.ORDER_DETAILS_SUCCESS:
        return {
          loading: false,
          order: action.payload,
        };
  
      case constants.ORDER_DETAILS_FAIL:
        return {
          loading: false,
          error: action.payload,
        };
      case constants.CLEAR_ERRORS:
        return {
          ...state,
          error: null,
        };
  
      default:
        return state;
    }
  };