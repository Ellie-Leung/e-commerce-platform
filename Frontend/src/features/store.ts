import { combineReducers} from "redux";
import { configureStore } from "@reduxjs/toolkit";
import * as productReducers from './product/productReducer';
import * as cartReducers from './cart/cartReducer';
import * as userReducers from './user/userReducer';
import * as orderReducers from './order/orderReducer';
import * as chatReducers from './chat/chatReducer';
// import * as storeReducers from './store/storeReducer';

const reducer = combineReducers({
    products: productReducers.productsReducer,
    productDetails: productReducers.productDetailsReducer,
    newProduct: productReducers.newProductReducer,
    product: productReducers.productReducer,
    productReviews: productReducers.productReviewsReducer,
    review: productReducers.reviewReducer,
    newReview: productReducers.newReviewReducer,
 
    user: userReducers.userReducer,
    userDetails: userReducers.userDetailsReducer,
    allUsers: userReducers.allUsersReducer,
    userProfile: userReducers.profileReducer,
    forgotPassword: userReducers.forgotPasswordReducer,

    chats: chatReducers.chatReducer,
    chatMember: chatReducers.chatMemberReducer,
    messages: chatReducers.messagesReducer,

    // store: storeReducers.storeReducer,
    // storeProfile: storeReducers.profileReducer,
 
    cart: cartReducers.cartReducer,

    newOrder: orderReducers.newOrderReducer,
    myOrders: orderReducers.myOrdersReducer,
    orderDetails: orderReducers.orderDetailsReducer,
    allOrders: orderReducers.allOrdersReducer,
    order: orderReducers.orderReducer,
    
})

let initialState = {
    cart: {
      cartItems: localStorage.getItem("cartItems")
        ? JSON.parse(localStorage.getItem("cartItems") || '[]')
        : [],
      shippingInfo: localStorage.getItem("shippingInfo")
        ? JSON.parse(localStorage.getItem("shippingInfo") || '{}')
        : {},
    },
  };

const store = configureStore({
    reducer,
    ...initialState,
})

export default store