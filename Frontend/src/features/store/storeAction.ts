import axios from "axios";
import * as constants from "./storeConstant";

  // Load store
  export const loadStore = () => async (dispatch: any) => {
    try {
      dispatch({ type: constants.LOAD_STORE_REQUEST });
  
      const { data } = await axios.get(`${process.env.REACT_APP_HOSTING}/store`);
  
      dispatch({ type: constants.LOAD_STORE_SUCCESS, payload: data.user });
    } catch (error: any) {
      dispatch({ type: constants.LOAD_STORE_FAIL, payload: error.response});
    }
  };


    
  // Clearing Errors
  export const clearErrors = () => async (dispatch: any) => {
    dispatch({ type: constants.CLEAR_ERRORS });
  };

export{}