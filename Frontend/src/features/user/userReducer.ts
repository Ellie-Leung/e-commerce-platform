import * as constants from "./userConstant";
import jwtDecode from "jwt-decode";

const iUserState = JSON.parse(localStorage.getItem("user")|| '{}')


export const userReducer = (state=iUserState, action: any,) => {
    switch (action.type) {
      case constants.LOGIN_REQUEST:
      case constants.REGISTER_USER_REQUEST:
      case constants.LOAD_USER_REQUEST:
        return {
          loading: true,
          isAuthenticated: false,
        };
      case constants.LOGIN_SUCCESS:
      case constants.REGISTER_USER_SUCCESS:{
        
        let jwtPayload = handleToken(action)   

        return {
          ...state,
          loading: false,
          isAuthenticated: true,
          user: {
            id: jwtPayload.id,
            name: jwtPayload.name,
            email: jwtPayload.email,
            image_url: jwtPayload.image_url,
            role: jwtPayload.role,
            created_at: jwtPayload.created_at,
          }
        }
    }

    case constants.LOAD_USER_SUCCESS:{
      return {
        ...state,
        loading: false,
        isAuthenticated: true,
        user: action.payload,
      };
    }
       
  
      case constants.LOGOUT_SUCCESS:
        return {
          loading: false,
          user: null,
          cart: null,
          isAuthenticated: false,
        };
      case constants.LOGIN_FAIL:
      case constants.REGISTER_USER_FAIL:
        console.log("fail state:", state)
        return {
          ...state,
          loading: false,
          isAuthenticated: false,
          user: null,
          error: action.payload,
        };
  
      case constants.LOAD_USER_FAIL:
        return {
          loading: false,
          isAuthenticated: false,
          user: null,
          error: action.payload,
        };
  
      case constants.LOGOUT_FAIL:
        return {
          ...state,
          loading: false,
          error: action.payload,
        };
  
      case constants.CLEAR_ERRORS:
        return {
          ...state,
          error: null,
        };
  
      default:
        return state;
    }
  };

  export interface MyJwtPayload {
    role: string;
    id: number;
    image_url: string;
    name: string;
    email: string;
    created_at: string;
  }

  const handleToken = (action: any) => {
    const token = action.payload;
      localStorage.setItem("token", JSON.stringify(token));
      const jwtPayload: MyJwtPayload = jwtDecode(token);
      return jwtPayload
  }

  
  export const profileReducer = (state = {}, action: any) => {
    switch (action.type) {
      case constants.UPDATE_PROFILE_REQUEST:
      case constants.UPDATE_PASSWORD_REQUEST:
      case constants.UPDATE_USER_REQUEST:
      case constants.DELETE_USER_REQUEST:
        return {
          ...state,
          loading: true,
        };
      case constants.UPDATE_PROFILE_SUCCESS:
      case constants.UPDATE_PASSWORD_SUCCESS:
      case constants.UPDATE_USER_SUCCESS:
        return {
          ...state,
          loading: false,
          isUpdated: action.payload,
        };
  
      case constants.DELETE_USER_SUCCESS:
        return {
          ...state,
          loading: false,
          isDeleted: action.payload.success,
          message: action.payload.message,
        };
  
      case constants.UPDATE_PROFILE_FAIL:
      case constants.UPDATE_PASSWORD_FAIL:
      case constants.UPDATE_USER_FAIL:
      case constants.DELETE_USER_FAIL:
        return {
          ...state,
          loading: false,
          error: action.payload,
        };
  
      case constants.UPDATE_PROFILE_RESET:
      case constants.UPDATE_PASSWORD_RESET:
      case constants.UPDATE_USER_RESET:
        return {
          ...state,
          isUpdated: false,
        };
  
      case constants.DELETE_USER_RESET:
        return {
          ...state,
          isDeleted: false,
        };
  
      case constants.CLEAR_ERRORS:
        return {
          ...state,
          error: null,
        };
  
      default:
        return state;
    }
  };
  
  export const forgotPasswordReducer = (state = {}, action: any) => {
    switch (action.type) {
      case constants.FORGOT_PASSWORD_REQUEST:
      case constants.RESET_PASSWORD_REQUEST:
        return {
          ...state,
          loading: true,
          error: null,
        };
      case constants.FORGOT_PASSWORD_SUCCESS:
        return {
          ...state,
          loading: false,
          message: action.payload,
        };
  
      case constants.RESET_PASSWORD_SUCCESS:
        return {
          ...state,
          loading: false,
          success: action.payload,
        };
  
      case constants.FORGOT_PASSWORD_FAIL:
      case constants.RESET_PASSWORD_FAIL:
        return {
          ...state,
          loading: false,
          error: action.payload,
        };
  
      case constants.CLEAR_ERRORS:
        return {
          ...state,
          error: null,
        };
  
      default:
        return state;
    }
  };
  
  export const allUsersReducer = (state = { users: [] }, action: any) => {
    switch (action.type) {
      case constants.ALL_USERS_REQUEST:
        return {
          ...state,
          loading: true,
        };
      case constants.ALL_USERS_SUCCESS:
        return {
          ...state,
          loading: false,
          users: action.payload,
        };
  
      case constants.ALL_USERS_FAIL:
        return {
          ...state,
          loading: false,
          error: action.payload,
        };
  
      case constants.CLEAR_ERRORS:
        return {
          ...state,
          error: null,
        };
  
      default:
        return state;
    }
  };
  
  export const userDetailsReducer = (state = { user: {} }, action: any) => {
    switch (action.type) {
      case constants.USER_DETAILS_REQUEST:
        return {
          ...state,
          loading: true,
        };
      case constants.USER_DETAILS_SUCCESS:
        return {
          ...state,
          loading: false,
          user: action.payload,
        };
  
      case constants.USER_DETAILS_FAIL:
        return {
          ...state,
          loading: false,
          error: action.payload,
        };
  
      case constants.CLEAR_ERRORS:
        return {
          ...state,
          error: null,
        };
  
      default:
        return state;
    }
  };



  