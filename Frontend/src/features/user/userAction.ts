import axios from "axios";
import * as constants from "./userConstant";


// Login
export const login = (email: string, password: string) => async (dispatch: any, getState: any) => {
    try {
      dispatch({ type: constants.LOGIN_REQUEST });
  
      const config = { headers: { "Content-Type": "application/json" } };
  
      const { data } = await axios.post(
        `${process.env.REACT_APP_HOSTING}/login`,
        { email, password },
        config
      );
  
      dispatch({ type: constants.LOGIN_SUCCESS, payload: data.token });
     
    } catch (error: any) {
      dispatch({ type: constants.LOGIN_FAIL, payload: error.response.data.message });
    }
    localStorage.setItem("user", JSON.stringify(getState().user));
  };
  
  // Register
  export const register = (userData: any) => async (dispatch: any, getState: any) => {
    try {
      dispatch({ type: constants.REGISTER_USER_REQUEST });
  
      const config = { headers: { "Content-Type": "multipart/form-data" } };

      const { data } = await axios.post(`${process.env.REACT_APP_HOSTING}/register`, userData, config);
  
      dispatch({ type: constants.REGISTER_USER_SUCCESS, payload: data.token });
   
    } catch (error: any) {
      dispatch({
        type: constants.REGISTER_USER_FAIL,
        payload: error.response.data.message,
      });
    }
    localStorage.setItem("user", JSON.stringify(getState().user));
  };
  
  // Load User
  export const loadUser = () => async (dispatch: any) => {
    try {
      dispatch({ type: constants.LOAD_USER_REQUEST });

      let token = JSON.parse(localStorage.getItem("token") || '{}')
  
      const { data } = await axios.get(`${process.env.REACT_APP_HOSTING}/user?access_token=${token}`);
  
      dispatch({ type: constants.LOAD_USER_SUCCESS, payload: data.user });
    } catch (error: any) {
      dispatch({ type: constants.LOAD_USER_FAIL, payload: error.response});
    }
  };
  
  // Logout User
  export const logout = () => async (dispatch: any) => {
    try {

      dispatch({ type: constants.LOGOUT_SUCCESS });

      localStorage.clear();
   
    } catch (error: any) {
      dispatch({ type: constants.LOGOUT_FAIL, payload: error.response.data.message });
    }
  };
  
  // Update Profile
  export const updateProfile = (userData: any) => async (dispatch: any) => {
    try {
      dispatch({ type: constants.UPDATE_PROFILE_REQUEST });
      let token = JSON.parse(localStorage.getItem("token") || '{}')
  
      const config = { headers: { "Content-Type": "multipart/form-data" } };
  
      const { data } = await axios.put(`${process.env.REACT_APP_HOSTING}/user/update?access_token=${token}`, userData, config);
  
      dispatch({ type: constants.UPDATE_PROFILE_SUCCESS, payload: data.success });
    } catch (error: any) {
      dispatch({
        type: constants.UPDATE_PROFILE_FAIL,
        payload: error.response.data.message,
      });
    }
  };
  
  // Update Password
  export const updatePassword = (passwords: any) => async (dispatch: any) => {
    try {
      dispatch({ type: constants.UPDATE_PASSWORD_REQUEST });
      let token = JSON.parse(localStorage.getItem("token") || '{}')
  
      const config = { headers: { "Content-Type": "application/json" } };
  
      const { data } = await axios.put(
        `${process.env.REACT_APP_HOSTING}/password/update?access_token=${token}`,
        passwords,
        config
      );
  
      dispatch({ type: constants.UPDATE_PASSWORD_SUCCESS, payload: data.success });
    } catch (error: any) {
      dispatch({
        type: constants.UPDATE_PASSWORD_FAIL,
        payload: error.response.data.message,
      });
    }
  };
  
  // Forgot Password
  export const forgotPassword = (email: string) => async (dispatch: any) => {
    try {
      dispatch({ type: constants.FORGOT_PASSWORD_REQUEST });
      let token = JSON.parse(localStorage.getItem("token") || '{}')
      const config = { headers: { "Content-Type": "application/json" } };
  
      const { data } = await axios.post(`${process.env.REACT_APP_HOSTING}/password/forgot?access_token=${token}`, email, config);
  
      dispatch({ type: constants.FORGOT_PASSWORD_SUCCESS, payload: data.message });
    } catch (error: any) {
      dispatch({
        type: constants.FORGOT_PASSWORD_FAIL,
        payload: error.response.data.message,
      });
    }
  };
  
  // Reset Password
  export const resetPassword = (token: string, passwords: any) => async (dispatch: any) => {
    try {
      dispatch({ type: constants.RESET_PASSWORD_REQUEST });
  
      const config = { headers: { "Content-Type": "application/json" } };
  
      const { data } = await axios.put(
        `${process.env.REACT_APP_HOSTING}/password/reset/${token}?access_token=${token}`,
        passwords,
        config
      );
  
      dispatch({ type: constants.RESET_PASSWORD_SUCCESS, payload: data.success });
    } catch (error: any) {
      dispatch({
        type: constants.RESET_PASSWORD_FAIL,
        payload: error.response.data.message,
      });
    }
  };
  
  // get All Users
  export const getAllUsers = () => async (dispatch: any) => {
    try {
      dispatch({ type: constants.ALL_USERS_REQUEST });
      let token = JSON.parse(localStorage.getItem("token") || '{}')
      const { data } = await axios.get(`${process.env.REACT_APP_HOSTING}/admin/users?access_token=${token}`);

      dispatch({ type: constants.ALL_USERS_SUCCESS, payload: data.users });
    } catch (error: any) {
      dispatch({ type: constants.ALL_USERS_FAIL, payload: error.response.data.message });
    }
  };
  
  // get User Details
  export const getUserDetails = (id: number) => async (dispatch: any) => {
    try {
      dispatch({ type: constants.USER_DETAILS_REQUEST });
      let token = JSON.parse(localStorage.getItem("token") || '{}')
      const { data } = await axios.get(`${process.env.REACT_APP_HOSTING}/admin/user/${id}?access_token=${token}`);
  
      dispatch({ type: constants.USER_DETAILS_SUCCESS, payload: data.user });
    } catch (error: any) {
      dispatch({ type: constants.USER_DETAILS_FAIL, payload: error.response.data.message });
    }
  };
  
  // Update User
  export const updateUser = (id: number, userData: any) => async (dispatch: any) => {
    try {
      dispatch({ type: constants.UPDATE_USER_REQUEST });
      let token = JSON.parse(localStorage.getItem("token") || '{}')
      const config = { headers: { "Content-Type": "application/json" } };
  
      const { data } = await axios.put(
        `${process.env.REACT_APP_HOSTING}/admin/user/${id}?access_token=${token}`,
        userData,
        config
      );
  
      dispatch({ type: constants.UPDATE_USER_SUCCESS, payload: data.success });
    } catch (error: any) {
      dispatch({
        type: constants.UPDATE_USER_FAIL,
        payload: error.response.data.message,
      });
    }
  };
  
  // Delete User
  export const deleteUser = (id: number) => async (dispatch: any) => {
    try {
      dispatch({ type: constants.DELETE_USER_REQUEST });
      let token = JSON.parse(localStorage.getItem("token") || '{}')
      const { data } = await axios.delete(`${process.env.REACT_APP_HOSTING}/admin/user/${id}?access_token=${token}`);
  
      dispatch({ type: constants.DELETE_USER_SUCCESS, payload: data });
    } catch (error: any) {
      dispatch({
        type: constants.DELETE_USER_FAIL,
        payload: error.response.data.message,
      });
    }
  };
  


  
  // Clearing Errors
  export const clearErrors = () => async (dispatch: any) => {
    dispatch({ type: constants.CLEAR_ERRORS });
  };
  