import * as constants from "./cartConstant";
  import axios from "axios";

  // Get cart items
export const getCartItems = () => async (dispatch: any) => {
  try {
    dispatch({ type: constants.ALL_CART_REQUEST });

    let token = JSON.parse(localStorage.getItem("token") || '{}')
    
    const { data } = await axios.get(`${process.env.REACT_APP_HOSTING}/cart?access_token=${token}`);
    dispatch({
      type: constants.ALL_CART_SUCCESS,
      payload: data,
    });
  } catch (error: any) {
    dispatch({
      type: constants.ALL_CART_FAIL,
      payload: error.response.data.message,
    });
  }
};

  // Add to Cart
  export const addItemToCart = (product: any, quantity: number) => async (dispatch: any) => {
    try { 
      const item = {
        productId: product.id,
        option: product.option,
        quantity,
      }
      console.log(item)
      const config = { headers: { "Content-Type": "application/json" } };

      let token = JSON.parse(localStorage.getItem("token") || '{}')

      let {data }= await axios.post(`${process.env.REACT_APP_HOSTING}/cart?access_token=${token}`, item, config);
      console.log(data)
      dispatch({
        type: constants.ADD_TO_CART,
        payload: {
          id: data.cartId,
          images:product.images,
          name:product.name,
          option:product.option,
          price:product.price,
          product_id:product.id,
          stock:product.stock,
          store_name:product.store_name,
          quantity,
        },
      });

    } catch (error: any) {
      dispatch({
        type: constants.UPDATE_CART_FAIL,
        payload: error.response.data.message,
      });
    } 
  };

  // update cart
  export const updateCartItem = (product: any, newQty: number) => async (dispatch: any) => {
    try { 
      const item = {
        itemId: product.id,
        newQty,
      }
      console.log(item)
      const config = { headers: { "Content-Type": "application/json" } };

      let token = JSON.parse(localStorage.getItem("token") || '{}')

      await axios.put(`${process.env.REACT_APP_HOSTING}/cart?access_token=${token}`, item, config);

      dispatch({
        type: constants.UPDATE_CART,
        payload: {
          ...product,
          quantity: newQty,
        },
      });

    } catch (error: any) {
      dispatch({
        type: constants.UPDATE_CART_FAIL,
        payload: error.response,
      });
    } 
  };
  
  // REMOVE FROM CART
  export const removeItemsFromCart = (itemId: number) => async (dispatch: any) => {
  try {
  let token = JSON.parse(localStorage.getItem("token") || '{}')

  await axios.delete(`${process.env.REACT_APP_HOSTING}/cart/${itemId}?access_token=${token}`);

  dispatch({
    type: constants.REMOVE_FROM_CART,
    payload: itemId,
  });

  }catch(error: any) {
    dispatch({
      type: constants.UPDATE_CART_FAIL,
      payload: error.response,
    });
  }   
};
  
  // SAVE SHIPPING INFO
  export const saveShippingInfo = (data: any) => async (dispatch: any) => {
    dispatch({
      type: constants.SAVE_SHIPPING_INFO,
      payload: data,
    });
  
    localStorage.setItem("shippingInfo", JSON.stringify(data));
  };
  
    // Clearing Errors
export const clearErrors = () => async (dispatch: any) => {
  await dispatch({ type: constants.CLEAR_ERRORS });
};