import * as constants from "./cartConstant";

// const shippingInfo= localStorage.getItem("shippingInfo") ? JSON.parse(localStorage.getItem("shippingInfo")|| '{}') : {}

  export const cartReducer = (state= { cartItems: [], shippingInfo: {} }, action: any) => {

    switch (action.type) {
      case constants.ALL_CART_REQUEST:
          return {
            loading: true,
            cartItems: [],
          };
          

        case constants.ALL_CART_SUCCESS:
          return  {
            cartItems: action.payload.cartItems
          }
           
    
        case constants.ALL_CART_FAIL:
          return {
            loading: false,
            error: action.payload,
          };


      case constants.ADD_TO_CART:
        const newItem = action.payload;
          return {
            ...state,
            cartItems: [...state.cartItems, newItem],
          };
        

        case constants.ADD_TO_CART_FAIL:
 
          return {
            error: action.payload,
        };

        case constants.UPDATE_CART:
          const item = action.payload;
  
            return {
              ...state,
              cartItems: state.cartItems.map((i: any) =>
                i.id === item.id ? item : i
              ),
            };


      case constants.UPDATE_CART_FAIL:
 
        return {
          error: action.payload,
      };

      case constants.REMOVE_FROM_CART:
        return {
          ...state,
          cartItems: state.cartItems.filter((i: any) => i.id !== action.payload),
        };
  
      case constants.SAVE_SHIPPING_INFO:
        return {
          ...state,
          shippingInfo: action.payload,
        };

      case constants.CLEAR_ERRORS:
        return {
          ...state,
          error: null,
        };
  
      default:
        return state;
    }
  };
  