export const ALL_CART_REQUEST = 'ALL_CART_REQUEST'
export const ALL_CART_SUCCESS = 'ALL_CART_SUCCESS'
export const ALL_CART_FAIL = 'ALL_CART_FAIL'

export const ADD_TO_CART = "ADD_TO_CART";
export const ADD_TO_CART_FAIL = "ADD_TO_CART_FAIL"

export const UPDATE_CART = "UPDATE_CART";
export const UPDATE_CART_FAIL = "UPDATE_CART_FAIL"

export const REMOVE_FROM_CART = "REMOVE_CART_ITEM";
export const REMOVE_FROM_CARTFAIL = "REMOVE_FROM_CART_FAIL"

export const SAVE_SHIPPING_INFO = "SAVE_SHIPPING_INFO";

export const CLEAR_ERRORS = 'CLEAR_ERRORS'
