import axios from "axios";
import * as constants from "./productConstant";

// Get All Products
export const getProduct = (queyStr?: string) => async (dispatch: any) => {
  console.log("getProduct 1");
  try {
    // dispatch({ type: constants.ALL_PRODUCT_REQUEST });
    console.log("getProduct 2");

    let link = `${process.env.REACT_APP_HOSTING}/products`;
    if (queyStr) {
      link = `${process.env.REACT_APP_HOSTING}/products${queyStr}`;
    }

    const { data } = await axios.get(link);

    console.log("getProduct 3");

    setTimeout(() => {
      dispatch({
        type: constants.ALL_PRODUCT_SUCCESS,
        payload: data,
      });
    }, 500);
  } catch (error: any) {
    console.log("error = ", error);
    dispatch({
      type: constants.ALL_PRODUCT_FAIL,
      payload: error.response.data.message,
    });
  }
};

// Get All Products
export const getStoreProducts = (storeId: number) => async (dispatch: any) => {
  try {
    dispatch({ type: constants.STORE_PRODUCT_REQUEST });
    console.log("called");
    // let link = `/products?search=${keyword}&page=${currentPage}&price[gte]=${priceFloor}&price[lte]=${priceCeiling}&ratings[gte]=${ratings}`;

    // if (category) {
    //   link = `/products?search=${keyword}&page=${currentPage}&price[gte]=${priceFloor}&price[lte]=${priceCeiling}&category=${category}&ratings[gte]=${ratings}`;
    // }

    const { data } = await axios.get(
      `${process.env.REACT_APP_HOSTING}/store/${storeId}`
    );
    console.log(data);
    dispatch({
      type: constants.STORE_PRODUCT_SUCCESS,
      payload: data,
    });
  } catch (error: any) {
    dispatch({
      type: constants.STORE_PRODUCT_FAIL,
      payload: error.response.data.message,
    });
  }
};

// Get Products Details
export const getProductDetails = (id: number) => async (dispatch: any) => {
  try {
    dispatch({ type: constants.PRODUCT_DETAILS_REQUEST });

    const { data } = await axios.get(
      `${process.env.REACT_APP_HOSTING}/product/${id}`
    );

    dispatch({
      type: constants.PRODUCT_DETAILS_SUCCESS,
      payload: data.product,
    });
  } catch (error: any) {
    dispatch({
      type: constants.PRODUCT_DETAILS_FAIL,
      payload: error.response.data.message,
    });
  }
};

// NEW REVIEW
export const newReview = (reviewData: any) => async (dispatch: any) => {
  try {
    dispatch({ type: constants.NEW_REVIEW_REQUEST });

    const config = {
      headers: { "Content-Type": "application/json" },
    };

    const { data } = await axios.put(
      `${process.env.REACT_APP_HOSTING}/review`,
      reviewData,
      config
    );

    dispatch({
      type: constants.NEW_REVIEW_SUCCESS,
      payload: data.success,
    });
  } catch (error: any) {
    dispatch({
      type: constants.NEW_REVIEW_FAIL,
      payload: error.response.data.message,
    });
  }
};

// Get All Reviews of a Product
export const getAllReviews = (id: number) => async (dispatch: any) => {
  try {
    dispatch({ type: constants.ALL_REVIEW_REQUEST });

    const { data } = await axios.get(
      `${process.env.REACT_APP_HOSTING}/reviews?id=${id}`
    );

    dispatch({
      type: constants.ALL_REVIEW_SUCCESS,
      payload: data.reviews,
    });
  } catch (error: any) {
    dispatch({
      type: constants.ALL_REVIEW_FAIL,
      payload: error.response.data.message,
    });
  }
};

// Delete Review of a Product
export const deleteReviews =
  (reviewId: number, productId: number) => async (dispatch: any) => {
    try {
      dispatch({ type: constants.DELETE_REVIEW_REQUEST });

      const { data } = await axios.delete(
        `${process.env.REACT_APP_HOSTING}/reviews?id=${reviewId}&productId=${productId}`
      );

      dispatch({
        type: constants.DELETE_REVIEW_SUCCESS,
        payload: data.success,
      });
    } catch (error: any) {
      dispatch({
        type: constants.DELETE_REVIEW_FAIL,
        payload: error.response.data.message,
      });
    }
  };

// Get All Products -- Admin
export const getAdminProduct = () => async (dispatch: any) => {
  try {
    dispatch({ type: constants.ADMIN_PRODUCT_REQUEST });
    let token = JSON.parse(localStorage.getItem("token") || "{}");
    const { data } = await axios.get(
      `${process.env.REACT_APP_HOSTING}/admin/products?access_token=${token}`
    );

    dispatch({
      type: constants.ADMIN_PRODUCT_SUCCESS,
      payload: data.products,
    });
  } catch (error: any) {
    dispatch({
      type: constants.ADMIN_PRODUCT_FAIL,
      payload: error.response.data.message,
    });
  }
};

// Delete Product
export const deleteProduct = (id: number) => async (dispatch: any) => {
  try {
    dispatch({ type: constants.DELETE_PRODUCT_REQUEST });

    const { data } = await axios.delete(
      `${process.env.REACT_APP_HOSTING}//admin/product/${id}`
    );

    dispatch({
      type: constants.DELETE_PRODUCT_SUCCESS,
      payload: data.success,
    });
  } catch (error: any) {
    dispatch({
      type: constants.DELETE_PRODUCT_FAIL,
      payload: error.response.data.message,
    });
  }
};

// Create Product
export const createProduct = (productData: any) => async (dispatch: any) => {
  try {
    dispatch({ type: constants.NEW_PRODUCT_REQUEST });
    console.log(productData);
    const config = {
      headers: { "Content-Type": "application/json" },
    };

    let token = JSON.parse(localStorage.getItem("token") || "{}");

    const { data } = await axios.post(
      `${process.env.REACT_APP_HOSTING}/product?access_token=${token}`,
      productData,
      config
    );

    dispatch({
      type: constants.NEW_PRODUCT_SUCCESS,
      payload: data,
    });
  } catch (error: any) {
    dispatch({
      type: constants.NEW_PRODUCT_FAIL,
      payload: error.response,
    });
  }
};

// Update Product
export const updateProduct =
  (id: number, productData: any) => async (dispatch: any) => {
    try {
      dispatch({ type: constants.UPDATE_PRODUCT_REQUEST });

      const config = {
        headers: { "Content-Type": "application/json" },
      };

      const { data } = await axios.put(
        `${process.env.REACT_APP_HOSTING}/admin/product/${id}`,
        productData,
        config
      );

      dispatch({
        type: constants.UPDATE_PRODUCT_SUCCESS,
        payload: data.success,
      });
    } catch (error: any) {
      dispatch({
        type: constants.UPDATE_PRODUCT_FAIL,
        payload: error.response.data.message,
      });
    }
  };

// Clearing Errors
export const clearErrors = () => async (dispatch: any) => {
  await dispatch({ type: constants.CLEAR_ERRORS });
};
