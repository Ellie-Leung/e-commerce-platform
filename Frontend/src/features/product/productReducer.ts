import * as constants from "./productConstant";

export const productsReducer = (
  state: any = { loading: true, products: [] },
  action: any
) => {
  switch (action.type) {
    // case constants.ALL_PRODUCT_REQUEST:
    //   case constants.STORE_PRODUCT_REQUEST:
    // case constants.ADMIN_PRODUCT_REQUEST:
    //   return {
    //     loading: true,
    //     products: [],
    //   };
    case constants.ALL_PRODUCT_SUCCESS:
    case constants.STORE_PRODUCT_SUCCESS:
      return {
        loading: false,
        error: null,
        products: action.payload.products,
        productsCount: action.payload.productsCount,
        resultPerPage: action.payload.resultPerPage,
        // filteredProductsCount: action.payload.filteredProductsCount,
      };

    case constants.ADMIN_PRODUCT_SUCCESS:
      return {
        loading: false,
        products: action.payload,
      };
    case constants.ALL_PRODUCT_FAIL:
    case constants.STORE_PRODUCT_FAIL:
    case constants.ADMIN_PRODUCT_FAIL:
      return {
        loading: false,
        error: action.payload,
      };

    case constants.CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};

export const productDetailsReducer = (
  state: any = { product: {} },
  action: any
) => {
  switch (action.type) {
    case constants.PRODUCT_DETAILS_REQUEST:
      return {
        loading: true,
        ...state,
      };
    case constants.PRODUCT_DETAILS_SUCCESS:
      return {
        loading: false,
        product: action.payload,
      };
    case constants.PRODUCT_DETAILS_FAIL:
      return {
        loading: false,
        error: action.payload,
      };

    case constants.CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};

export const newProductReducer = (state = { product: {} }, action: any) => {
  switch (action.type) {
    case constants.NEW_PRODUCT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case constants.NEW_PRODUCT_SUCCESS:
      return {
        loading: false,
        success: action.payload.success,
        product: action.payload.product,
      };
    case constants.NEW_PRODUCT_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case constants.NEW_PRODUCT_RESET:
      return {
        ...state,
        success: false,
      };
    case constants.CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};

export const productReducer = (state = {}, action: any) => {
  switch (action.type) {
    case constants.DELETE_PRODUCT_REQUEST:
    case constants.UPDATE_PRODUCT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case constants.DELETE_PRODUCT_SUCCESS:
      return {
        ...state,
        loading: false,
        isDeleted: action.payload,
      };

    case constants.UPDATE_PRODUCT_SUCCESS:
      return {
        ...state,
        loading: false,
        isUpdated: action.payload,
      };
    case constants.DELETE_PRODUCT_FAIL:
    case constants.UPDATE_PRODUCT_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case constants.DELETE_PRODUCT_RESET:
      return {
        ...state,
        isDeleted: false,
      };
    case constants.UPDATE_PRODUCT_RESET:
      return {
        ...state,
        isUpdated: false,
      };
    case constants.CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};

export const productReviewsReducer = (state = { reviews: [] }, action: any) => {
  switch (action.type) {
    case constants.ALL_REVIEW_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case constants.ALL_REVIEW_SUCCESS:
      return {
        loading: false,
        reviews: action.payload,
      };
    case constants.ALL_REVIEW_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case constants.CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};

export const newReviewReducer = (state = {}, action: any) => {
  switch (action.type) {
    case constants.NEW_REVIEW_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case constants.NEW_REVIEW_SUCCESS:
      return {
        loading: false,
        success: action.payload,
      };
    case constants.NEW_REVIEW_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case constants.NEW_REVIEW_RESET:
      return {
        ...state,
        success: false,
      };
    case constants.CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};

export const reviewReducer = (state = {}, action: any) => {
  switch (action.type) {
    case constants.DELETE_REVIEW_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case constants.DELETE_REVIEW_SUCCESS:
      return {
        ...state,
        loading: false,
        isDeleted: action.payload,
      };
    case constants.DELETE_REVIEW_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case constants.DELETE_REVIEW_RESET:
      return {
        ...state,
        isDeleted: false,
      };
    case constants.CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};
