import * as constants from "./chatConstant";

export const chatReducer = (state = { chats: [], loading: true, error: "" }, action: any) => {
  switch (action.type) {
    case constants.CHATS_SUCCESS:
    case constants.NEW_CHAT_SUCCESS:
      return {
        ...state,
        loading: false,
        chats: action.payload,
      };

    case constants.CHATS_FAIL:
    case constants.NEW_CHAT_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case constants.CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};

export const chatMemberReducer = (state = { member: [], error: "" }, action: any) => {
  switch (action.type) {
    case constants.MEMBER_SUCCESS:
      return {
        ...state,
        member: action.payload,
      };

    case constants.MEMBER_FAIL:
      return {
        ...state,
        error: action.payload,
      };

    case constants.CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};

export const messagesReducer = (state = { messages: ([] = []), error: "" }, action: any) => {
  switch (action.type) {
    case constants.MESSAGES_SUCCESS:
      return {
        messages: action.payload,
      };

    case constants.NEW_MESSAGE_SUCCESS:
    case constants.GET_NEW_MESSAGE:
      console.log(action.payload);
      return {
        ...state,
        messages: [...state.messages, action.payload],
      };

    case constants.NEW_MESSAGE_FAIL:
    case constants.MESSAGES_FAIL:
      return {
        ...state,
        error: action.payload,
      };

    case constants.CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};
