import axios from "axios";
import * as constants from "./chatConstant";

export const getChats = (id: number) => async (dispatch: any) => {
  try {
    const { data } = await axios.get(`${process.env.REACT_APP_HOSTING}/chats/${id}`);

    dispatch({ type: constants.CHATS_SUCCESS, payload: data });
  } catch (err: any) {
    dispatch({ type: constants.CHATS_FAIL, payload: err.response.data.message });
  }
};

export const getChatMember = (id: number) => async (dispatch: any) => {
  try {
    const { data } = await axios.get(`${process.env.REACT_APP_HOSTING}/chat/member?id=${id}`);

    dispatch({ type: constants.MEMBER_SUCCESS, payload: data.member });
  } catch (err: any) {
    dispatch({ type: constants.MEMBER_FAIL, payload: err.response.data.message });
  }
};

export const getMessages = (id: number) => async (dispatch: any) => {
  try {
    const { data } = await axios.get(`${process.env.REACT_APP_HOSTING}/messages/${id}`);

    dispatch({ type: constants.MESSAGES_SUCCESS, payload: data });
  } catch (err: any) {
    dispatch({ type: constants.MESSAGES_FAIL, payload: err });
  }
};

export const postNewMessage = (msg: any) => async (dispatch: any) => {
  try {
    const config = {
      headers: { "Content-Type": "application/json" },
    };

    const { data } = await axios.post(`${process.env.REACT_APP_HOSTING}/message`, msg, config);
    console.log(data);
    dispatch({ type: constants.NEW_MESSAGE_SUCCESS, payload: data });
  } catch (err: any) {
    dispatch({ type: constants.NEW_MESSAGE_FAIL, payload: err });
  }
};

export const createNewChat = (chat: any) => async (dispatch: any) => {
  try {
    console.log("createNewChat");
    const config = {
      headers: { "Content-Type": "application/json" },
    };

    const { data } = await axios.post(`${process.env.REACT_APP_HOSTING}/chat`, chat, config);
    console.log(data);
    dispatch({ type: constants.NEW_CHAT_SUCCESS, payload: data });
  } catch (err: any) {
    dispatch({ type: constants.NEW_CHAT_FAIL, payload: err });
  }
};

export const clearErrors = () => async (dispatch: any) => {
  dispatch({ type: constants.CLEAR_ERRORS });
};
