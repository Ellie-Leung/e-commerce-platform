import "./css/message.css";
import { format } from 'timeago.js'
import { useSelector} from "react-redux";

const Message = (message: any) => {
  const { user } = useSelector((state: any) => state.user);
  const own = message.senderId === user.id
  const img = own ? 
              user.image_url ? user.image_url: "/assets/user-img/userIcon.jpg"
              : "/assets/user-img/userIcon.jpg"
      
  return (
    <div className={own ? "message own" : "message"}>
      <div className="messageTop">
        <img
          className="messageImg"
          src={img}
          alt=""
        />
        <p className="messageText">{message.text}</p>
      </div>
      <div className="messageBottom">{format(message.createdAt)}</div>
    </div>
  );
}

export default Message
