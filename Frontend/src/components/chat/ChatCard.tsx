import "./css/chatCard.css";
import { useEffect} from "react";
import { useSelector, useDispatch } from "react-redux";
import { clearErrors, getChatMember } from "../../features/chat/chatAction";


const ChatCard = (chat: {members: []} , currentUser: string) => {
  const dispatch = useDispatch();
  const { user } = useSelector((state: any) => state.user);
  const { error, member } = useSelector((state: any) => state.chatMember);

  useEffect(() => {
    const friendId = chat.members.find((m: string) => m !== user.id)
   
    if(friendId) {
      dispatch<any>(getChatMember(friendId));
    }

    if (error) {
      console.log(error);
      dispatch<any>(clearErrors());
    }
    
  }, [dispatch, error, currentUser, chat]);

  let avatar = member.img_url ? member.img_url : "/assets/user-img/userIcon.jpg"

  return (
    <div className="chatCard">
      <img
        className="memberImg"
        src={avatar}
        alt={member.name}
      />
      <span className="memberName">{member.name}</span>
    </div>
  );
}

export default ChatCard