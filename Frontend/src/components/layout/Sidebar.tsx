import "./css/sidebar.css";
import InventoryIcon from '@mui/icons-material/Inventory';
import ListAltIcon from '@mui/icons-material/ListAlt';
import DashboardIcon from '@mui/icons-material/Person';
import PersonIcon from '@mui/icons-material/Dashboard';
import PeopleIcon from '@mui/icons-material/People';
import AddIcon from "@mui/icons-material/Add";
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import FavoriteIcon from '@mui/icons-material/Favorite';
import SettingsIcon from '@mui/icons-material/Settings';
import RateReviewIcon from '@mui/icons-material/RateReview';
import { Link } from "react-router-dom";

const Sidebar = ({role}: any) => {

  const options =[
            {link: "/admin", icon: <DashboardIcon />, name: "Dashboard", role: "admin"}, 
            {link: "/admin/products", icon: <InventoryIcon />, name: "Products", role: "admin"},
            {link: "/admin/orders", icon: <ListAltIcon />, name: "Orders", role: "admin"},
            {link: "/admin/users", icon: <PeopleIcon />, name: "Users", role: "admin"},
            {link: "/admin/reviews", icon: <RateReviewIcon />, name: "Reviews", role: "admin"},
            {link: "/account", icon: <PersonIcon />, name: "Profile", role: "user"}, 
            {link: "/account/orders", icon: <ListAltIcon />, name: "Orders", role: "user"},
            {link: "/account/wishlist", icon: <FavoriteIcon />, name: "Wishlist", role: "user"},
            {link: "", icon: <SettingsIcon />, name: "Settings", role: "user"},
            {link: "/store/product", icon: <AddIcon />, name: "Create", role: "store"}, 
            {link: "/store/products", icon: <InventoryIcon />, name: "Products", role: "store"},
            {link: "/store/orders", icon: <ListAltIcon />, name: "Orders", role: "store"},
            // {link: "/store/reviews", icon: <RateReviewIcon />, name: "Reviews", role: "store"},
            {link: "", icon: <SettingsIcon />, name: "Settings", role: "store"},
            ]


  return (
    <div className="sidebar">
      { role && options.map((option: any, index: number) => { 
        return <Link to={option.link} style={{display: option.role == role ? "block" : "none"}} key={index}>
               <p>{option.icon}{option.name}</p>
                </Link>
        })}
    </div>
  );
};

export default Sidebar;
