import './css/header.css'
import SearchIcon from '@mui/icons-material/Search'
import Badge from '@mui/material/Badge'
import MailIcon from '@mui/icons-material/Mail'
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart'
import MailOutlineIcon from '@mui/icons-material/MailOutline'
import LockOpenIcon from '@mui/icons-material/LockOpen'
import PersonOutlineIcon from '@mui/icons-material/PersonOutline'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'

import { Fragment, useRef, useState, useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import {
  clearErrors,
  login,
  register,
  logout,
} from '../../features/user/userAction'
import { getCartItems } from '../../features/cart/cartAction'

function Header() {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const [keyword, setKeyword] = useState('')
  const [loginOpen, setLoginOpen] = useState(false)
  const [userDropdown, setUserDropdown] = useState(false)
  const [loginEmail, setLoginEmail] = useState('admin@gmail.com')
  const [loginPassword, setLoginPassword] = useState('000000')
  const [newUser, setNewUser] = useState({ name: '', email: '', password: '' })

  const { cartItems, error: cartErr } = useSelector((state: any) => state.cart)
  const { user, error, isAuthenticated } = useSelector(
    (state: any) => state.user,
  )

  const loginTab = useRef<HTMLFormElement>(null)
  const registerTab = useRef<HTMLFormElement>(null)
  const switcherTab = useRef<HTMLButtonElement>(null)

  const searchSubmitHandler = (e: any) => {
    e.preventDefault()
    if (keyword.trim()) {
      let search = keyword.trim().replaceAll(' ', '+')
      navigate(`/results?search=${search}`)
    } else {
      navigate('/results')
    }
  }

  const { name, email, password } = newUser

  const loginFormToggle = () => {
    loginOpen ? setLoginOpen(false) : setLoginOpen(true)
  }

  const handleLoginClose = () => {
    setLoginOpen(false)
  }

  const userDropdownToggle = () => {
    userDropdown ? setUserDropdown(false) : setUserDropdown(true)
  }

  const loginSubmit = (e: any) => {
    e.preventDefault()
    dispatch<any>(login(loginEmail, loginPassword))

    if (error) {
      console.log(error)
      dispatch<any>(clearErrors())
    }
  }

  const registerSubmit = (e: any) => {
    e.preventDefault()

    const myForm = new FormData()

    myForm.set('name', name)
    myForm.set('email', email)
    myForm.set('password', password)

    dispatch<any>(register(myForm))
  }

  const registerDataChange = (e: any) => {
    setNewUser({ ...newUser, [e.target.name]: e.target.value })
    console.log(newUser)
  }

  const switchTabs = (e: any, tab: string) => {
    if (tab === 'login') {
      switcherTab.current?.classList.add('shiftToNeutral')
      switcherTab.current?.classList.remove('shiftToRight')

      registerTab.current?.classList.remove('shiftToNeutralForm')
      loginTab.current?.classList.remove('shiftToLeft')
    }
    if (tab === 'register') {
      switcherTab.current?.classList.add('shiftToRight')
      switcherTab.current?.classList.remove('shiftToNeutral')

      registerTab.current?.classList.add('shiftToNeutralForm')
      loginTab.current?.classList.add('shiftToLeft')
    }
  }

  useEffect(() => {
    if (cartErr) {
      console.log(cartErr)
      dispatch<any>(clearErrors())
    }

    dispatch<any>(getCartItems())
  }, [dispatch, cartErr, isAuthenticated])

  function userLogOut() {
    dispatch<any>(logout())
    console.log('Logout Successfully')
    userDropdownToggle()
  }

  const msg = []

  return (
    <Fragment>
      <div className="header">
        <Link to="/" className="headerLogo">
          <div>Jamkel</div>
        </Link>

        <form className="headerSearch" onSubmit={searchSubmitHandler}>
          <SearchIcon className="headerSearchScon" />
          <input
            className="headerSearchInput"
            type="text"
            placeholder="Search a product "
            onChange={(e) => setKeyword(e.target.value)}
          />
          <input className="headerSearchBtn" type="submit" value="search" />
        </form>

        <div className="headerNavRight">
          <Link to={`/chat`}>
            <div className="headerMsg">
              <Badge badgeContent={msg?.length || 0} className="msgBadge">
                <MailIcon className="headerSvgIcon" sx={{ fontSize: 32 }} />
              </Badge>
            </div>
          </Link>

          <Link to="/cart">
            <div className="headerShoppingCart">
              <Badge badgeContent={cartItems?.length || 0} color="primary">
                <ShoppingCartIcon
                  className="headerSvgIcon"
                  sx={{ fontSize: 32 }}
                />
              </Badge>
            </div>
          </Link>

          <div className="headerUser">
            {isAuthenticated ? (
              <Fragment>
                <img
                  className="userImg"
                  src={
                    user.img_url
                      ? user.img_url
                      : '/assets/user-img/userIcon.jpg'
                  }
                  alt={user.name}
                  onClick={userDropdownToggle}
                />
                <div className={`userDropdownContainer ${userDropdown}`}>
                  <div className="userDropdown">
                    {/* <ul> */}
                    <div>
                      <span className="userDropdown">
                        Hello! <span id="displayUsername">{user.name}</span>
                      </span>
                    </div>
                    {user.role == 'admin' && (
                      <div
                        className="userDropdown"
                        onClick={userDropdownToggle}
                      >
                        <Link to={`/admin`}>Dashboard</Link>
                      </div>
                    )}
                    <div className="userDropdown" onClick={userDropdownToggle}>
                      <Link to={`/account`}>My Account</Link>
                    </div>
                    <div className="userDropdown" onClick={userDropdownToggle}>
                      <Link to={`/store`}>My Store</Link>
                    </div>
                    <div className="userDropdown signOut" onClick={userLogOut}>
                      Sign out
                    </div>
                    {/* </ul> */}
                  </div>
                </div>
              </Fragment>
            ) : (
              <div className="headerNavLogin" onClick={loginFormToggle}>
                <span className="headerNavGuest">Hello Guest</span>
                <span className="headerNavSignin">Sign In</span>
              </div>
            )}
          </div>
        </div>
      </div>

      <Dialog open={loginOpen} onClose={handleLoginClose}>
        <div className="LoginSignUpContainer">
          <div className="LoginSignUpBox">
            <div>
              <div className="loginSignUpToggle">
                <p onClick={(e) => switchTabs(e, 'login')}>LOGIN</p>
                <p onClick={(e) => switchTabs(e, 'register')}>REGISTER</p>
              </div>
              <button ref={switcherTab}></button>
            </div>

            <DialogContent>
              <form className="loginForm" ref={loginTab} onSubmit={loginSubmit}>
                <div className="loginEmail">
                  <MailOutlineIcon />
                  <input
                    type="email"
                    placeholder="Email"
                    required
                    value={loginEmail}
                    onChange={(e) => setLoginEmail(e.target.value)}
                  />
                </div>

                <div className="loginPassword">
                  <LockOpenIcon />
                  <input
                    type="password"
                    placeholder="Password"
                    required
                    value={loginPassword}
                    onChange={(e) => setLoginPassword(e.target.value)}
                  />
                </div>

                <Link to="/password/forgot">Forget Password ?</Link>
                <DialogActions>
                  <input
                    type="button"
                    value="Cancel"
                    className="signUpBtn"
                    onClick={handleLoginClose}
                  />
                  <input
                    type="submit"
                    value="Login"
                    className="loginBtn"
                    onClick={handleLoginClose}
                  />
                </DialogActions>
              </form>

              <form
                className="signUpForm"
                ref={registerTab}
                encType="multipart/form-data"
                onSubmit={registerSubmit}
              >
                <div className="signUpName">
                  <PersonOutlineIcon />
                  <input
                    type="text"
                    placeholder="Name"
                    required
                    name="name"
                    value={name}
                    onChange={registerDataChange}
                  />
                </div>

                <div className="signUpEmail">
                  <MailOutlineIcon />
                  <input
                    type="email"
                    placeholder="Email"
                    required
                    name="email"
                    value={email}
                    onChange={registerDataChange}
                  />
                </div>

                <div className="signUpPassword">
                  <LockOpenIcon />
                  <input
                    type="password"
                    placeholder="Password"
                    required
                    name="password"
                    value={password}
                    onChange={registerDataChange}
                  />
                </div>

                <DialogActions>
                  <input
                    type="button"
                    value="Cancel"
                    className="signUpBtn"
                    onClick={handleLoginClose}
                  />
                  <input
                    type="submit"
                    value="Register"
                    className="signUpBtn"
                    onClick={handleLoginClose}
                  />
                </DialogActions>
              </form>
            </DialogContent>
          </div>
        </div>
      </Dialog>
    </Fragment>
  )
}

export default Header
