import { Fragment} from 'react'

function CloudBtn() {

    function handleOpenWidget() {
 
        var myWidget = window.cloudinary.createUploadWidget({
          cloudName: 'diqlgacag', 
          uploadPreset: 'jamkel'
        }, 
          (error, result) => { 
            if (!error && result && result.event === "success") { 
              console.log('Done! Here is the image info: ', result.info); 
            }
          })
          myWidget.open();
        }

  return (
    <Fragment>
 <button id='upload_widget' className='cloudinary-button' onClick={() => handleOpenWidget()}>
              Upload pictures
            </button>

            <div id="createProductFormImage"></div>
    </Fragment>

  )
}

export default CloudBtn