import Helmet from "react-helmet";

const DocTitle = ({ title }: any) => {
  return (
    <Helmet>
      <title>{title}</title>
    </Helmet>
  );
};

export default DocTitle;