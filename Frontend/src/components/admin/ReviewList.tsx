import "./css/reviewList.css";
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import StarIcon from '@mui/icons-material/Star';
import { DataGrid } from '@mui/x-data-grid';

import DocTitle from "../layout/DocTitle";
import SideBar from "../layout/Sidebar";

import { Fragment, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { clearErrors, getAllReviews, deleteReviews } from "../../features/product/productAction";
import { DELETE_REVIEW_RESET } from "../../features/product/productConstant";

const ProductReviews = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { id, rating } = useParams();

  const { error: deleteError, isDeleted } = useSelector((state: any) => state.review);
  const { error, reviews, loading } = useSelector((state: any) => state.productReviews);

  const [productId, setProductId] = useState("");

  const deleteReviewHandler = (reviewId: number) => {
    dispatch<any>(deleteReviews(reviewId, Number(productId)));
  };

  const productReviewsSubmitHandler = (e: any) => {
    e.preventDefault();
    dispatch<any>(getAllReviews(Number(productId)));
  };

  useEffect(() => {
    if (productId.length === 24) {
      dispatch<any>(getAllReviews(Number(productId)));
    }
    if (error) {
      console.log(error);
      dispatch<any>(clearErrors());
    }

    if (deleteError) {
      console.log(deleteError);
      dispatch<any>(clearErrors());
    }

    if (isDeleted) {
      console.log("Review Deleted Successfully");
      navigate("/admin/reviews");
      dispatch<any>({ type: DELETE_REVIEW_RESET });
    }
  }, [dispatch, error, deleteError, isDeleted, productId]);

  const columns = [
    { field: "id", headerName: "Review ID", minWidth: 200, flex: 0.5 },

    {
      field: "user",
      headerName: "User",
      minWidth: 200,
      flex: 0.6,
    },

    {
      field: "comment",
      headerName: "Comment",
      minWidth: 350,
      flex: 1,
    },

    {
      field: "rating",
      headerName: "Rating",
      type: "number",
      minWidth: 180,
      flex: 0.4,

      cellClassName: () => {
        return Number(rating) >= 3
          ? "greenColor"
          : "redColor";
      },
    },

    {
      field: "actions",
      flex: 0.3,
      headerName: "Actions",
      minWidth: 150,
      type: "number",
      sortable: false,
      renderCell: () => {
        return (
          <Fragment>
            <Button
              onClick={() =>
                deleteReviewHandler(Number(id))
              }
            >
              <DeleteIcon />
            </Button>
          </Fragment>
        );
      },
    },
  ];

  const rows: any[] = [];

  reviews &&
    reviews.forEach((item: any) => {
      rows.push({
        id: item.id,
        rating: item.rating,
        comment: item.content,
        user: item.username,
      });
    });

  return (
    <Fragment>
      <DocTitle title={`ALL REVIEWS - Admin`} />

      <div className="dashboard">
        <SideBar role="admin"/>
        <div className="productReviewsContainer">
          <form
            className="productReviewsForm"
            onSubmit={productReviewsSubmitHandler}
          >
            <h1 className="productReviewsFormHeading">ALL REVIEWS</h1>

            <div>
              <StarIcon />
              <input
                type="text"
                placeholder="Product Id"
                required
                value={productId}
                onChange={(e) => setProductId(e.target.value)}
              />
            </div>

            <Button
              id="createProductBtn"
              type="submit"
              disabled={
                loading ? true : false || productId === "" ? true : false
              }
            >
              Search
            </Button>
          </form>

          {reviews && reviews.length > 0 ? (
            <DataGrid
              rows={rows}
              columns={columns}
              pageSize={10}
              disableSelectionOnClick
              className="productListTable"
              autoHeight
            />
          ) : (
            <h1 className="productReviewsFormHeading">No Reviews Found</h1>
          )}
        </div>
      </div>
     </Fragment>
  );
};

export default ProductReviews;
