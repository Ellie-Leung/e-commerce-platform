import "./css/list.css";
import Button from '@mui/material/Button';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { DataGrid } from '@mui/x-data-grid';

import DocTitle from "../layout/DocTitle";
import SideBar from "../layout/Sidebar";

import { Fragment, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link, useNavigate, useParams } from "react-router-dom";
import { getAllUsers, clearErrors, deleteUser } from "../../features/user/userAction";
import { DELETE_USER_RESET } from "../../features/user/userConstant";

const UsersList = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { id, role } = useParams();

  const { error, users } = useSelector((state: any) => state.allUsers);
  const { error: deleteError, isDeleted, message } = useSelector((state: any) => state.userProfile);

  const deleteUserHandler = (id: number) => {
    dispatch<any>(deleteUser(id));
  };

  useEffect(() => {
    if (error) {
      console.log(error);
      dispatch<any>(clearErrors());
    }

    if (deleteError) {
      console.log(deleteError);
      dispatch<any>(clearErrors());
    }

    if (isDeleted) {
      console.log(message);
      navigate("/admin/users");
      dispatch<any>({ type: DELETE_USER_RESET });
    }

    dispatch<any>(getAllUsers());
  }, [dispatch,error, deleteError, isDeleted, message]);

  const columns = [
    { field: "id", headerName: "User ID", minWidth: 180, flex: 0.8 },

    {
      field: "email",
      headerName: "Email",
      minWidth: 200,
      flex: 1,
    },
    {
      field: "name",
      headerName: "Name",
      minWidth: 150,
      flex: 0.5,
    },

    {
      field: "role",
      headerName: "Role",
      type: "number",
      minWidth: 150,
      flex: 0.3,
      cellClassName: () => {
        return role === "admin"
          ? "greenColor"
          : "redColor";
      },
    },

    {
      field: "actions",
      flex: 0.3,
      headerName: "Actions",
      minWidth: 150,
      type: "number",
      sortable: false,
      renderCell: () => {
        return (
          <Fragment>
            <Link to={`/admin/user/${id}`}>
              <EditIcon />
            </Link>

            <Button
              onClick={() =>
                deleteUserHandler(Number(id))
              }
            >
              <DeleteIcon />
            </Button>
          </Fragment>
        );
      },
    },
  ];

  const rows: any[] = [];

  users &&
    users.forEach((item: any) => {
      rows.push({
        id: item.id,
        role: item.role,
        email: item.email,
        name: item.name,
      });
    });

  return (
    <Fragment>
      <DocTitle title={`ALL USERS - Admin`} />

      <div className="dashboard">
        <SideBar role="admin"/>
        <div className="listContainer">
          <h1 id="listHeading">ALL USERS</h1>

          <DataGrid
            rows={rows}
            columns={columns}
            pageSize={10}
            disableSelectionOnClick
            className="listTable"
            autoHeight
          />
        </div>
      </div>
     </Fragment>
  );
};

export default UsersList;
