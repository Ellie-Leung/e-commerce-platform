import "./css/list.css";
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

import DocTitle from "../layout/DocTitle";
import SideBar from "../layout/Sidebar";

import { Fragment, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link, useNavigate, useParams } from "react-router-dom";
import { deleteOrder, getAllOrders, clearErrors } from "../../features/order/orderAction";
import { DELETE_ORDER_RESET } from "../../features/order/orderConstant";

const OrderList = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { id, status } = useParams();

  const { error, orders } = useSelector((state: any) => state.allOrders);
  const { error: deleteError, isDeleted } = useSelector((state: any) => state.order);

  const deleteOrderHandler = (id: number) => {
    dispatch<any>(deleteOrder(id));
  };

  useEffect(() => {
    if (error) {
      console.log(error);
      dispatch<any>(clearErrors());
    }

    if (deleteError) {
        console.log(deleteError);
      dispatch<any>(clearErrors());
    }

    if (isDeleted) {
        console.log("Order Deleted Successfully");
       
      navigate("/admin/orders");
      dispatch({ type: DELETE_ORDER_RESET });
    }

    dispatch<any>(getAllOrders());
  }, [dispatch, error, deleteError, isDeleted]);

  const columns = [
    { field: "id", headerName: "Order#", minWidth: 300, flex: 1 },

    {
      field: "status",
      headerName: "Status",
      minWidth: 150,
      flex: 0.5,
      cellClassName: () => {
        return status === "Delivered"
          ? "greenColor"
          : "redColor";
      },
    },
    {
      field: "qty",
      headerName: "Qty",
      type: "number",
      minWidth: 150,
      flex: 0.4,
    },

    {
      field: "amount",
      headerName: "Amount",
      type: "number",
      minWidth: 270,
      flex: 0.5,
    },

    {
      field: "actions",
      flex: 0.3,
      headerName: "Actions",
      minWidth: 150,
      type: "number",
      sortable: false,
      renderCell: () => {
        return (
          <Fragment>
            <Link to={`/admin/order/${Number(id)}`}>
              <EditIcon />
            </Link>

            <Button
              onClick={() =>
                deleteOrderHandler(Number(id))
              }
            >
              <DeleteIcon />
            </Button>
           </Fragment>
        );
      },
    },
  ];

  const rows: any[] = [];

  orders &&
    orders.forEach((item: any) => {
      rows.push({
        id: item.store_id,
        // itemsQty: item.orderItems.length,
        amount: item.amount,
        status: item.status,
      });
    });

  return (
    <Fragment>
      <DocTitle title={`ALL ORDERS - Admin`} />

      <div className="dashboard">
        <SideBar role="admin"/>
        <div className="listContainer">
          <h1 id="listHeading">ALL ORDERS</h1>

          <DataGrid
            rows={rows}
            columns={columns}
            pageSize={10}
            disableSelectionOnClick
            className="listTable"
            autoHeight
          />
        </div>
      </div>
     </Fragment>
  );
};

export default OrderList;