import "./css/updateOrder.css";
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

import DocTitle from "../layout/DocTitle";
import SideBar from "../layout/Sidebar";
import Loader from "../layout/Loader";

import { Fragment, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { getOrderDetails, clearErrors, updateOrder } from "../../features/order/orderAction";
import { UPDATE_ORDER_RESET } from "../../features/order/orderConstant";


const ProcessOrder = () => {
  const dispatch = useDispatch();
  const { id } = useParams();

  const [status, setStatus] = useState("");
  const { order, error, loading } = useSelector((state: any) => state.orderDetails);
  const { error: updateError, isUpdated } = useSelector((state: any) => state.order);

  const updateOrderSubmitHandler = (e: any) => {
    e.preventDefault();

    const myForm = new FormData();

    myForm.set("status", status);

    dispatch<any>(updateOrder(Number(id), myForm));
  };


  useEffect(() => {
    if (error) {
      console.log(error);
      dispatch<any>(clearErrors());
    }
    if (updateError) {
      console.log(updateError);
      dispatch<any>(clearErrors());
    }
    if (isUpdated) {
      console.log("Order Updated Successfully");
      dispatch<any>({ type: UPDATE_ORDER_RESET });
    }

    dispatch<any>(getOrderDetails(Number(id)));
  }, [dispatch, error, id, isUpdated, updateError]);

  return (
    <Fragment>
      <DocTitle title="Process Order" />
      <div className="dashboard">
        <SideBar />
        <div className="newProductContainer">
          {loading ? (
            <Loader />
          ) : (
            <div
              className="confirmOrderPage"
              style={{
                display: order.orderStatus === "Delivered" ? "block" : "grid",
              }}
            >
              <div>
                <div className="confirmshippingArea">
                  <Typography>Shipping Info</Typography>
                  <div className="orderDetailsContainerBox">
                    <div>
                      <p>Name:</p>
                      <span>{order && order.receiver}</span>
                    </div>
                    <div>
                      <p>Phone:</p>
                      <span>
                        {order && order.contact_number}
                      </span>
                    </div>
                    <div>
                      <p>Address:</p>
                      <span>
                        {order && order.address}
                      </span>
                    </div>
                  </div>

                  <Typography>Payment</Typography>
                  <div className="orderDetailsContainerBox">
                    <div>
                      <p
                        className={
                          order && order.status === "succeeded"
                            ? "greenColor"
                            : "redColor"
                        }
                      >
                        {order && order.status === "succeeded"
                          ? "PAID"
                          : "NOT PAID"}
                      </p>
                    </div>

                    <div>
                      <p>Amount:</p>
                      <span>{order && order.amount}</span>
                    </div>
                  </div>

                  <Typography>Order Status</Typography>
                  <div className="orderDetailsContainerBox">
                    <div>
                      <p
                        className={
                          order && order.status === "Delivered"
                            ? "greenColor"
                            : "redColor"
                        }
                      >
                        {order && order.status}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="confirmCartItems">
                  <Typography>Your Cart Items:</Typography>
                  <div className="confirmCartItemsContainer">
                    {order.orderItems &&
                      order.orderItems.map((item: any) => (
                        <div key={item.product}>
                          <img src={item.image} alt="Product" />
                          <Link to={`/product/${item.product}`}>
                            {item.name}
                          </Link>{" "}
                          <span>
                            {item.quantity} X ₹{item.price} ={" "}
                            <b>₹{item.price * item.quantity}</b>
                          </span>
                        </div>
                      ))}
                  </div>
                </div>
              </div>
  
              <div
                style={{
                  display: order.orderStatus === "Delivered" ? "none" : "block",
                }}
              >
                <form
                  className="updateOrderForm"
                  onSubmit={updateOrderSubmitHandler}
                >
                  <h1>Process Order</h1>

                  <div>
                    <AccountTreeIcon />
                    <select onChange={(e) => setStatus(e.target.value)}>
                      <option value="">Choose Category</option>
                      {order.orderStatus === "Processing" && (
                        <option value="Shipped">Shipped</option>
                      )}

                      {order.orderStatus === "Shipped" && (
                        <option value="Delivered">Delivered</option>
                      )}
                    </select>
                  </div>

                  <Button
                    id="createProductBtn"
                    type="submit"
                    disabled={
                      loading ? true : false || status === "" ? true : false
                    }
                  >
                    Process
                  </Button>
                </form>
              </div>
            </div>
          )}
        </div>
      </div>
     </Fragment>
  );
};

export default ProcessOrder;
