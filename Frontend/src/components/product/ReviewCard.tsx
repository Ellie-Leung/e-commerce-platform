import Rating from '@mui/material/Rating';

export interface ReviewCardProps {
  id: number
  user_img: string
  user_name: string
  rating: number
  content: string
}

const ReviewCard = (props:ReviewCardProps ) => {
console.log(props)
//   const options = {
//     value: review.rating,
//     readOnly: true,
//     precision: 0.5,
//   };

  return (
    <div className="reviewCard">
      <img src={props.user_img? props.user_img
                      : '/assets/user-img/userIcon.jpg'} alt="User" />
      <p>{props.user_name}</p>
      <Rating value={props.rating} precision={0.5} readOnly />
      <span className="reviewCardComment">{props.content}</span>
    </div>
  );
};

export default ReviewCard;
