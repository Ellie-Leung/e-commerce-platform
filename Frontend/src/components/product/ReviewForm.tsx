import "./css/reviewForm.css"
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Rating from '@mui/material/Rating';
import Button from '@mui/material/Button';
import { useParams } from 'react-router';
import { useDispatch } from 'react-redux';
import { useState } from "react";
import { newReview} from "../../features/product/productAction";


function ReviewForm() {
  const dispatch = useDispatch();
const { id } = useParams();

  const [open, setOpen] = useState(false);
  const [rating, setRating] = useState(0);
  const [review, setReview] = useState("");

  const submitReviewToggle = () => {
    open ? setOpen(false) : setOpen(true);
  };

  const reviewSubmitHandler = () => {
    const myForm: any = new FormData();

    myForm.set("rating", rating);
    myForm.set("comment", review);
    myForm.set("productId", id);

    dispatch<any>(newReview(myForm));

    setOpen(false);
  };

  return (


          <Dialog
            aria-labelledby="simple-dialog-title"
            open={open}
            onClose={submitReviewToggle}
          >
            <DialogTitle>Submit Review</DialogTitle>
             <DialogContent className="submitDialog">
              <Rating
                name="simple-controlled"
                value={rating}
                precision={0.5}
                onChange={(e) => {setRating(Number((e.target as HTMLInputElement).value));}}
              />   

              <textarea
                className="submitDialogTextArea"
                cols={30}
                rows={5}
                value={review}
                onChange={(e) => setReview(e.target.value)}
              ></textarea>
            </DialogContent>
            <DialogActions>
              <Button onClick={submitReviewToggle} >
                Cancel
              </Button>
              <Button onClick={reviewSubmitHandler} >
                Submit
              </Button>
            </DialogActions>
          </Dialog>
  )
}

export default ReviewForm