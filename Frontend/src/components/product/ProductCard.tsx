import './css/productCard.css'
import { Link } from 'react-router-dom'
import Rating from '@mui/material/Rating'

import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import Skeleton from '@mui/material/Skeleton'

import NumberFormat from 'react-number-format'
import { useSelector } from 'react-redux'

export interface ProductProps {
  id: number
  name: string
  images_url: string[]
  images_id: string[]
  store_name: string
  store_id: number
  price: number
  avg_rating: number
  numOfReviews: number
}

function ProductCard() {

  const { products } = useSelector((state: any) => state.products)

  return (
    <div className="productCardContainer">
      {products &&
        products.map((product: ProductProps, index: number) => (
          <Box className="productCard" key={index}>
            {product ? (
              <Link to={`/product/${product.id}`}>
                <div className="productImg">
                  <img alt={product.name} src={product.images_url[0]} />
                </div>
              </Link>
            ) : (
              <Skeleton variant="rectangular" width={210} height={118} />
            )}
            {product ? (
              <Box sx={{ pr: 2 }}>
                <Link className="productLink" to={`/product/${product.id}`}>
                  <Typography gutterBottom variant="body2" marginTop={0.5}>
                    {product.name}
                  </Typography>
                </Link>
                <Link className="storeLink" to={`/store/${product.store_id}`}>
                  <Typography
                    className="storeName"
                    display="block"
                    variant="caption"
                    color="text.secondary"
                  >
                    {product.store_name}
                  </Typography>
                </Link>
                <Typography
                  display="flex"
                  variant="caption"
                  color="text.secondary"
                >
                  <Rating value={product.avg_rating} size="small" readOnly />
                  {`  • (${product.numOfReviews} Reviews)`}
                </Typography>
                <Typography
                  display="block"
                  variant="caption"
                  color="text.first"
                  fontSize={15}
                >
                  <NumberFormat
                    value={Number(product.price)}
                    thousandSeparator={true}
                    prefix={'$'}
                    displayType={'text'}
                  />
                </Typography>
              </Box>
            ) : (
              <Box sx={{ pt: 0.5 }}>
                <Skeleton />
                <Skeleton width="60%" />
              </Box>
            )}
          </Box>
        ))}
    </div>
  )
}

export default ProductCard
