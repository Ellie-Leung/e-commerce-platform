import './css/Slider.css'
import Carousel from 'react-material-ui-carousel'

const sliderItems = [
  {
    id: 1,
    img: '/assets/mug-image.jpg',
    title: 'SUMMER SALE',
  },
  {
    id: 2,
    img: '/assets/product-img/l1.jpg',
    title: 'AUTUMN COLLECTION',
  },
]

function Slider() {
  return (
    <Carousel className="sliderContainer">
      {sliderItems &&
        sliderItems.map((sliderItem) => (
          <img
            className="sliderImage"
            key={sliderItem.id}
            src={sliderItem.img}
            alt={sliderItem.title}
          />
        ))}
    </Carousel>
  )
}

export default Slider
