import { useState, useEffect } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { clearErrors, getProduct } from '../../features/product/productAction'
import { useSelector, useDispatch } from 'react-redux'
import Slider from '@mui/material/Slider'
import Typography from '@mui/material/Typography'
// import TreeView from '@mui/lab/TreeView';
// import TreeItem from '@mui/lab/TreeItem';
// import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

const categories = [
  {
    name: 'Housewares',
  },
  {
    name: 'Electronics',
    // sub: [{ name: 'Laptop' },
    //       { name: 'Camera' },
    //       { name: 'SmartPhones' } ]
  },
  {
    name: 'Fashion',
    // sub: [ {
    //         name: 'Women',
    //         sub: [{ name: 'Top' },
    //               { name: 'Bottom' },
    //               { name: 'One-piece' },
    //               { name: 'Bag' },
    //               { name: 'Footwear' } ]
    //       },
    //       {
    //         name: 'Men',
    //         sub: [{ name: 'Top' },
    //               { name: 'Bottom' },
    //               { name: 'Bag' },
    //               { name: 'Footwear' } ]
    //       } ]
  },
  {
    name: 'Personal Care',
  },
  {
    name: 'Home Appliance',
  },
  {
    name: 'Sports & Travel',
  },
  {
    name: 'Toys',
  },
  {
    name: 'Pets',
  },
]

function FilterCard() {
  const { search } = useLocation()
  const navigate = useNavigate()

  const dispatch = useDispatch()
  const [minPrice, setMinPrice] = useState('')
  const [maxPrice, setMaxPrice] = useState('')
  const [category, setCategory] = useState('')
  const [ratings, setRatings] = useState<number | number[]>([0, 5])
  const { error } = useSelector((state: any) => state.products)

  const getQueryArr = () => {
    let queryArr = []

    if (minPrice) {
      queryArr.push('priceflr=' + minPrice)
    }
    if (maxPrice) {
      queryArr.push('priceceil=' + maxPrice)
    }
    if (category) {
      queryArr.push('category=' + category)
    }
    if (ratings && ratings instanceof Array) {
      queryArr.push(
        'ratingflr=' + ratings[0] + '&' + 'ratingceil=' + ratings[1],
      )
    }

    return queryArr
  }

  const getQueryStr = () => {
    let queryStr: string

    if (search) {
      queryStr = search + '&' + getQueryArr().toString().replaceAll(',', '&')
    } else {
      queryStr = '?' + getQueryArr().toString().replaceAll(',', '&')
    }

    return queryStr
  }

  useEffect(() => {
    if (
      minPrice !== '' ||
      maxPrice !== '' ||
      ratings !== [0, 5] ||
      category !== ''
    ) {
      let queryStr = getQueryStr()
      console.log(queryStr)
      dispatch<any>(getProduct(queryStr))
    }
  }, [minPrice, maxPrice, ratings, category])

  return (
    <div className="filterBox">
      <fieldset className="priceBox">
        <Typography className="filterTitle" component="legend">
          Price
        </Typography>
        <input
          className="priceInput"
          type="text"
          value={minPrice}
          onChange={(e) => setMinPrice(e.target.value)}
        ></input>
        <p> - </p>
        <input
          className="priceInput"
          type="text"
          value={maxPrice}
          onChange={(e) => setMaxPrice(e.target.value)}
        ></input>
      </fieldset>

      <fieldset>
        <Typography className="filterTitle" component="legend">
          Ratings Above
        </Typography>
        <Slider
          value={ratings}
          onChange={(e, newRating) => {
            setRatings(newRating)
          }}
          aria-labelledby="range-slider"
          valueLabelDisplay="auto"
          min={0}
          max={5}
          size="small"
        />
      </fieldset>

      <fieldset>
        <Typography className="categoryTitle" component="legend">
          Categories
        </Typography>
        <ul className="categoryBox">
          {categories.map((category: { name: string; sub?: any[] }) => {
            return (
              <li
                className="category-link"
                key={category.name}
                onClick={() => setCategory(category.name)}
              >
                {category.name}
                {category.sub &&
                  category.sub.map((cate2: { name: string; sub: any[] }) => {
                    return (
                      <ul className="categoryBox" key={cate2.name}>
                        <li className="category-link">
                          {cate2.name}
                          {cate2.sub &&
                            cate2.sub.map(
                              (cate3: { name: string; sub: any[] }) => {
                                return (
                                  <ul className="categoryBox" key={cate3.name}>
                                    <li className="category-link">
                                      {cate3.name}
                                    </li>
                                  </ul>
                                )
                              },
                            )}
                        </li>
                      </ul>
                    )
                  })}
              </li>
            )
          })}
        </ul>
      </fieldset>
    </div>
  )
}

export default FilterCard
