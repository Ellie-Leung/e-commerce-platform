import "./css/forgotPassword.css";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import Loader from "../layout/Loader";
import DocTitle from "../layout/DocTitle";
import { Fragment, useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { clearErrors, forgotPassword } from "../../features/user/userAction";


const ForgotPassword = () => {
  const dispatch = useDispatch();

  const { error, message, loading } = useSelector((state: any) => state.forgotPassword);
  const [email, setEmail] = useState("");

  const forgotPasswordSubmit = (e: any) => {
    e.preventDefault();
    dispatch<any>(forgotPassword(email));
  };

  useEffect(() => {
    if (error) {
      console.log(error);
      dispatch<any>(clearErrors());
    }

    if (message) {
      console.log(message);
    }
  }, [dispatch, error, message]);

  return (
    <Fragment>
      {loading ? (
        <Loader />
      ) : (
        <Fragment>
          <DocTitle title="Forgot Password" />
          <div className="forgotPasswordContainer">
            <div className="forgotPasswordBox">
              <h2 className="forgotPasswordHeading">Forgot Password</h2>

              <form
                className="forgotPasswordForm"
                onSubmit={forgotPasswordSubmit}
              >
                <div className="forgotPasswordEmail">
                  <MailOutlineIcon />
                  <input
                    type="email"
                    placeholder="Email"
                    required
                    name="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>

                <input
                  type="submit"
                  value="Send"
                  className="forgotPasswordBtn"
                />
              </form>
            </div>
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

export default ForgotPassword;
