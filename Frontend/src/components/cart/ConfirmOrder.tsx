import "./css/confirmOrder.css";
import Typography from '@mui/material/Typography';

import CheckoutSteps from "./CheckoutSteps";
import DocTitle from "../layout/DocTitle";

import NumberFormat from 'react-number-format';
import { Fragment } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";


const ConfirmOrder = () => {
const navigate = useNavigate()

  const { shippingInfo, cartItems } = useSelector((state: any) => state.cart);
  const { user } = useSelector((state: any) => state.user);

  const subtotal = cartItems.reduce(
    (acc: number, item: any) => acc + item.quantity * item.price,
    0
  );

  const shippingCharges = subtotal > 1000 ? 0 : 50;

  const tax = Math.round(subtotal * 0.02 * 100)/100;

  const totalPrice = subtotal + tax + shippingCharges;

  const address = `${shippingInfo.address}, ${shippingInfo.city}, ${shippingInfo.state}, ${shippingInfo.pinCode}, ${shippingInfo.country}`;

  const proceedToPayment = () => {
    const data = {
      subtotal,
      shippingCharges,
      tax,
      totalPrice,
    };

    sessionStorage.setItem("orderInfo", JSON.stringify(data));

    navigate("/checkout");
  };
console.log(cartItems)
  return (
    <Fragment>
      <DocTitle title="Confirm Order" />
      <CheckoutSteps activeStep={1} />
      <div className="confirmOrderPage">
         <div>
          <div className="confirmshippingArea">
            <Typography>Shipping Info</Typography>
            <div className="confirmshippingAreaBox">
              <div>
                <p>Name:</p>
                <span>{user.name}</span>
              </div>
              <div>
                <p>Phone:</p>
                <span>{shippingInfo.phoneNo}</span>
              </div>
              <div>
                <p>Address:</p>
                <span>{address}</span>
              </div>
            </div>
          </div>
          <div className="confirmCartItems">
            <Typography>Your Cart Items:</Typography>
            <div className="confirmCartItemsContainer">
              {cartItems && cartItems.map((item: any) => (
                  <div key={item.id}>
                    <img src={`/assets/product-img/${item.images[0]}`} alt="Product" />
                    <Link to={`/product/${item.id}`}>
                      {item.name}
                    </Link>{" "}
                    <span>
                      {item.quantity} X 
                      <NumberFormat 
                value={Number(item.price)}
                thousandSeparator={true}
                prefix={'$'}
                displayType={'text'}
                />
                       ={" "}
                      <b>
                      <NumberFormat 
                value={Number(item.price * item.quantity)}
                thousandSeparator={true}
                prefix={'$'}
                displayType={'text'}
                />
            
                        </b>
                    </span>
                  </div>
                ))}
            </div>
          </div> 
        </div>
 
         <div>
           <div className="orderSummary">
             <Typography>Order Summery</Typography>
             <div>
               <div>
                 <p>Subtotal:</p>
                 <span>
                 <NumberFormat 
                value={subtotal}
                thousandSeparator={true}
                prefix={'$'}
                displayType={'text'}
                />
            </span>
               </div>
               <div>
                 <p>Shipping Charges:</p>
                 <span>
                 <NumberFormat 
                value={shippingCharges}
                thousandSeparator={true}
                prefix={'$'}
                displayType={'text'}
                />
                  </span>
               </div>
               <div>
                 <p>Tax:</p>
                 <span>
                 <NumberFormat 
                value={tax}
                thousandSeparator={true}
                prefix={'$'}
                displayType={'text'}
                />
                  
                  </span>
               </div>
             </div>

             <div className="orderSummaryTotal">
               <p>
                 <b>Total:</b>
               </p>
               <span><NumberFormat 
                value={totalPrice}
                thousandSeparator={true}
                prefix={'$'}
                displayType={'text'}
                />
                </span>
             </div>

             <button onClick={proceedToPayment}>Proceed To Payment</button>
           </div>
         </div>
       </div>
   </Fragment>
  );
};

export default ConfirmOrder;
