import "./css/cartItemCard.css";
import { Link } from "react-router-dom";
import NumberFormat from 'react-number-format';

const CartItemCard = ({ item, deleteCartItems }: any) => {

  return (
    <div className="CartItemCard">
      <Link to={`/product/${item.product_id}`} className="CartImgBox">
      <img src={item.images[0]} alt="ssa" />
      </Link>
      <div>
        <Link to={`/product/${item.product_id}`} className="CartItemName">{item.name}</Link>
        <Link to={`/product/${item.store_id}`} className="CartItemStore">{item.store_name}</Link>
        <span>Price:
        <NumberFormat 
                value={item.price}
                thousandSeparator={true}
                prefix={'$'}
                decimalScale={2}
                displayType={'text'}
                /> </span>
        <p onClick={() => deleteCartItems(item.id)}>Remove</p>
      </div>
    </div>
  );
};

export default CartItemCard;