import "./css/newProductForm.css";
import Button from '@mui/material/Button';
import AccountTreeIcon from "@mui/icons-material/AccountTree";
import DescriptionIcon from "@mui/icons-material/Description";
import StorageIcon from "@mui/icons-material/Storage";
import SpellcheckIcon from "@mui/icons-material/Spellcheck";
import AttachMoneyIcon from "@mui/icons-material/AttachMoney";

import DocTitle from "../layout/DocTitle";
import Sidebar from "../layout/Sidebar";

import { Fragment, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { clearErrors, createProduct } from "../../features/product/productAction";
import { NEW_PRODUCT_RESET } from "../../features/product/productConstant";

const NewProduct = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { error, success } = useSelector((state) => state.newProduct);

  const [name, setName] = useState("");
  const [price, setPrice] = useState(0);
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");
  const [stock, setStock] = useState(0);
  const [images, setImages] = useState([]);
  // const [imagesPreview, setImagesPreview] = useState([]);

  const categories = [
    "Fashion",
    "Electronics",
    "Personal Care",
    "Home Appliance",
    "Sports & Travel",
    "Pets",
  ];

  // useEffect(() => {
  //   if (error) {
  //     console.log(error);
  //     dispatch(clearErrors());
  //   }

  //   if (success) {
  //     console.log("Product Created Successfully");
  //     navigate("/admin/dashboard");
  //     dispatch({ type: NEW_PRODUCT_RESET });
  //   }
  // }, [dispatch, error, success]);

  const createProductSubmitHandler = (e) => {
    e.preventDefault();
    const myForm = new FormData();

    myForm.set("name", name);
    myForm.set("price", price);
    myForm.set("description", description);
    myForm.set("category", category);
    myForm.set("stock", stock);

    images.forEach((image) => {
      myForm.append("images_id", image.public_id);
      myForm.append("images_url", image.url);

    console.log(myForm)
    });
    dispatch(createProduct(myForm));
  };

  // const createProductImagesChange = (e) => {
  //   const files = Array.from(e.target.files);

  //   setImages([]);
  //   setImagesPreview([]);

  //   files.forEach((file) => {
  //     const reader = new FileReader();

  //     reader.onload = () => {
  //       if (reader.readyState === 2) {
  //         setImagesPreview((old) => [...old, reader.result]);
  //         setImages((old) => [...old, reader.result]);
  //       }
  //     };

  //     reader.readAsDataURL(file);
  //   });
  // };

  function handleOpenWidget() {
 
    var myWidget = window.cloudinary.createUploadWidget({
      cloudName: 'diqlgacag', 
      uploadPreset: 'jamkel'
    }, 
      (error, result) => { 
        if (!error && result && result.event === "success") { 
          setImages((prev) => [...prev, {url: result.info.url, public_id: result.info.public_id}]) 
        }
      })
      myWidget.open();
    }

  return (
    <Fragment>
      <DocTitle title="Create Product" />
      <div className="dashboard">
        <Sidebar role="store"/>
        <div className="newProductContainer">
          <form
            className="createProductForm"
            encType="multipart/form-data"
            onSubmit={createProductSubmitHandler}
          >
            <h1>Create Product</h1>

            <div>
              <SpellcheckIcon />
              <input
                type="text"
                placeholder="Product Name"
                required
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </div>
            <div>
              <AttachMoneyIcon />
              <input
                type="number"
                placeholder="Price"
                required
                onChange={(e) => setPrice(Number(e.target.value))}
              />
            </div>

            <div>
              <DescriptionIcon />

              <textarea
                placeholder="Product Description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                cols={30}
                rows={1}
              ></textarea>
            </div>

            <div>
              <AccountTreeIcon />
              <select onChange={(e) => setCategory(e.target.value)}>
                <option value="">Choose Category</option>
                {categories.map((cate) => (
                  <option key={cate} value={cate}>
                    {cate}
                  </option>
                ))}
              </select>
            </div>

            <div>
              <StorageIcon />
              <input
                type="number"
                placeholder="Stock"
                required
                onChange={(e) => setStock(Number(e.target.value))}
              />
            </div>

            <div id="createProductFormFile">
            <div id='upload_widget' className='cloudinary-button' onClick={() => handleOpenWidget()}>
              Upload pictures
            </div>
            </div>

            <div id="createProductFormImage">
              {images.map((image, index) => (
                <img key={index} src={image.url} alt="Product Preview" />
              ))}
            </div>
           
            {/* <div id="createProductFormFile">
              <input
                type="file"
                name="product"
                accept="image/*"
                onChange={createProductImagesChange}
                multiple
              />
            </div> */}

            {/* <div id="createProductFormImage">
              {imagesPreview.map((image, index) => (
                <img key={index} src={image} alt="Product Preview" />
              ))}
            </div> */}

            <Button
              id="createProductBtn"
              type="submit"
              // disabled={loading ? true : false}
            >
              Create
            </Button>
          </form>
        </div>
      </div>
    // </Fragment>
  );
};

export default NewProduct;
