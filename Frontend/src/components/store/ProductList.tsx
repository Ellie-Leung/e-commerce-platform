import "../admin/css/list.css";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';

import DocTitle from "../layout/DocTitle";
import SideBar from "../layout/Sidebar";

import { Fragment, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link, useNavigate, useParams } from "react-router-dom";
import { clearErrors, getAdminProduct, deleteProduct } from "../../features/product/productAction";
import { DELETE_PRODUCT_RESET } from "../../features/product/productConstant";

const ProductList = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { id } = useParams();

  const { error, products } = useSelector((state: any) => state.products);
  // const { error: deleteError, isDeleted } = useSelector((state: any) => state.product);

  const deleteProductHandler = (id: number) => {
    dispatch<any>(deleteProduct(id));
  };

  useEffect(() => {
    if (error) {
      console.log(error);
      dispatch<any>(clearErrors());
    }

    // if (deleteError) {
    //   console.log(deleteError);
    //   dispatch<any>(clearErrors());
    // }

    // if (isDeleted) {
    //   console.log("Product Deleted Successfully");
    //   navigate("/admin/dashboard");
    //   dispatch<any>({ type: DELETE_PRODUCT_RESET });
    // }

    dispatch<any>(getAdminProduct());
  }, [dispatch, alert, error
    // , deleteError, isDeleted
  ]);

  const columns = [
    { field: "id", headerName: "Product ID", minWidth: 200, flex: 0.5 },

    {
      field: "name",
      headerName: "Name",
      minWidth: 350,
      flex: 1,
    },
    {
      field: "stock",
      headerName: "Stock",
      type: "number",
      minWidth: 150,
      flex: 0.3,
    },

    {
      field: "price",
      headerName: "Price",
      type: "number",
      minWidth: 270,
      flex: 0.5,
    },

    {
      field: "actions",
      flex: 0.3,
      headerName: "Actions",
      minWidth: 150,
      type: "number",
      sortable: false,
      renderCell: () => {
        return (
          <Fragment>
            <Link to={`/store/product/${id}`}>
              <EditIcon />
            </Link>

            <Button
              onClick={() =>
                deleteProductHandler(Number(id))
              }
            >
              <DeleteIcon />
            </Button>
          </Fragment>
        );
      },
    },
  ];

  const rows: any[] = [];
// console.log(products)
//   products &&
//     products.forEach((item: any) => {
//       rows.push({
//         id: item.id,
//         stock: item.stock,
//         price: item.price,
//         name: item.name,
//       });
//     });

  return (
    <Fragment>
      <DocTitle title={`ALL PRODUCTS`} />

      <div className="dashboard">
        <SideBar role="store"/>
        <div className="listContainer">
          <h1 id="listHeading">ALL PRODUCTS</h1>

          <DataGrid
            rows={rows}
            columns={columns}
            pageSize={10}
            disableSelectionOnClick
            className="listTable"
            autoHeight
          />
        </div>
      </div>
     </Fragment>
  );
};

export default ProductList;
