import "./css/chat.css"
import ChatCard from "../components/chat/ChatCard";
import Message from "../components/chat/Message";
import DocTitle from "../components/layout/DocTitle";
import { Fragment, useEffect, useRef, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { clearErrors, getChats, getMessages, postNewMessage } from "../features/chat/chatAction";
import { GET_NEW_MESSAGE } from "../features/chat/chatConstant";
import { io } from "socket.io-client";


const Chat = () => {
  const dispatch = useDispatch();

  const { user } = useSelector((state: any) => state.user);
  const [currentChat, setCurrentChat] = useState<any>(null);
  const [newMessage, setNewMessage] = useState("");
  const [arrivalMessage, setArrivalMessage] = useState<any>(null);
  const { error: chatErr, chats } = useSelector((state: any) => state.chats);
  const { error: msgErr, messages } = useSelector((state: any) => state.messages);
  const { member } = useSelector((state: any) => state.chatMember);

  const socket = useRef<any>();
  const scrollRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    socket.current = io("http://localhost:8080");
    socket.current.on("getMessage", (data:{senderId:number, text:string, createdAt:string}) => {
      // console.log(`new arrival msg: ${data.text}`)
      setArrivalMessage({
        sender: data.senderId,
        text: data.text,
        createdAt: Date.now(),
      });
    });
  }, []);

  useEffect(() => {
    // if (
      arrivalMessage &&
      currentChat?.members.includes(arrivalMessage.sender) 
      &&
      dispatch<any>({ type: GET_NEW_MESSAGE, payload: arrivalMessage });
      // console.log(`dispatched: ${arrivalMessage}`)
  }, [arrivalMessage, currentChat]);

  useEffect(() => {
    user.id && socket.current.emit("addUser", user.id);
  }, [user]);

  useEffect(() => {
    dispatch<any>(getChats(user.id));

    if (chatErr) {
      console.log(chatErr);
      dispatch<any>(clearErrors());
    }
  }, [chatErr, user]);

  useEffect(() => {
    if(currentChat) {
      dispatch<any>(getMessages(currentChat._id));
    }
    
    if (msgErr) {
      console.log(msgErr);
      dispatch<any>(clearErrors());
    }
  }, [dispatch, msgErr, currentChat]);

  const handleSubmit = async (e: any) => {
    e.preventDefault();

    const receiverId = currentChat?.members.find(
      (member: number) => member !== user.id
    );

    const message = {
      senderId: user.id,
      receiverId,
      text: newMessage,
      chatId: currentChat?._id,
    };

    // console.log(message)
    // console.log(receiverId)

    socket.current.emit("sendMessage", {
      senderId: user.id,
      receiverId,
      text: newMessage,
    });

    try {
      dispatch<any>(postNewMessage(message));
      setNewMessage("");
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    scrollRef.current?.scrollIntoView({ behavior: "smooth" });
  }, [messages]);


  return (
    <Fragment >
      <DocTitle title="Messenger - Jamkel" />
      <div className="chat">
        
        <div className="chatMenu">
          <div className="chatMenuWrapper">
            <input placeholder="Search for friends" className="chatMenuInput" />
            {chats && chats.map((chat: any) => (
              <div key={chat._id} onClick={() => setCurrentChat(chat)}>
                <ChatCard {...chat} currentUser={user.id} />
              </div>
            ))}
          </div>
        </div>

        <div className="chatBox">
          
          <div className="chatBoxWrapper">
            {currentChat ? (
              <Fragment >
                <div className="currentChatName">
                  <p>{member.name}</p>
                  </div>
                <div className="chatBoxTop">
                  {messages && messages.map((message: any, index:number) => (
                    <div ref={scrollRef}>
                      <Message {...message} key={index}/>
                    </div>
                  ))}
                </div>

                <div className="chatBoxBottom">
                  <input
                    className="chatMessageInput"
                    onChange={(e) => setNewMessage(e.target.value)}
                    value={newMessage}
                  ></input>
                  <button className="chatSubmitButton" onClick={handleSubmit}>
                    Send
                  </button>
                </div>              
              </Fragment >
            ) : (
              <span className="noConversationText">
                Hello!
              </span>
            )}
          </div>
        </div>

      </div>
    </Fragment >
  );
}

export default Chat