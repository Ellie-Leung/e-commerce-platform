import "./css/cart.css";
import Typography from '@mui/material/Typography';
import RemoveShoppingCartIcon from '@mui/icons-material/RemoveShoppingCart';
import CartItemCard from "../components/cart/CartItemCard";
import DocTitle from "../components/layout/DocTitle";
import NumberFormat from 'react-number-format';
import { Fragment, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { updateCartItem, removeItemsFromCart, clearErrors } from "../features/cart/cartAction";

const Cart = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { cartItems, error} = useSelector((state: any) => state.cart);


  const increaseQuantity = (item: any) => {

    const newQty = item.quantity + 1;
    if (item.stock <= item.quantity) {
      console.log("not enough stock")
      return;
    }

    dispatch<any>(updateCartItem(item, newQty));

    if (error) {
      console.log(error);
      dispatch<any>(clearErrors());
    }
  };

  const decreaseQuantity = (item: any) => {
    const newQty = item.quantity - 1;
    if (1 >= item.quantity) {
      return;
    }
    dispatch<any>(updateCartItem(item, newQty));
  };

  const deleteCartItems = (id: number) => {
    dispatch<any>(removeItemsFromCart(id));
  };


  const checkoutHandler = () => {
    navigate("/shipping");
  };


  return (
    <Fragment>
      {cartItems.length === 0 ? (
        <div className="emptyCart">
          <RemoveShoppingCartIcon />

          <Typography>No Product in Your Cart</Typography>
          <Link to="/">Shop Now</Link>
        </div>
      ) : (
        <Fragment>
          <DocTitle title="Shopping Cart - Jamkel" />
          <div className="cartPage">
            <div className="cartHeader">
              <p>Product</p>
              <p>Quantity</p>
              <p>Subtotal</p>
            </div>

            {cartItems && cartItems.map((item: any) => (
                <div className="cartContainer" key={item.id}>
                  <CartItemCard item={item} deleteCartItems={deleteCartItems} />
                  <div className="cartInput">
                    <button
                      onClick={() =>
                        decreaseQuantity(item)
                      }
                    >
                      -
                    </button>
                    <input type="number" value={item.quantity} readOnly />
                    <button
                      onClick={() =>
                        increaseQuantity(item)
                      }
                    >
                      +
                    </button>
                  </div>
                  <span className="cartSubtotal">
                  <NumberFormat 
                value={`$${
                  item.price * item.quantity
                }`}
                decimalScale={2}
                thousandSeparator={true}
                prefix={'$'}
                displayType={'text'}
                />  
                  </span>
                </div>
              ))}



            <div className="cartGrossProfit">
              <div></div>
              <div className="cartGrossProfitBox">
                <p>Gross Total</p>
                <p>
                <NumberFormat 
                value={`${cartItems.reduce(
                  (acc: any, item: any) => acc + item.quantity * item.price,
                  0
                )}.toFixed(2)`}
                thousandSeparator={true}
                decimalScale={2}
                prefix={'$'}
                displayType={'text'}
                />
                </p>
              </div>
              <div></div>
              <div className="checkOutBtn">
                <button onClick={checkoutHandler}>Check Out</button>
              </div>
            </div>
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

export default Cart;