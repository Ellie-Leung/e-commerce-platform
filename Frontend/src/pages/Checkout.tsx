import "./css/checkout.css";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import EventIcon from "@mui/icons-material/Event";
import VpnKeyIcon from "@mui/icons-material/VpnKey";

import DocTitle from "../components/layout/DocTitle";
import CheckoutSteps from "../components/cart/CheckoutSteps";

import { Fragment, useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { createOrder, clearErrors } from "../features/order/orderAction";
// import { CardNumberElement, CardCvcElement, CardExpiryElement, useStripe, useElements } from "@stripe/react-stripe-js";
import axios from "axios";
import { useNavigate } from "react-router-dom";


const Checkout = () => {
  const orderInfo = JSON.parse(sessionStorage.getItem("orderInfo") || "");

  // const dispatch = useDispatch();
  // const navigate = useNavigate();
  // const stripe = useStripe();
  // const elements = useElements();
  const payBtn = useRef<HTMLInputElement>(null);

  const { shippingInfo, cartItems } = useSelector((state: any) => state.cart);
  const { user } = useSelector((state: any) => state.user);
  const { error } = useSelector((state: any) => state.newOrder);

  const paymentData = {
    amount: Math.round(orderInfo.totalPrice * 100),
  };

  const order = {
    shippingInfo,
    orderItems: cartItems,
    itemsPrice: orderInfo.subtotal,
    taxPrice: orderInfo.tax,
    shippingPrice: orderInfo.shippingCharges,
    totalPrice: orderInfo.totalPrice,
    paymentInfo: {},
  };

  // const submitHandler = async (e: any) => {
  //   e.preventDefault();

  //   if(payBtn.current) {
  //     payBtn.current.disabled = true;
  //   }
    

  // try {
      // const config = {
      //   headers: { "Content-Type": "application/json",},
      // };

      // let token = JSON.parse(localStorage.getItem("token") || '{}')

      // const { data } = await axios.post(
      //   `/payment?access_token=${token}`,
      //   paymentData,
      //   config
      // );

      // const client_secret = data.client_secret;

      // if (!stripe || !elements) return;

      // const result = await stripe.confirmCardPayment(client_secret, {
      //   payment_method: {
      //     card: elements.getElement(CardNumberElement)!,
      //     billing_details: {
      //       name: user.name,
      //       email: user.email,
      //       address: {
      //         line1: shippingInfo.address,
      //         city: shippingInfo.city,
      //         state: shippingInfo.state,
      //         postal_code: shippingInfo.pinCode,
      //         country: shippingInfo.country,
      //       },
      //     },
      //   },
      // });

  //   console.log(result)

  //     if (result.error && payBtn.current) {
  //       payBtn.current.disabled = false;

  //       console.log(result.error.message);
  //     } else {
  //       if (result.paymentIntent?.status === "succeeded") {
  //         order.paymentInfo = {
  //           id: result.paymentIntent.id,
  //           status: result.paymentIntent.status,
  //         };

  //         dispatch<any>(createOrder(order));

  //         navigate("/success");
  //       } else {
  //         console.log("There's some issue while processing payment ");
  //       }
  //     }
  //   } catch (error: any ) {
  //     if(payBtn.current) {
  //       payBtn.current.disabled = false;
  //     }
  //     console.log(error.response.data.message);
  //   }
  // };

  // useEffect(() => {
  //   if (error) {
  //     console.log(error);
  //     dispatch<any>(clearErrors());
  //   }
  // }, [dispatch, error]);

  return (
    <Fragment>
      <DocTitle title="Payment" />
       <CheckoutSteps activeStep={2} />
      <div className="paymentContainer">
        <form className="paymentForm" 
        // onSubmit={(e) => submitHandler(e)}
        >
          <h2>Card Info</h2>
          <div>
            <CreditCardIcon />
            {/* <CardNumberElement className="paymentInput" /> */}
          </div>
          <div>
            <EventIcon />
            {/* <CardExpiryElement className="paymentInput" /> */}
          </div>
          <div>
            <VpnKeyIcon />
            {/* <CardCvcElement className="paymentInput" /> */}
          </div>

          <input
            type="submit"
            value={`Pay - $${orderInfo && orderInfo.totalPrice}`}
            ref={payBtn}
            className="paymentFormBtn"
          />
        </form>
      </div>
     </Fragment>
  );
};

export default Checkout;
