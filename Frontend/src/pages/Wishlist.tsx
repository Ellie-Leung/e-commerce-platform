import { DataGrid } from '@mui/x-data-grid';
import { Fragment, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import DocTitle from "../components/layout/DocTitle";
import SideBar from "../components/layout/Sidebar";
import Loader from "../components/layout/Loader";

function Wishlist() {

    const { loading, error, orders } = useSelector((state: any) => state.myOrders);
    const { user } = useSelector((state: any) => state.user);


    const columns = [
        { 
          field: "product_id", 
          headerName: "Product#", 
          minWidth: 300, 
          flex: 1,
          renderCell: (params: any ) => {
            return (
              <Fragment>
                {/* <p>{params.id}</p> */}
                <div>
                  {/* <img src={`/assets/product-img/${params.getValue(params.id, "image")}`}/> */}
                  {/* <p>{params.name}</p> */}
                </div>
           
              </Fragment>
            );
          },
        },
    
        // {
        //   field: "status",
        //   headerName: "Status",
        //   minWidth: 150,
        //   flex: 0.5,
        //   cellClassName: (params: any) => {
        //     return params.getValue(params.id, "status") === "Delivered"
        //       ? "greenColor"
        //       : "redColor";
        //   },
        // },
        // {
        //   field: "qty",
        //   headerName: "Qty",
        //   type: "number",
        //   minWidth: 150,
        //   flex: 0.3,
        // },
    
        // {
        //   field: "amount",
        //   headerName: "Amount",
        //   type: "number",
        //   minWidth: 270,
        //   flex: 0.5,
        // },
    
        // {
        //   field: "actions",
        //   flex: 0.3,
        //   headerName: "Actions",
        //   minWidth: 150,
        //   type: "number",
        //   sortable: false,
        //   renderCell: (params: any) => {
        //     return (
        //       <Link to={`/order/${params.getValue(params.id, "id")}`}>
        //         <LaunchIcon />
        //       </Link>
        //     );
        //   },
        // },
      ];
    
      const rows: any[] = [];
    
    
    //   orders &&
    //     orders.forEach((item: any, index: any) => {
    //       rows.push({
    //         qty: item.qty,
    //         id: item.order_id,
    //         status: item.status,
    //         amount: item.unit_price,
    //         image: item.image
    //       });
    //     });
    

  return (
    <Fragment>
    <DocTitle title={`${user.name} - Orders`} />
    <div className="dashboard">
      <SideBar role="user"/>
    {loading ? (
      <Loader />
    ) : (
      <div className="myOrdersPage">
        <DataGrid
          rows={rows}
          columns={columns}
          pageSize={10}
          disableSelectionOnClick
          className="myOrdersTable"
          autoHeight
        />

        <div id="myOrdersHeading">{user.name}'s Wishlist</div>
      </div>
    )}
    </div>
  </Fragment>
  )
}

export default Wishlist