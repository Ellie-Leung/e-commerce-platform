import "./css/products.css";
import Box from '@mui/material/Box';

import Loader from "../components/layout/Loader";
import DocTitle from "../components/layout/DocTitle";
import FilterCard from "../components/product/FilterCard";
import ProductCard from "../components/product/ProductCard"
import { clearErrors, getProduct } from "../features/product/productAction";
import { Fragment, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import ReactPaginate from 'react-paginate';


function Products() {
  const dispatch = useDispatch();
 
  const [pageOffset, setPageOffset] = useState(0);
  const [pageCount, setPageCount] = useState(0);

  const {
    products,
    loading,
    error,
    productsCount,
    resultPerPage,
  } = useSelector((state: any) => state.products);
  
  const handlePageClick = (e: any) => {
    setPageOffset(e.selected);
  };

//   useEffect(() => {
//     if (error) {
//       console.log(error);
//       dispatch<any>(clearErrors());
//     }

//     // const newPageCount = getGitHubPageCount(response);
//     // console.log(responseJson, newPageCount);
//     // setRepositories(responseJson);
//     // setPageCount(newPageCount);
  

//     dispatch<any>(getProduct());
//   }, [dispatch, error
//   // , pageOffset, perPage
// ]);

useEffect(()=>{
  dispatch<any>(getProduct());
},[])

  return (
    <Fragment>
    {loading && <Loader /> }
        <DocTitle title="Products - Jamkel" />
        <h2 className="results-heading">Result products</h2>
        <div className='results-container' > 
          <FilterCard />     
          <Box className="resultsBox">
            <ProductCard />
          </Box>
        </div> 
      
    {/* {resultPerPage < productsCount && (
      <div className="paginationBox">
      <ReactPaginate
        breakLabel="..."
        previousLabel="< previous"
        nextLabel="next >"
        pageClassName="page-item"
        pageLinkClassName="page-link"
        onPageChange={handlePageClick}

        pageRangeDisplayed={resultPerPage}
        pageCount={pageCount}
        forcePage={pageOffset}
        activeClassName="pageItemActive"
        activeLinkClassName="pageLinkActive"
      />
      </div>
    )} */}
    </Fragment>
  )
}

export default Products