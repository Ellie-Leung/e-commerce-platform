import "./css/dashboard.css";
import Typography from "@mui/material//Typography";
import Sidebar from "../components/layout/Sidebar";
import DocTitle from "../components/layout/DocTitle";
import NumberFormat from 'react-number-format';
import { useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { getAdminProduct } from "../features/product/productAction";
import { getAllOrders } from "../features/order/orderAction";
import { getAllUsers } from "../features/user/userAction";
import { Doughnut, Line } from "react-chartjs-2";

const Dashboard = () => {
  const dispatch = useDispatch();

  const { products } = useSelector((state: any) => state.products);
  const { orders } = useSelector((state: any) => state.allOrders);
  const { users } = useSelector((state: any) => state.allUsers);

  let outOfStock = 0;

  products && products.forEach((item: any) => {
      if (item.tock === 0) {
        outOfStock += 1;
      }
  });

  useEffect(() => {
    dispatch<any>(getAdminProduct());
    dispatch<any>(getAllOrders());
    dispatch<any>(getAllUsers());
  }, [dispatch]);


  let totalAmount = 0;
  orders && orders.forEach((item: any) => {
      totalAmount += Number(item.amount);
  });


  const lineState = {
    labels: ["Initial Amount", "Amount Earned"],
    datasets: [
      {
        label: "TOTAL AMOUNT",
        backgroundColor: ["tomato"],
        hoverBackgroundColor: ["rgb(197, 72, 49)"],
        data: [0, totalAmount],
      },
    ],
  };

  console.log(lineState)

  const doughnutState = {
    labels: ["Out of Stock", "In Stock"],
    datasets: [
      {
        backgroundColor: ["#00A6B4", "#6800B4"],
        hoverBackgroundColor: ["#4B5000", "#35014F"],
        data: [outOfStock, products.length - outOfStock],
      },
    ],
  };

  console.log(doughnutState)

  return (
    <div className="dashboard">
      <DocTitle title="Dashboard - Admin Panel" />
      <Sidebar role="admin"/>

      <div className="dashboardContainer">
        <Typography component="h1">Dashboard</Typography>

        <div className="dashboardSummary">
          <div>
            <p>
              Total Amount <br /> 
              <NumberFormat 
                value={totalAmount}
                thousandSeparator={true}
                prefix={'$'}
                displayType={'text'}
                />
              
            </p>
          </div>
          <div className="dashboardSummaryBox2">
            <Link to="/admin/products">
              <p>Product</p>
              <p>{products && products.length}</p>
            </Link>
            <Link to="/admin/orders">
              <p>Orders</p>
              <p>{orders && orders.length}</p>
            </Link>
            <Link to="/admin/users">
              <p>Users</p>
              <p>{users && users.length}</p>
            </Link>
          </div>
        </div>

        {/* <div className="lineChart">
          <Line data={lineState} />
        </div> */}

        {/* <div className="doughnutChart">
          <Doughnut data={doughnutState} />
        </div> */}
      </div>
    </div>
  );
};

export default Dashboard;
