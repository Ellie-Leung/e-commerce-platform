import './css/productDetails.css'
import Carousel from 'react-material-ui-carousel'
import Rating from '@mui/material/Rating'
import TextsmsIcon from '@mui/icons-material/Textsms';

import ReviewCard, { ReviewCardProps } from '../components/product/ReviewCard'
import Loader from '../components/layout/Loader'
import DocTitle from '../components/layout/DocTitle'

import NumberFormat from 'react-number-format'
import { Fragment, useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useParams, Link } from 'react-router-dom'
import {
  clearErrors,
  getProductDetails,
  getAllReviews,
} from '../features/product/productAction'
import { addItemToCart, updateCartItem } from '../features/cart/cartAction'
import { createNewChat } from '../features/chat/chatAction'

function ProductDetails() {
  const { id } = useParams()
  const dispatch = useDispatch()
  const { product, loading, error } = useSelector((state: any) => state.productDetails)
  const { reviews, error: reviewsError } = useSelector((state: any) => state.productReviews)
  const { cartItems } = useSelector((state: any) => state.cart)

  const [quantity, setQuantity] = useState(1)

  const increaseQuantity = () => {
    if (product.stock <= quantity) return

    const qty = quantity + 1
    setQuantity(qty)
  }

  const decreaseQuantity = () => {
    if (1 >= quantity) return

    const qty = quantity - 1
    setQuantity(qty)
  }

  const addToCartHandler = () => {
    const isItemExist = cartItems.find((i: any) => i.product_id === product.id)

    if (isItemExist) {
      const newQty = isItemExist.quantity + quantity
      dispatch<any>(updateCartItem(isItemExist, newQty))
      console.log('updated cart item')
    } else {
      dispatch<any>(addItemToCart(product, quantity))
      console.log('Item added to cart')
    }
  }

  const handleClick = () => {
    try {
      dispatch<any>(createNewChat(product.store_id));
    } catch (err) {
      console.log(err);
    }
  }

  useEffect(() => {
    if (error | reviewsError) {
      console.log(error)
      dispatch<any>(clearErrors())
    }

    dispatch<any>(getProductDetails(Number(id)))
    dispatch<any>(getAllReviews(Number(id)))
  }, [dispatch, id, error, reviewsError])

  return (
    <Fragment>
      {loading && <Loader />}
      <DocTitle title={`${product.name} - Jamkel`} />
      <div className="product-details">
        <div>
          <Carousel className="product-carousel">
            {product.images_url &&
              product.images_url.map((img: string, i: number) => (
                <img
                  className="carousel-image"
                  key={i}
                  src={img}
                  alt={`${i} Slide`}
                />
              ))}
          </Carousel>
        </div>

        <div>
          <div className="detailsBlock-1">
            <h2>{product.name}</h2>
            <div>
            <Link to={`/store/${product.store_id}`} className="store-name">
              {product.store_name}
            </Link>
            <TextsmsIcon className="chatIcon" onClick={handleClick} />
            </div>
           
          </div>

          <div className="detailsBlock-2">
            <Rating value={product.avg_rating} precision={0.5} readOnly />
            <span className="detailsBlock-2-span">
              ({product.numOfReviews} Reviews)
            </span>
          </div>

          <div className="detailsBlock-3">
            <h1>
              <NumberFormat
                value={product.price}
                thousandSeparator={true}
                prefix={'$'}
                displayType={'text'}
              />
            </h1>
            {/* <div className="filter-container">
            <div className="filter">
              <span className="filter-title">Color</span>
              {product.color?.map((c) => (
                <div className="filter-color" color={c} key={c} onClick={() => setColor(c)} />
              ))}
            </div>
            <div>
              <span>Size</span>
              <select className="filter-size" onChange={(e) => setSize(e.target.value)}>
                {product.size?.map((s) => (
                  <option className="filter-size-option" key={s}>{s}</option>
                ))}
              </select>
            </div>
          </div> */}
            {product.stock > 0 && (
              <div className="detailsBlock-3-1">
                <div className="detailsBlock-3-1-1">
                  <button onClick={decreaseQuantity}>-</button>
                  <input readOnly type="number" value={quantity} />
                  <button onClick={increaseQuantity}>+</button>
                </div>
                <button onClick={addToCartHandler}>Add to Cart</button>
              </div>
            )}

            <p>
              Status:
              <b className={product.stock < 1 ? 'redColor' : 'greenColor'}>
                {product.stock < 1 ? ' Out Of Stock' : ' In Stock'}
              </b>
            </p>
          </div>

          <div className="detailsBlock-4">
            Description : <p>{product.description}</p>
          </div>
        </div>
      </div>

      <h3 className="reviewsHeading">REVIEWS</h3>
      {reviews && reviews[0] ? (
        <div className="reviews">
          {reviews &&
            reviews.map((review: ReviewCardProps) => (
              <ReviewCard key={review.id} {...review} />
            ))}
        </div>
      ) : (
        <p className="noReviews">No Reviews Yet</p>
      )}
    </Fragment>
  )
}

export default ProductDetails
