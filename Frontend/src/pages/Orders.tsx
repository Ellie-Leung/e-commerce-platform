import "./css/orders.css";
import { DataGrid } from '@mui/x-data-grid';
import LaunchIcon from '@mui/icons-material/Launch';

import Loader from "../components/layout/Loader";
import DocTitle from "../components/layout/DocTitle";
import SideBar from "../components/layout/Sidebar";

import { Fragment, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { clearErrors, myOrders } from "../features/order/orderAction";


const Orders = () => {
  const dispatch = useDispatch();

  const { loading, error, orders } = useSelector((state: any) => state.myOrders);
  const { user } = useSelector((state: any) => state.user);

  const columns = [
    { 
      field: "order_id", 
      headerName: "Order#", 
      minWidth: 300, 
      flex: 1,
      renderCell: (params: any ) => {
        return (
          <Fragment>
            <p>{params.id}</p>
            <div>
              <img src={`/assets/product-img/${params.getValue(params.id, "image")}`}/>
              <p>{params.name}</p>
            </div>
            {/* <div className="CartItemCard">
      <img src={`/assets/product-img/${item.images[0]}`} alt="ssa" />
      <div>
        <Link to={`/product/${item.id}`}>{item.name}</Link>
        <span>{`Price: $${item.price}`}</span>
        <p onClick={() => deleteCartItems(item.id)}>Remove</p>
      </div>
    </div> */}
          </Fragment>
        );
      },
    },

    {
      field: "status",
      headerName: "Status",
      minWidth: 150,
      flex: 0.5,
      cellClassName: (params: any) => {
        return params.getValue(params.id, "status") === "Delivered"
          ? "greenColor"
          : "redColor";
      },
    },
    {
      field: "qty",
      headerName: "Qty",
      type: "number",
      minWidth: 150,
      flex: 0.3,
    },

    {
      field: "amount",
      headerName: "Amount",
      type: "number",
      minWidth: 270,
      flex: 0.5,
    },

    {
      field: "actions",
      flex: 0.3,
      headerName: "Actions",
      minWidth: 150,
      type: "number",
      sortable: false,
      renderCell: (params: any) => {
        return (
          <Link to={`/order/${params.getValue(params.id, "id")}`}>
            <LaunchIcon />
          </Link>
        );
      },
    },
  ];

  const rows: any[] = [];


  orders &&
    orders.forEach((item: any, index: any) => {
      rows.push({
        qty: item.qty,
        id: item.order_id,
        status: item.status,
        amount: item.unit_price,
        image: item.image
      });
    });

  useEffect(() => {
    if (error) {
      console.log(error);
      dispatch<any>(clearErrors());
    }

    dispatch<any>(myOrders());
  }, [dispatch, error]);

  return (
    <Fragment>
      <DocTitle title={`${user.name} - Orders`} />
      <div className="dashboard">
        <SideBar role="user"/>
      {loading ? (
        <Loader />
      ) : (
        <div className="myOrdersPage">
          <DataGrid
            rows={rows}
            columns={columns}
            pageSize={10}
            disableSelectionOnClick
            className="myOrdersTable"
            autoHeight
          />

          <div id="myOrdersHeading">{user.name}'s Orders</div>
        </div>
      )}
      </div>
    </Fragment>
  );
};

export default Orders;