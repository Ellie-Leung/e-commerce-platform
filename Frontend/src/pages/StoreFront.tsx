import "./css/storeFront.css";
import Box from '@mui/material/Box';

import Loader from "../components/layout/Loader";
import DocTitle from "../components/layout/DocTitle";
import FilterCard from "../components/product/FilterCard";
import ProductCard from "../components/product/ProductCard"


import { Fragment, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { clearErrors, getStoreProducts } from "../features/product/productAction";
import { useParams } from "react-router";

function StoreFront() {
    const { id } = useParams();
    const dispatch = useDispatch();
    const {products, loading, error} = useSelector((state: any) => state.products);

    console.log(products)

    useEffect(() => {
        if (error) {
          console.log(error);
          dispatch<any>(clearErrors());
        }
        dispatch<any>(getStoreProducts(Number(id)));
      }, [dispatch, error]);

  return (
    <Fragment>
    {loading ? (
      <Loader />
    ) : (
      <Fragment>
        <DocTitle title="Products - Jamkel" />
        <h2 className="results-heading">{products[0].store_name}</h2>
        <div className='results-container' > 
          <FilterCard />     
          <Box className="resultsBox">
            <ProductCard />
          </Box>
        </div> 
        </Fragment>
      )}
    </Fragment>
  )
  
}


export default StoreFront
