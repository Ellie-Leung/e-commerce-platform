import "./css/user.css";

import SideBar from "../components/layout/Sidebar";
import DocTitle from "../components/layout/DocTitle";
import Loader from "../components/layout/Loader";

import { Link } from "react-router-dom";
import { Fragment, useEffect } from "react";
import { useSelector } from "react-redux";


const Profile = () => {
  // const navigate = useNavigate();
  const { user, loading, isAuthenticated } = useSelector((state: any) => state.user);

  useEffect(() => {
    if (isAuthenticated === false) {
      console.log("not yet sign in")
    }
  }, [isAuthenticated]);

  let avatar = user.image_url? user.image_url : "userIcon.jpg"

  return (
    <Fragment>
      {loading ? (
        <Loader />
      ) : ( 
        <Fragment>
        <DocTitle title={`${user.name}'s Profile`} />
        <div className="dashboard">
        <SideBar role="user"/>
        <div className="profileContainer">
          <div>
            <h1>My Profile</h1>
            <img src={`/assets/user-img/${avatar}`} alt={user.name} />
            <Link to="/account/update">Edit Profile</Link>
          </div>
          <div>
            <div>
              <h4>Username</h4>
              <p>{user.name}</p>
            </div>
            <div>
              <h4>Email</h4>
              <p>{user.email}</p>
            </div>
            <div>
              <h4>Joined On</h4>
              <p>{String(user.created_at).substring(0, 10)}</p>
            </div>

            <div>
              <Link to="/password/update">Change Password</Link>
            </div>
          </div>
        </div>
        </div>
      </Fragment>
        
      )}
    </Fragment>
  );
};

export default Profile;