import './css/homepage.css'
import Box from '@mui/material/Box'
import Slider from '../components/product/Slider'
import ProductCard from '../components/product/ProductCard'
import Loader from '../components/layout/Loader'
import DocTitle from '../components/layout/DocTitle'
import { useEffect, Fragment } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { clearErrors, getProduct } from '../features/product/productAction'

function Homepage() {
  const dispatch = useDispatch()
  const { loading, error, products } = useSelector(
    (state: any) => state.products,
  )

  useEffect(() => {
    console.log('useEffect')

    dispatch<any>(getProduct())
  }, [])


  return (
    <Fragment>
      {loading && <Loader />}
      <DocTitle title="Jamkel" />
      <div className="hompage">
        <Slider />
        <div className="homeContainer">
          <h2 className="homeHeading">Featured Products</h2>
          <Box className="resultsBox">
            <ProductCard />
          </Box>
          {/* <div className="productContainer" id="container">
            {products &&
              products.map((product: ProductCardProps) => (
                <ProductCard key={product.id} {...product} />
              ))}
            </div> */}
        </div>
      </div>
    </Fragment>
  )
}

export default Homepage
