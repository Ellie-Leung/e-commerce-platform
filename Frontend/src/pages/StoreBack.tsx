import "./css/storeBack.css";
// import { DataGrid } from '@mui/x-data-grid';
// import DocTitle from "../components/layout/DocTitle";
import SideBar from "../components/layout/Sidebar";
import { useSelector } from "react-redux";
import { Fragment } from "react";

function StoreBack() {
  // const { store } = useSelector((state: any) => state.store);
  // console.log(store)
  return (
    <Fragment>
      {/* <DocTitle title={`${store.name} - ALL PRODUCTS`} /> */}

      <div className="dashboard">
        <SideBar role="store"/>
        <div className="listContainer">
          <h1 id="listHeading">ALL PRODUCTS</h1>

          {/* <DataGrid
            rows={rows}
            columns={columns}
            pageSize={10}
            disableSelectionOnClick
            className="listTable"
            autoHeight
          /> */}
        </div>
      </div>
     </Fragment>
  )
}

export default StoreBack