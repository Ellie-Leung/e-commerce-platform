
import "./css/orderDetails.css";
import Typography from '@mui/material/Typography';

import DocTitle from "../components/layout/DocTitle";
import Loader from "../components/layout/Loader";

import { Fragment, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from 'react-router-dom';
import { Link } from "react-router-dom";
import { getOrderDetails, clearErrors } from "../features/order/orderAction"


const OrderDetails = () => {
  const { order, error, loading } = useSelector((state: any) => state.orderDetails);
  const { id } = useParams();
  const dispatch = useDispatch();

  useEffect(() => {
    if (error) {
      console.log(error);
      dispatch<any>(clearErrors());
    }

    dispatch<any>(getOrderDetails(Number(id)));
  }, [dispatch, error, id]);

  return (
    <Fragment>
      {loading ? (
        <Loader />
      ) : (
        <Fragment>
          <DocTitle title="Order Details" />
          <div className="orderDetailsPage">
            <div className="orderDetailsContainer">
              <Typography component="h1">
                Order #{order && order._id}
              </Typography>
              <Typography>Shipping Info</Typography>
              <div className="orderDetailsContainerBox">
                <div>
                  <p>Name:</p>
                  <span>{order && order.receiver}</span>
                </div>
                <div>
                  <p>Phone:</p>
                  <span>
                    {order && order.contact_number}
                  </span>
                </div>
                <div>
                  <p>Address:</p>
                  <span>
                    {order && order.address}
                  </span>
                </div>
              </div>
              <Typography>Payment</Typography>
              <div className="orderDetailsContainerBox">
                <div>
                  <p
                    className={
                      order && order.status === "succeeded"
                        ? "greenColor"
                        : "redColor"
                    }
                  >
                    {order && order.status === "succeeded"
                      ? "PAID"
                      : "NOT PAID"}
                  </p>
                </div>

                <div>
                  <p>Amount:</p>
                  <span>{order && order.amount}</span>
                </div>
              </div>

              <Typography>Order Status</Typography>
              <div className="orderDetailsContainerBox">
                <div>
                  <p
                    className={
                      order && order.status === "Delivered"
                        ? "greenColor"
                        : "redColor"
                    }
                  >
                    {order && order.status}
                  </p>
                </div>
              </div>
            </div>

            <div className="orderDetailsCartItems">
              <Typography>Order Items:</Typography>
              <div className="orderDetailsCartItemsContainer">
                {order.orderItems &&
                  order.orderItems.map((item: any) => (
                    <div key={item.product}>
                      <img src={item.image} alt="Product" />
                      <Link to={`/product/${item.product}`}>
                        {item.name}
                      </Link>{" "}
                      <span>
                        {item.quantity} X ₹{item.price} ={" "}
                        <b>₹{item.price * item.quantity}</b>
                      </span>
                    </div>
                  ))}
              </div>
            </div>
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

export default OrderDetails;