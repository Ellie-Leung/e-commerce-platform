import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { RequireAuth, RequireAdmin } from "./RequireAuth";
import store from "./features/store";
import { loadUser } from "./features/user/userAction";
import axios from "axios";
import Header from "./components/layout/Header";
import UpdateProfile from "./components/user/UpdateProfile";
import Homepage from "./pages/Homepage";
import ProductDetails from "./pages/ProductDetails";
import Products from "./pages/Products";
import Cart from "./pages/Cart";
import User from "./pages/User";
import Orders from "./pages/Orders";
import OrderDetails from "./pages/OrderDetails";
import NewProduct from "./components/store/NewProductForm";
import OrderList from "./components/admin/OrderList";
import UpdateOrder from "./components/admin/UpdateOrder";
import ProductList from "./components/admin/ProductList";
import ReviewList from "./components/admin/ReviewList";
import UserList from "./components/admin/UserList";
import UpdateUser from "./components/admin/UpdateUser";
import Chat from "./pages/Chat";
import Checkout from "./pages/Checkout";
import Shipping from "./components/cart/Shipping";
import ConfirmOrder from "./components/cart/ConfirmOrder";
import StoreBack from "./pages/StoreBack";
import StoreFront from "./pages/StoreFront";
import Dashboard from "./pages/Dashboard";
import Wishlist from "./pages/Wishlist";
import StoreProductList from "./components/store/ProductList";
import StoreOrderList from "./components/store/OrderList";
import EditProduct from "./components/admin/EditProduct";
import CloudBtn from "./components/layout/CloudBtn";

function App() {
  // const [stripeApiKey, setStripeApiKey] = useState("");
  // let token = JSON.parse(localStorage.getItem("token") || '{}')

  // async function getStripeApiKey() {
  //   const { data } = await axios.get(`/stripeapikey?access_token=${token}`);
  //   setStripeApiKey(data.stripeApiKey);
  // }

  // useEffect(() => {
  //   store.dispatch(loadUser());
  //   getStripeApiKey();
  // }, []);

  // window.addEventListener("contextmenu", (e) => e.preventDefault());

  return (
    <Router>
      <div className="app">
        <Routes>
          <Route path="/" element={[<Header key={1} />, <Homepage key={2} />]}></Route>
          <Route path="/product/:id" element={[<Header />, <ProductDetails />]}></Route>
          <Route path="/results" element={[<Header />, <Products />]}></Route>
          <Route path="/store/:id" element={[<Header />, <StoreFront />]}></Route>

          <Route path="/" element={<RequireAuth />}>
            <Route path="cart" element={[<Header />, <Cart />]}></Route>
            <Route path="order/:id" element={[<Header />, <OrderDetails />]}></Route>
            <Route path="account" element={[<Header />, <User />]}></Route>
            <Route path="account/orders" element={[<Header />, <Orders />]}></Route>
            <Route path="account/update" element={[<Header />, <UpdateProfile />]}></Route>
            <Route path="account/wishlist" element={[<Header />, <Wishlist />]}></Route>

            <Route path="store" element={[<Header />, <StoreBack />]}></Route>
            <Route path="store/product" element={[<Header />, <NewProduct />]}></Route>
            <Route path="/store/products" element={[<Header />, <StoreProductList />]}></Route>
            <Route path="/store/orders" element={[<Header />, <StoreOrderList />]}></Route>

            {/* <Route path="/store/product/:id" element={[ <Header />, <UpdateProduct />]}></Route>
         <Route path="/store/order/:id" element={[ <Header />, <UpdateOrder />]}></Route>
         <Route path="/store/reviews" element={[ <Header />, <ReviewList />]}></Route> */}

            <Route path="chat" element={[<Header />, <Chat />]}></Route>

            <Route path="shipping" element={[<Header />, <Shipping />]}></Route>
            <Route path="order/confirm" element={[<Header />, <ConfirmOrder />]}></Route>
            <Route path="/checkout" element={[<Header />, <Checkout />]}></Route>

            {/* {stripeApiKey && (
            <Elements stripe={loadStripe(stripeApiKey)}>
              <Route path="/checkout" element={<Checkout />}></Route>
            </Elements>
         )} */}
          </Route>

          <Route path="/admin" element={<RequireAdmin />}>
            <Route path="" element={[<Header />, <Dashboard />]}></Route>
            <Route path="orders" element={[<Header />, <OrderList />]}></Route>
            <Route path="order/:id" element={[<Header />, <UpdateOrder />]}></Route>
            <Route path="products" element={[<Header />, <ProductList />]}></Route>
            <Route path="product/edit/:id" element={[<Header />, <EditProduct />]}></Route>
            <Route path="reviews" element={[<Header />, <ReviewList />]}></Route>
            <Route path="users" element={[<Header />, <UserList />]}></Route>
            <Route path="user/:id" element={[<Header />, <UpdateOrder />]}></Route>
          </Route>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
