import { useSelector } from "react-redux";
import { Outlet, Navigate } from "react-router-dom";

// interface RequireAuthProps {
//   role: string
// }

export const RequireAuth = ()=> {
  const {isAuthenticated }= useSelector((state: any) => state.user);

  return ( 
    isAuthenticated? (
        <div>
       <Outlet />
     </div>
    ) : (
        // console.log('Please sign in to continue')
        <Navigate to={"/"} />
    )
   
  )
}

  export const RequireAdmin = ()=> {
    const {user, isAuthenticated }= useSelector((state: any) => state.user);
  
    return ( 
      isAuthenticated && user.role == 'admin'? (
          <div>
         <Outlet />
       </div>
      ) : (
          // console.log(`resources not assessable by role:${user.role}`)
          <Navigate to={"/"} />
      )
     
    )
 
    
}

// export function RequireAuthAdmin(props: RequireAuthProps) {
//   const isLoggedIn = useSelector((state: IRootState) => state.auth.isLoggedIn);
//   const role = useSelector((state: IRootState) => state.auth.role);
//   return isLoggedIn && role == 'admin'? (
//     <div>
//       Hi 123
//       <Outlet />
//     </div>
//   ) : (
//     <Navigate to={"/"} />
//   );
// }




export {}